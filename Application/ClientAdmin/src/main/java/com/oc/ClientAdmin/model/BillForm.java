package com.oc.ClientAdmin.model;

import com.oc.ClientAdmin.beans.BillBean;
import lombok.extern.slf4j.Slf4j;

/**
 * RegisterForm is a form to create a bill
 * bill
 * idCustomer is for add the customer after
 */
@Slf4j
public class BillForm {
    private BillBean bill;
    private Long idCustomer;

    public BillForm() {
        log.debug("[ClientAdmin/BillForm()] instantiation of a new BillForm");
    }

    public BillBean getBill() {
        return bill;
    }

    public void setBill(BillBean bill) {
        this.bill = bill;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }
}

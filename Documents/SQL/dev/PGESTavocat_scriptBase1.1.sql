-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: db_mfavocat
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `num` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `zip_code` varchar(5) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `id_user` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_address_user_idx` (`id_user`),
  CONSTRAINT `fk_address_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill` (
  `id` int NOT NULL AUTO_INCREMENT,
  `paid` tinyint NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `date` date NOT NULL,
  `id_customer` int NOT NULL,
  `id_lawyer` int NOT NULL,
  `service` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `seen` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bill_customer_idx` (`id_customer`),
  KEY `fk_bill_lawyer_idx` (`id_lawyer`),
  CONSTRAINT `fk_bill_customer` FOREIGN KEY (`id_customer`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_bill_lawyer` FOREIGN KEY (`id_lawyer`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(5000) NOT NULL,
  `valid` tinyint NOT NULL,
  `id_lawyer` int DEFAULT NULL,
  `id_customer` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_lawyer_idx` (`id_lawyer`),
  KEY `fk_comment_customer_idx` (`id_customer`),
  CONSTRAINT `fk_comment_customer` FOREIGN KEY (`id_customer`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_comment_lawyer` FOREIGN KEY (`id_lawyer`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `seen` tinyint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `description` varchar(1000) NOT NULL,
  `id_lawyer` int NOT NULL,
  `id_customer` int NOT NULL,
  `type` tinyint NOT NULL,
  `seen` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_customer_idx` (`id_customer`),
  KEY `fk_message_lawyer_idx` (`id_lawyer`),
  CONSTRAINT `fk_message_customer` FOREIGN KEY (`id_customer`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_message_lawyer` FOREIGN KEY (`id_lawyer`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` enum('MEETING','HOLIDAY','AUDIENCE','OTHER','NO_CHOICE') NOT NULL,
  `date` date NOT NULL,
  `hour` int NOT NULL,
  `duration` int NOT NULL,
  `reason` enum('DIVORCE','ADVICE','SUCCESSION','CRIMINAL','FAMILY','CHILD_CARE','ALIMONY','CONSTRUCTION','INTELLECTUAL_PROPERTY','OTHER','NO_CHOICE') DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `accepted` tinyint(1) NOT NULL,
  `id_lawyer` int DEFAULT NULL,
  `id_customer` int DEFAULT NULL,
  `visio` tinyint NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_lawyer_idx` (`id_lawyer`),
  KEY `fk_task_customer_idx` (`id_customer`),
  CONSTRAINT `fk_task_customer` FOREIGN KEY (`id_customer`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_task_lawyer` FOREIGN KEY (`id_lawyer`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` enum('DEVELOPPER','LAWYER','CLIENT','NOT_CONNECTED') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-01  9:50:37

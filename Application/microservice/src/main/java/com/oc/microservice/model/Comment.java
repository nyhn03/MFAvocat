package com.oc.microservice.model;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * comment characterized by
 * id
 * description = message
 * valid if lawyer authorizes the publication
 * lawyer
 * customer
 */
@Slf4j
@Entity
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Length(min = 3,max = 5000,message = "La description doit être compris entre 3 et 5000 caractères.")
    private String description;
    private boolean valid;
    @ManyToOne
    @JoinColumn(name = "id_lawyer")
    private User lawyer;
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private User customer;

    public Comment() {
        log.debug("[Microservice/Comment()] instantiation of a new comment");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean check) {
        this.valid = check;
    }

    public User getLawyer() {
        return lawyer;
    }

    public void setLawyer(User lawyer) {
        this.lawyer = lawyer;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }



    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", check=" + valid +
                ", lawyer=" + lawyer +
                ", customer=" + customer +
                '}';
    }
}

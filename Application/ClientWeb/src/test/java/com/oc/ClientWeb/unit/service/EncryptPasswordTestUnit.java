package com.oc.ClientWeb.unit.service;

import com.oc.ClientWeb.beans.UserBean;
import com.oc.ClientWeb.service.EncryptPassword;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class EncryptPasswordTestUnit {
    @InjectMocks
    EncryptPassword encryptPassword;

    @Test
    public void encryptTest_user_string(){
        UserBean user = new UserBean();
        user.setPassword("Oiseau");
        user.setUsername("Poney");
        Assert.assertEquals("ec37f5fd66ebf94455ac45c061239a2f",encryptPassword.encrypt(user));
    }
}

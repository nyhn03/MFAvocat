package com.oc.ClientAdmin.service;

import com.oc.ClientAdmin.beans.UserBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

/**
 * This class is used to encrypt a string using user data
 */
@Slf4j
@Service
public class EncryptPassword {
    /**
     * Encrypt a password by concatenating the password
     * with the nickname with additional characters
     * @param user user is the user to encrypt
     * @return passwordEncrypted
     */
    public String encrypt(UserBean user){
        log.debug("[ClientAdmin/EncryptPassword/encrypt(UserBean)] with user = "+user);
        String hash = user.getPassword()+"#Gdabj+k*A"+user.getUsername()+"xC^Z£C";
        byte[] hashToByte = hash.getBytes();
        String passwordEncrypted = DigestUtils.md5DigestAsHex(hashToByte);
        log.debug("[ClientAdmin/EncryptPassword/encrypt(UserBean)] Return -> \n" +passwordEncrypted+"\n");
        return passwordEncrypted;
    }
}

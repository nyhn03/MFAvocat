package com.oc.ClientWeb.beans;

import lombok.extern.slf4j.Slf4j;

/**
 * i.e Comment of microservice
 */
@Slf4j
public class CommentBean {
    private Long id;
    private String description;
    private boolean valid;
    private UserBean lawyer;
    private UserBean customer;

    public CommentBean() {
        log.debug("[ClientWeb/CommentBean()] instantiation of a new CommentBean");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCheck() {
        return valid;
    }

    public void setCheck(boolean check) {
        this.valid = check;
    }

    public UserBean getLawyer() {
        return lawyer;
    }

    public void setLawyer(UserBean lawyer) {
        this.lawyer = lawyer;
    }

    public UserBean getCustomer() {
        return customer;
    }

    public void setCustomer(UserBean customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "CommentBean{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", check=" + valid +
                ", lawyer=" + lawyer +
                ", customer=" + customer +
                '}';
    }
}

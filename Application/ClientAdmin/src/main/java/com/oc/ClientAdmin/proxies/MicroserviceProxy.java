package com.oc.ClientAdmin.proxies;

import com.oc.ClientAdmin.beans.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@ComponentScan
@FeignClient(name="microservice", url = "localhost:9001")
public interface MicroserviceProxy {
    @GetMapping(value = "/authentification/{username}")
    UserBean findByUsername(@PathVariable("username") String username);
    @GetMapping("/taskByDate/{date}")
    List<TaskBean> findTaskByDate(@PathVariable Date date);
    @GetMapping(value = "/task/weekDate/{date}")
    ArrayList getWeekDate(@PathVariable Date date);
    @GetMapping("/customers")
    List<AddressBean> findAllCustomers();
    @GetMapping("/authentification/AddressUser/{id}")
    AddressBean findAddressByUserId(@PathVariable Long id);
    @GetMapping(value = "/billNotPaid")
    List<BillBean> findNotPaid();
    @GetMapping(value = "/billPaid")
    List<BillBean> findByPaid();
    @GetMapping(value ="/billMonth/{month}")
    double findSumBillMonth(@PathVariable int month);
    @GetMapping(value ="/billMonthNotPaid/{month}")
    double findSumBillMonthNotPaid(@PathVariable int month);
    @GetMapping(value ="/bill/{id}")
    BillBean findBillById(@PathVariable Long id);
    @GetMapping(value = "/task/get/{id}")
    TaskBean findTaskById(@PathVariable Long id);
    @GetMapping(value = "/billByUser/{idCustomer}")
    List<BillBean> findBillByUser(@PathVariable Long idCustomer);
    @GetMapping(value = "/taskByUser/{idUser}")
    List<TaskBean> getTaskByUser(@PathVariable Long idUser);
    @GetMapping(value = "/authentification/get/{id}")
    UserBean findUserById(@PathVariable Long id);
    @GetMapping("/authentification/mail/{mail}")
    AddressBean findByMail(@PathVariable String mail);
    @PostMapping("/authentification/addUser")
    ResponseEntity<UserBean> addUser(@RequestBody UserBean user);
    @PostMapping("/authentification/addAddress")
    ResponseEntity<AddressBean> addAddress(@RequestBody AddressBean address);
    @PostMapping("/addBill")
    ResponseEntity<BillBean> addBill(@RequestBody BillBean bill);
    @PostMapping("/isPaid")
    ResponseEntity<BillBean> paid(@RequestBody BillBean bill);
    @GetMapping("deleteBill/{id}")
    void deleteBill(@PathVariable Long id);
    @GetMapping("/allCustomers")
    List<UserBean> findCustomers();
    @GetMapping(value = "/messageCustomerAndLawyer/{idCustomer}/{idLawyer}")
    List<MessageBean> findMessageByCustomer(@PathVariable Long idCustomer, @PathVariable Long idLawyer);
    @PostMapping("/message/addMessage")
    ResponseEntity<MessageBean> addMessage(@RequestBody MessageBean message);
    @GetMapping(value = "/commentNotCheck/{idLawyer}")
    List<CommentBean> findCommentNotCheck(@PathVariable Long idLawyer);
    @GetMapping(value = "/commentCheck/{idLawyer}")
    List<CommentBean> findCommentCheck(@PathVariable Long idLawyer);
    @GetMapping("comment/{id}")
    CommentBean findCommentById(@PathVariable Long id);
    @PostMapping("/comment/CommentPutOnHold")
    ResponseEntity<CommentBean> putOnHoldComment(@RequestBody CommentBean comment);
    @PostMapping("/comment/updateComment")
    ResponseEntity<CommentBean> updateComment(@RequestBody CommentBean comment);
    @GetMapping("/commentDelete/{id}")
    void deleteComment(@PathVariable Long id);
    @GetMapping(value = "/contact")
    List<ContactBean> findContactByNotSeen();
    @GetMapping(value = "/contactSeen")
    List<ContactBean> findContactBySeen();
    @GetMapping(value = "/contact/{id}")
    ContactBean findContactById(@PathVariable Long id);
    @PostMapping(value = "/contactIsSeen/{id}")
    ResponseEntity<ContactBean> contactIsSeen(@PathVariable Long id);
    @GetMapping("/deleteContact/{id}")
    void deleteContact(@PathVariable Long id);
    @GetMapping("/alertBill")
    List<BillBean> alertBill();
    @GetMapping("/alertMessage/{idLawyer}")
    List<MessageBean> alertMessage(@PathVariable Long idLawyer);
    @GetMapping("/alertComment/{idLawyer}")
    List<CommentBean> alertComment(@PathVariable Long idLawyer);
    @GetMapping("/alertContact")
    List<ContactBean> alertContact();
    @GetMapping("/alertTask/{idLawyer}")
    List<TaskBean> alertTask(@PathVariable Long idLawyer);
    @PostMapping("/alertBillSeen/{id}")
    ResponseEntity<BillBean> billSeen(@PathVariable Long id);
    @PostMapping("/alertContactSeen/{id}")
    ResponseEntity<ContactBean> contactSeen(@PathVariable Long id);
    @PostMapping("/alertMessageSeen/{id}")
    ResponseEntity<MessageBean> messageSeen(@PathVariable Long id);
    @GetMapping(value = "/task/day/{date}")
    String nameDate(@PathVariable Date date);
    @GetMapping(value = "/task/NumDay/{date}")
    int numDate(@PathVariable Date date);
    @GetMapping(value = "/task/week/{date}")
    ArrayList getWeek(@PathVariable Date date);
    @GetMapping(value = "/task/getString/{date}")
    List<String> getString(@PathVariable Date date);
    @GetMapping(value = "/task/getDates/{date}")
    List<Date> getDates(@PathVariable Date date);
    @GetMapping(value = "/sizeDate")
    int getSizeDate();
    @GetMapping(value = "/tast/lastAvailable")
    Date getLastAvailable();
    @GetMapping(value = "/task/{date}")
    List<Integer> findByDate(@PathVariable Date date);
    @GetMapping("/taskInTask/{date}/{hour}/{duration}")
    List<TaskBean> findTaskInTask(@PathVariable Date date,@PathVariable int hour,@PathVariable int duration);
    @PostMapping("/task/updateTask")
    ResponseEntity<TaskBean> updateTask(@RequestBody TaskBean task);
    @GetMapping(value = "/deleteTask/{id}")
    void deleteTask(@PathVariable Long id);
    @GetMapping("/taskAccepted/{id}")
    void taskAccepted(@PathVariable Long id);
    @PostMapping("/sendMessage/{subject}/{text}/{from}/{to}")
    void sendMessage(@PathVariable String subject,@PathVariable String text,@PathVariable String from,@PathVariable String to);
}

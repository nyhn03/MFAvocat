package com.oc.microservice.dao;

import com.oc.microservice.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageDao extends JpaRepository<Message,Long> {
    /**
     * Give all message for a customer
     * @param idCustomer id of customer
     * @return liste of message
     */
    @Query("SELECT message FROM Message message WHERE message.customer.id = :idCustomer")
    List<Message> findMessageByCustomer(@Param("idCustomer") Long idCustomer);

    /**
     * Give all message for a lawyer and not seen
     * @param idLawyer id of lawyer
     * @return list of message
     */
    @Query("SELECT message FROM Message message WHERE message.lawyer.id = :idLawyer AND message.seen = false ")
    List<Message> findNewMessageByLawyer(@Param("idLawyer") Long idLawyer);

    /**
     * Give all message of customer with id lawyer order by date
     * @param idCustomer client
     * @param idLawyer lawyer
     * @return list of message
     */
    @Query("SELECT message FROM Message message WHERE  message.customer.id = :idCustomer AND message.lawyer.id = :idLawyer ORDER BY message.date")
    List<Message> findMessageByCustomerAndLawyer(@Param("idCustomer") Long idCustomer,@Param("idLawyer") Long idLawyer);
}

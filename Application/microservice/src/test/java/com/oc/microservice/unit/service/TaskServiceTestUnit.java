package com.oc.microservice.unit.service;

import com.oc.microservice.dao.TaskDao;
import com.oc.microservice.model.Task;
import com.oc.microservice.service.TaskService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTestUnit {
    @Mock
    TaskDao taskDao;

    @InjectMocks
    TaskService taskService;

    @Test
    public void sayNumDayDateTest_Date_int(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);
        Date date = new Date(cal.getTime().getTime());
        int result = new TaskService().sayNumDayDate(date);
        Assert.assertEquals(5,result);
    }

    @Test
    public void getWeekTest_Date_ArrayList(){
        List<Task> taskList1 = new ArrayList<>();
        List<Task> taskList2 = new ArrayList<>();
        List<Task> taskList3 = new ArrayList<>();
        List<Task> taskList4 = new ArrayList<>();
        List<Task> taskList5 = new ArrayList<>();
        List<Task> taskList6 = new ArrayList<>();
        List<Task> taskList7 = new ArrayList<>();

        Task task1 = new Task();
        task1.setId(1L);
        task1.setAccepted(true);
        task1.setHour(8);
        task1.setDuration(2);
        Task task2 = new Task();
        task2.setId(3L);
        task2.setAccepted(false);
        task2.setHour(15);
        task2.setDuration(1);
        Task task3 = new Task();
        task3.setId(5L);
        task3.setAccepted(true);
        task3.setHour(9);
        task3.setDuration(1);
        Task task4 = new Task();
        task4.setId(7L);
        task4.setAccepted(false);
        task4.setHour(13);
        task4.setDuration(1);
        Task task5 = new Task();
        task5.setId(11L);
        task5.setAccepted(true);
        task5.setHour(10);
        task5.setDuration(2);
        Task task6 = new Task();
        task6.setId(2L);
        task6.setAccepted(false);
        task6.setHour(17);
        task6.setDuration(1);
        Task task7 = new Task();
        task7.setId(18L);
        task7.setAccepted(true);
        task7.setHour(12);
        task7.setDuration(2);


        taskList1.add(task1);
        taskList2.add(task2);
        taskList3.add(task3);
        taskList4.add(task4);
        taskList5.add(task5);
        taskList6.add(task6);
        taskList7.add(task7);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);
        Date date1 = new Date(cal.getTime().getTime());
        cal.add(Calendar.DATE,1);
        Date date2 = new Date(cal.getTimeInMillis());
        cal.add(Calendar.DATE,1);
        Date date3 = new Date(cal.getTimeInMillis());
        cal.add(Calendar.DATE,1);
        Date date4 = new Date(cal.getTimeInMillis());
        cal.add(Calendar.DATE,1);
        Date date5 = new Date(cal.getTimeInMillis());
        cal.add(Calendar.DATE,1);
        Date date6 = new Date(cal.getTimeInMillis());
        cal.add(Calendar.DATE,1);
        Date date7 = new Date(cal.getTimeInMillis());

        Mockito.when(taskDao.findByDateOrderHour(date1)).thenReturn(taskList1);
        Mockito.when(taskDao.findByDateOrderHour(date2)).thenReturn(taskList2);
        Mockito.when(taskDao.findByDateOrderHour(date3)).thenReturn(taskList3);
        Mockito.when(taskDao.findByDateOrderHour(date4)).thenReturn(taskList4);
        Mockito.when(taskDao.findByDateOrderHour(date5)).thenReturn(taskList5);
        Mockito.when(taskDao.findByDateOrderHour(date6)).thenReturn(taskList6);
        Mockito.when(taskDao.findByDateOrderHour(date7)).thenReturn(taskList7);

        ArrayList arrayList = taskService.getWeek(date1);

        Assert.assertEquals(taskService.getNbDay(),arrayList.size());
        Assert.assertTrue(arrayList.get(3).equals(taskList4));
    }

    @Test
    public void HoursAvailableTest_ListTask_ArrayList(){
        List<Integer> list = new ArrayList<>();
        list.add(8);
        list.add(10);
        list.add(5);

        Task task = new Task();
        Task task1 = new Task();

        task.setHour(9);
        task.setDuration(2);
        task1.setHour(13);
        task1.setDuration(3);

        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        tasks.add(task1);



        List result = taskService.hoursAvailable(tasks);

        Assert.assertEquals(5,result.size());
        Assert.assertTrue(result.get(1).equals(11));
    }

    @Test
    public void initHourTest_no_ListInteger(){
        List<Integer> result = taskService.initHour();
        Assert.assertEquals(taskService.getMaxHour()-taskService.getMinHour()+1,result.size());
        Assert.assertTrue(result.get(5).equals(13));
    }
    @Test
    public void getDatesTest_date_ListDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        ArrayList result = taskService.getDates(new Date(cal.getTime().getTime()));

        cal.add(Calendar.DATE,3);

        Assert.assertEquals(taskService.getNbDay(),result.size());
        Assert.assertTrue(result.get(3).equals(new Date(cal.getTime().getTime())));
    }

    @Test
    public void getStringDateTest_date_ListString(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        ArrayList result = taskService.getStringDate(new Date(cal.getTime().getTime()));

        cal.add(Calendar.DATE,3);

        Assert.assertEquals(taskService.getNbDay(),result.size());
        Assert.assertTrue(result.get(3).equals("Dimanche 16 Mai 2021"));
    }

    @Test
    public void stringDateTest_date_string(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        String result = taskService.stringDate(new Date(cal.getTime().getTime()));

        Assert.assertTrue(result.equals("Jeudi 13 Mai 2021"));
    }
}

package com.oc.microservice.controller;

import com.oc.microservice.dao.CommentDao;
import com.oc.microservice.exception.ImpossibleAddCommentException;
import com.oc.microservice.model.Comment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class CommentController {
    @Autowired
    CommentDao commentDao;

    @GetMapping(value = "/commentCheck")
    public List<Comment> findCommentCheck(){
        log.debug("[Microservice/CommentController/commentCheck]");
        List<Comment> result = commentDao.findCommentByCheck();
        log.debug("[Microservice/CommentController/commentCheck] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/commentNotCheck/{idLawyer}")
    public List<Comment> findCommentNotCheck(@PathVariable Long idLawyer){
        log.debug("[Microservice/CommentController/commentNotCheck/{idLawyer}] with idLawyer = "+idLawyer);
        List<Comment> result = commentDao.findCommentByNotCheck(idLawyer);
        log.debug("[Microservice/CommentController/commentNotCheck/{idLawyer}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/commentCheck/{idLawyer}")
    public List<Comment> findCommentCheck(@PathVariable Long idLawyer){
        log.debug("[Microservice/CommentController/commentCheck/{idLawyer}] with idLawyer = "+idLawyer);
        List<Comment> result = commentDao.findCommentByCheck(idLawyer);
        log.debug("[Microservice/CommentController/commentCheck/{idLawyer}] Return -> \n"+result +" \n");
        return result;
    }

    @PostMapping("/comment/addComment")
    public ResponseEntity<Comment> addComment(@RequestBody Comment comment) throws ImpossibleAddCommentException {
        Comment newComment = commentDao.save(comment);
        log.debug("[Microservice/CommentController/comment/addComment] with Comment = "+newComment);
        if (newComment == null) throw new ImpossibleAddCommentException("Impossible d'ajouter le témoignage");
        return new ResponseEntity<Comment>(comment, HttpStatus.CREATED);
    }
    @GetMapping("/commentDelete/{id}")
    public void deleteComment(@PathVariable Long id){
        log.debug("[Microservice/CommentController/commentDelete/{id}] with id = "+id);
        commentDao.deleteById(id);
    }

    @PostMapping("/comment/updateComment")
    public ResponseEntity<Comment> updateComment(@RequestBody Comment comment) throws ImpossibleAddCommentException {
        comment.setValid(true);
        Comment newComment = commentDao.save(comment);
        log.debug("[Microservice/CommentController/comment/updateComment] with Comment = "+newComment);
        if (newComment == null) throw new ImpossibleAddCommentException("Impossible d'ajouter le témoignage");
        return new ResponseEntity<Comment>(comment, HttpStatus.CREATED);
    }
    @PostMapping("/comment/CommentPutOnHold")
    public ResponseEntity<Comment> putOnHoldComment(@RequestBody Comment comment) throws ImpossibleAddCommentException {
        comment.setValid(false);
        Comment newComment = commentDao.save(comment);
        log.debug("[Microservice/CommentController/comment/CommentPutOnHold] with Comment = "+newComment);
        if (newComment == null) throw new ImpossibleAddCommentException("Impossible d'ajouter le témoignage");
        return new ResponseEntity<Comment>(comment, HttpStatus.CREATED);
    }

    @GetMapping("comment/{id}")
    public Optional<Comment> findCommentById(@PathVariable Long id){
        log.debug("[Microservice/CommentController/comment/{id}] with id = "+id);
        Optional<Comment> result = commentDao.findById(id);
        log.debug("[Microservice/CommentController/comment/{id}] Return -> \n"+result +" \n");
        return result;
    }


}

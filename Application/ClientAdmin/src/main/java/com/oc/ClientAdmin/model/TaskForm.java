package com.oc.ClientAdmin.model;

import com.oc.ClientAdmin.beans.TaskBean;
import lombok.extern.slf4j.Slf4j;

/**
 * TaskForm is a form to create task
 * task for task wanted
 * idCustomer for add customer after
 * hourForced if we want add even if a task exist in a same time
 */
@Slf4j
public class TaskForm {
    public Long idCustomer;
    public TaskBean task;
    public int hourForced;

    public TaskForm() {
        log.debug("[ClientAdmin/TaskForm()] instantiation of a new TaskForm");
        task = new TaskBean();
        hourForced = -1;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public TaskBean getTask() {
        return task;
    }

    public void setTask(TaskBean task) {
        this.task = task;
    }

    public int getHourForced() {
        return hourForced;
    }

    public void setHourForced(int hourForced) {
        this.hourForced = hourForced;
    }

    @Override
    public String toString() {
        return "TaskForm{" +
                "idCustomer=" + idCustomer +
                ", task=" + task +
                ", hourForced=" + hourForced +
                '}';
    }
}

package com.oc.microservice.dao;

import com.oc.microservice.enums.Role;
import com.oc.microservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User,Long> {

    /**
     * Give user by username
     * @param username username
     * @return User
     */
    @Query("SELECT user FROM User user WHERE user.username like :username")
    User findByUsername(@Param("username") String username);

    /**
     * Give all user with role = lawyer
     * @return list of user
     */
    @Query("SELECT user FROM User user WHERE user.role ='LAWYER' ")
    List<User> findLawyer();

    /**
     * Give all user with role = client
     * @return List of User
     */
    @Query("SELECT user FROM User user WHERE user.role ='CLIENT' ")
    List<User> findAllClient();
}


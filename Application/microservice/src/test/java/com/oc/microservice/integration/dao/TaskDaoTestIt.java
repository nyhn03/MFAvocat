package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.TaskDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.enums.Reason;
import com.oc.microservice.enums.TaskType;
import com.oc.microservice.model.Task;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class TaskDaoTestIt {
    @Autowired
    TaskDao taskDao;

    @Autowired
    UserDao userDao;

    @Test
    public void saveTaskTest_task_noReturn(){
        Task task = new Task();
        task.setHour(10);
        task.setDuration(1);
        task.setAccepted(false);
        task.setCustomer(userDao.getOne(2l));
        task.setLawyer(userDao.getOne(1l));

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);

        task.setDate(new Date(cal.getTime().getTime()));
        task.setReason(Reason.ALIMONY);
        task.setType(TaskType.MEETING);
        task.setVisio(true);
        task.setLink("LienTest");

        taskDao.save(task);
        Task taskSearch = taskDao.findTaskByUser(2l).get(6);
        boolean testCheck = taskSearch.getHour() == 10
                &&  taskSearch.getDuration() == 1
                &&  !taskSearch.isAccepted()
                &&  taskSearch.getCustomer().getId().equals(task.getCustomer().getId())
                &&  taskSearch.getLawyer().getId().equals(task.getLawyer().getId())
                &&  taskSearch.getReason().equals(task.getReason())
                &&  taskSearch.getType().equals(task.getType())
                &&  taskSearch.isVisio()
                &&  taskSearch.getLink().equals(task.getLink());
        Assert.assertTrue(testCheck);
        taskDao.delete(task);
    }

    @Test
    public void findByDateTest_Date_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findByDate(date);

        Assert.assertEquals(9,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(81l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 8
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.DIVORCE)
                && tasks.get(0).getDescription().equals("description5")
                && tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien6");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findByDateOrderHourTest_Date_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findByDateOrderHour(date);

        Assert.assertEquals(9,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(81l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 8
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.DIVORCE)
                && tasks.get(0).getDescription().equals("description5")
                && tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien6");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findAllAfterDateTest_Date_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findAllAfterDate(date);

        Assert.assertEquals(9,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(81l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 8
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.DIVORCE)
                && tasks.get(0).getDescription().equals("description5")
                && tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien6");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findTaskPassedByUserTest_DateAndIdUser_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findTaskPassedByUser(date,2l);

        Assert.assertEquals(1,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(78l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 8
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.DIVORCE)
                && tasks.get(0).getDescription().equals("description1")
                && tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien1");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findTaskByUserTest_idUser_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findTaskByUser(2l);

        Assert.assertEquals(6,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(78l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 8
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.DIVORCE)
                && tasks.get(0).getDescription().equals("description1")
                && tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien1");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findTaskNotPassedByUserTest_DateAndIdUser_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findTaskNotPassedByUser(date,2l);

        Assert.assertEquals(3,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(81l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 8
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.DIVORCE)
                && tasks.get(0).getDescription().equals("description5")
                && tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien6");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findTaskNotAcceptedByUserTest_DateAndIdUser_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findTaskNotAcceptedByUser(date,2l);

        Assert.assertEquals(1,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(88l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 16
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.CONSTRUCTION)
                && tasks.get(0).getDescription().equals("description12")
                && !tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(2l)
                && tasks.get(0).isVisio()
                && tasks.get(0).getLink().equals("lien12");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findTaskNotAcceptedByLawyerTest_DateAndIdLawyer_ListTask(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        Date date = new Date(cal.getTime().getTime());

        List<Task> tasks = taskDao.findTaskNotAcceptedByLawyer(date,1l);

        Assert.assertEquals(4,tasks.size());
        boolean testCheck = tasks.get(0).getId().equals(86l)
                && tasks.get(0).getType().equals(TaskType.MEETING)
                && tasks.get(0).getHour() == 14
                && tasks.get(0).getDuration() == 1
                && tasks.get(0).getReason().equals(Reason.CHILD_CARE)
                && tasks.get(0).getDescription().equals("description10")
                && !tasks.get(0).isAccepted()
                && tasks.get(0).getLawyer().getId().equals(1l)
                && tasks.get(0).getCustomer().getId().equals(3l)
                && !tasks.get(0).isVisio();
        Assert.assertTrue(testCheck);
    }
}

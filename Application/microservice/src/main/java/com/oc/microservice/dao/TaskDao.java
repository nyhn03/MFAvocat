package com.oc.microservice.dao;

import com.oc.microservice.model.Task;
import com.oc.microservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface TaskDao extends JpaRepository<Task,Long> {
    /**
     * Give all tasks for a Date
     * @param date date
     * @return list of task
     */
    @Query("SELECT task FROM Task task WHERE task.date = :date")
    List<Task> findByDate(@Param("date") Date date);

    /**
     * Give all task for a date order by hour
     * @param date date
     * @return list of task
     */
    @Query("SELECT task FROM Task task WHERE task.date = :date order by task.hour")
    List<Task> findByDateOrderHour(@Param("date") Date date);

    /**
     * Give all task with date is greater than date wanted order by date
     * @param date date
     * @return list of task
     */
    @Query("SELECT task FROM Task task WHERE task.date > :date ORDER BY task.date")
    List<Task> findAllAfterDate(@Param("date") Date date);

    /**
     * Give all meeting  is less than Date by customer order by date
     * @param date date
     * @param idUser id of customer
     * @return List of task
     */
    @Query("SELECT task FROM Task task WHERE task.date < :date AND task.customer.id = :idUser AND task.type = 'MEETING' ORDER BY task.date")
    List<Task> findTaskPassedByUser(@Param("date") Date date,@Param("idUser") Long idUser);

    /**
     * Give all task by customer
     * @param idUser id of customer
     * @return List of Task
     */
    @Query("SELECT task FROM Task task WHERE task.customer.id = :idUser")
    List<Task> findTaskByUser(@Param("idUser") Long idUser);

    /**
     * Give all meeting is greater than date by customer and accepted order by date
     * @param date date
     * @param idUser id of customer
     * @return list of task
     */
    @Query("SELECT task FROM Task task WHERE task.date > :date AND task.customer.id = :idUser AND task.type = 'MEETING' AND task.accepted = true ORDER BY task.date")
    List<Task> findTaskNotPassedByUser(@Param("date") Date date,@Param("idUser") Long idUser);

    /**
     * Give all meeting is greater than date by customer and not accepted order by date
     * @param date date
     * @param idUser id of customer
     * @return list of task
     */
    @Query("SELECT task FROM Task task WHERE task.date > :date AND task.customer.id = :idUser AND task.type = 'MEETING' AND task.accepted = false ORDER BY task.date")
    List<Task> findTaskNotAcceptedByUser(@Param("date") Date date,@Param("idUser") Long idUser);

    /**
     * Give all meeting is greater than date by lawyer and not accepted order by date
     * @param date date
     * @param idLawyer id of lawyer
     * @return list of Task
     */
    @Query("SELECT task FROM Task task WHERE task.date > :date AND task.lawyer.id = :idLawyer AND task.type = 'MEETING' AND task.accepted = false ORDER BY task.date")
    List<Task> findTaskNotAcceptedByLawyer(@Param("date") Date date,@Param("idLawyer") Long idLawyer);
}
package com.oc.ClientAdmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.oc.ClientAdmin")
public class ClientAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientAdminApplication.class, args);
	}

}

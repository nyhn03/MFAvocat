package com.oc.ClientWeb.beans;

import lombok.extern.slf4j.Slf4j;

import java.sql.Date;

/**
 * i.e Message of microservice
 */
@Slf4j
public class MessageBean {
    private Long id;
    private Date date;
    private String description;
    private UserBean lawyer;
    private UserBean customer;
    // true for a message sent by customer, and false for lawyer
    private boolean type;
    private boolean seen;

    public MessageBean() {
        seen = false;
        type = true;
        log.debug("[ClientWeb/MessageBean()] instantiation of a new messageBean");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserBean getLawyer() {
        return lawyer;
    }

    public void setLawyer(UserBean lawyer) {
        this.lawyer = lawyer;
    }

    public UserBean getCustomer() {
        return customer;
    }

    public void setCustomer(UserBean customer) {
        this.customer = customer;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "MessageBean{" +
                "id=" + id +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", lawyer=" + lawyer +
                ", customer=" + customer +
                ", type=" + type +
                ", seen=" + seen +
                '}';
    }
}

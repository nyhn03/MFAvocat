package com.oc.ClientWeb.service;

import com.oc.ClientWeb.beans.ContactBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * ContactFormCheck is characterized by
 * boolean for contact attribute
 */
@Slf4j
@Service
public class ContactFormCheck {
    private static boolean nameSize;
    private static boolean nameEmpty;
    private static boolean mailSize;
    private static boolean mailEmpty;
    private static boolean subjectSize;
    private static boolean subjectEmpty;
    private static boolean messageSize;
    private static boolean messageEmpty;

    public ContactFormCheck() {
        log.debug("[ClientWeb/ContactFormCheck/ContactFormCheck()] instanciation");
        init();
    }

    /**
     * Make all boolean to false
     */
    public void init(){
        log.debug("[ClientWeb/ContactFormCheck/init()]");
        nameSize = false;
        nameEmpty = false;
        mailSize = false;
        mailEmpty = false;
        subjectSize = false;
        subjectEmpty = false;
        messageSize = false;
        messageEmpty = false;
    }

    /**
     * This function initialize attributes
     * And evaluate all contact attributes
     * @param contact contact
     */
    public void evaluate(ContactBean contact){
        log.debug("[ClientWeb/ContactFormCheck/evaluate(ContactBean)] with contact = "+contact);
        init();
        nameCheck(contact.getName());
        mailCheck(contact.getMail());
        subjectCheck(contact.getSubject());
        messageCheck(contact.getMessage());
    }

    /**
     * Validate return false if one of boolean is true else return true
     * @return Boolean of acceptance
     */
    public static boolean validate() {
        log.debug("[ClientWeb/ContactFormCheck/validate()] ");
        if (nameSize)
            return false;
        if (nameEmpty)
            return false;
        if (mailEmpty)
            return false;
        if (mailSize)
            return false;
        if (subjectEmpty)
            return false;
        if (subjectSize)
            return false;
        if (messageEmpty)
            return false;
        if (messageSize)
            return false;
        log.debug("[ClientWeb/ContactFormCheck/validate()] Return -> true");
        return true;
    }

    /**
     * check name
     * If is not null and between 3 and 50 characters
     * @param name name
     */
    public void nameCheck(String name){
        log.debug("[ClientWeb/ContactFormCheck/nameCheck(String)] with name = "+name);
        if(name.length() == 0)
            nameEmpty = true;
        if(name.length() <= 3 || name.length() >= 50)
            nameSize = true;
    }

    /**
     * check mail
     * If is not null and between 3 and 50 characters
     * @param mail mail
     */
    public void mailCheck(String mail){
        log.debug("[ClientWeb/ContactFormCheck/mailCheck(String)] with mail = "+mail);
        if(mail.length() == 0)
            mailEmpty = true;
        if(mail.length() <= 3 || mail.length() >= 50)
            mailSize = true;
    }

    /**
     * check subject
     * If is not null and between 3 and 100 characters
     * @param subject subject
     */
    public void subjectCheck(String subject){
        log.debug("[ClientWeb/ContactFormCheck/subjectCheck(String)] with subject = "+subject);
        if(subject.length() == 0)
            subjectEmpty = true;
        if(subject.length() <= 3 || subject.length() >= 100)
            subjectSize = true;
    }

    /**
     * check message
     * If is not null and between 3 and 1000 characters
     * @param message message
     */
    public void messageCheck(String message){
        log.debug("[ClientWeb/ContactFormCheck/messageCheck(String)] with message = "+message);
        if(message.length() == 0)
            messageEmpty = true;
        if(message.length() <= 3 || message.length() >= 1000)
            messageSize = true;
    }

    public static boolean isNameSize() {
        return nameSize;
    }

    public static void setNameSize(boolean nameSize) {
        ContactFormCheck.nameSize = nameSize;
    }

    public static boolean isNameEmpty() {
        return nameEmpty;
    }

    public static void setNameEmpty(boolean nameEmpty) {
        ContactFormCheck.nameEmpty = nameEmpty;
    }

    public static boolean isMailSize() {
        return mailSize;
    }

    public static void setMailSize(boolean mailSize) {
        ContactFormCheck.mailSize = mailSize;
    }

    public static boolean isMailEmpty() {
        return mailEmpty;
    }

    public static void setMailEmpty(boolean mailEmpty) {
        ContactFormCheck.mailEmpty = mailEmpty;
    }

    public static boolean isSubjectSize() {
        return subjectSize;
    }

    public static void setSubjectSize(boolean subjectSize) {
        ContactFormCheck.subjectSize = subjectSize;
    }

    public static boolean isSubjectEmpty() {
        return subjectEmpty;
    }

    public static void setSubjectEmpty(boolean subjectEmpty) {
        ContactFormCheck.subjectEmpty = subjectEmpty;
    }

    public static boolean isMessageSize() {
        return messageSize;
    }

    public static void setMessageSize(boolean messageSize) {
        ContactFormCheck.messageSize = messageSize;
    }

    public static boolean isMessageEmpty() {
        return messageEmpty;
    }

    public static void setMessageEmpty(boolean messageEmpty) {
        ContactFormCheck.messageEmpty = messageEmpty;
    }

}

package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.MessageDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.model.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class MessageDaoTestIt {
    @Autowired
    MessageDao messageDao;
    @Autowired
    UserDao userDao;

    @Test
    public void saveMessageTest_message_noReturn(){
        Message message = new Message();
        message.setCustomer(userDao.getOne(1l));
        message.setLawyer(userDao.getOne(1l));

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        message.setDate(new Date(cal.getTime().getTime()));
        message.setSeen(false);
        message.setDescription("Ajout test");
        message.setType(false);

        messageDao.save(message);
        Message messageSearch = messageDao.findMessageByCustomerAndLawyer(1l,1l).get(0);

        boolean testCheck = !messageSearch.isSeen()
                && !messageSearch.isType()
                && messageSearch.getCustomer().getId().equals(message.getCustomer().getId())
                && messageSearch.getLawyer().getId().equals(message.getLawyer().getId())
                && messageSearch.getDescription().equals(message.getDescription());

        Assert.assertTrue(testCheck);
        messageDao.delete(message);
    }

    @Test
    public void findMessageByCustomerTest_idCustomer_ListMessage(){
        List<Message> messages = messageDao.findMessageByCustomer(2l);
        Assert.assertEquals(3,messages.size());
        boolean testCheck = messages.get(0).getId().equals(17l)
                && messages.get(0).getDescription().equals("message1")
                && messages.get(0).getLawyer().getId().equals(1l)
                && messages.get(0).getCustomer().getId().equals(2l)
                && !messages.get(0).isType()
                && messages.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findNewMessageByLawyerTest_idLawyer_ListMessage(){
        List<Message> messages = messageDao.findNewMessageByLawyer(1l);
        Assert.assertEquals(3,messages.size());
        boolean testCheck = messages.get(0).getId().equals(18l)
                && messages.get(0).getDescription().equals("message2")
                && messages.get(0).getLawyer().getId().equals(1l)
                && messages.get(0).getCustomer().getId().equals(3l)
                && messages.get(0).isType()
                && !messages.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findMessageByCustomerAndLawyer_idCustomerAndIdLawyer_ListMessage(){
        List<Message> messages = messageDao.findMessageByCustomerAndLawyer(2l,1l);
        Assert.assertEquals(3,messages.size());
        boolean testCheck = messages.get(0).getId().equals(17l)
                && messages.get(0).getDescription().equals("message1")
                && messages.get(0).getLawyer().getId().equals(1l)
                && messages.get(0).getCustomer().getId().equals(2l)
                && !messages.get(0).isType()
                && messages.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }
}

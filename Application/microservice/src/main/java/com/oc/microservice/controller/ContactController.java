package com.oc.microservice.controller;

import com.oc.microservice.dao.ContactDao;
import com.oc.microservice.exception.ImpossibleAddContactException;
import com.oc.microservice.model.Contact;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class ContactController {
    @Autowired
    ContactDao contactDao;

    @GetMapping(value = "/contact")
    public List<Contact> findContactByNotSeen(){
        log.debug("[Microservice/ContactController/contact]");
        List<Contact> result = contactDao.findContactByNotSeen();
        log.debug("[Microservice/ContactController/contact] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/contactSeen")
    public List<Contact> findContactBySeen(){
        log.debug("[Microservice/ContactController/contactSeen]");
        List<Contact> result = contactDao.findContactBySeen();
        log.debug("[Microservice/ContactController/contactSeen] Return -> \n"+result +" \n");
        return result;
    }

    @PostMapping("/contact/addContact")
    public ResponseEntity<Contact> addContact(@RequestBody Contact contact) throws ImpossibleAddContactException {
        Contact newContact = contactDao.save(contact);
        log.debug("[Microservice/ContactController/contact/addContact] with Contact = "+newContact);
        if (newContact == null) throw new ImpossibleAddContactException("Impossible d'ajouter le Contact");
        return new ResponseEntity<Contact>(contact, HttpStatus.CREATED);
    }
    @GetMapping(value = "/contact/{id}")
    public Optional<Contact> findContactById(@PathVariable Long id){
        log.debug("[Microservice/ContactController/contact/{id}] with id = "+id);
        Optional<Contact> result = contactDao.findById(id);
        log.debug("[Microservice/ContactController/contact/{id}] Return -> \n"+result +" \n");
        return result;
    }

    /**
     * transform contact attribute seen to true
     * @param id of contact
     * @return ResponseEntity
     * @throws ImpossibleAddContactException  ImpossibleAddContactException
     */
    @PostMapping(value = "/contactIsSeen/{id}")
    public ResponseEntity<Contact> contactIsSeen(@PathVariable Long id) throws ImpossibleAddContactException {
        Optional<Contact> contact = contactDao.findById(id);
        contact.get().setSeen(true);
        Contact newContact = contactDao.save(contact.get());
        log.debug("[Microservice/ContactController/contactIsSeen/{id}] with id = "+id+" and Contact = "+newContact);
        if (newContact == null) throw new ImpossibleAddContactException("Impossible d'ajouter le Contact");
        return new ResponseEntity<Contact>(contact.get(), HttpStatus.CREATED);
    }
    @GetMapping("/deleteContact/{id}")
    public void deleteContact(@PathVariable Long id){
        log.debug("[Microservice/ContactController/deleteContact/{id}] with id = "+id);
        contactDao.deleteById(id);
    }
}

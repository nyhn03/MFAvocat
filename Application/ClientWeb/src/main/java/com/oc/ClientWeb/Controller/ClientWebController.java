package com.oc.ClientWeb.Controller;

import com.oc.ClientWeb.beans.*;
import com.oc.ClientWeb.enums.Role;
import com.oc.ClientWeb.enums.TaskType;
import com.oc.ClientWeb.model.MessageForm;
import com.oc.ClientWeb.model.RegisterForm;
import com.oc.ClientWeb.model.TaskForm;
import com.oc.ClientWeb.proxies.MicroserviceProxy;
import com.oc.ClientWeb.service.ContactFormCheck;
import com.oc.ClientWeb.service.EncryptPassword;
import com.oc.ClientWeb.service.RegisterFormCheck;
import com.oc.ClientWeb.service.TaskFormCheck;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Slf4j
@Controller
public class ClientWebController {
    @Autowired
    MicroserviceProxy microserviceProxy;

    @Autowired
    RegisterFormCheck registerFormCheck;

    @Autowired
    TaskFormCheck taskFormCheck;

    @Autowired
    ContactFormCheck contactFormCheck;

    @Autowired
    EncryptPassword encryptPassword;

    public static String days[] = {"samedi","dimanche","lundi","mardi","mercredi","jeudi","vendredi"};

    @RequestMapping("/")
    public String homepage(){
        log.debug("[ClientWeb/ClientWebController/]");
        return "redirect:/index";
    }
    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        ContactBean contact = new ContactBean();
        model.addAttribute("contact",contact);
        List<CommentBean> comments = microserviceProxy.findCommentCheck();
        model.addAttribute("comments",comments);
        log.debug("[ClientWeb/ClientWebController/index] with userCurrent = "+userCurrent+" and contact = "+contact);
        return "index";
    }
    @RequestMapping("/aboutMe")
    public String aboutMe(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        List<CommentBean> comments = microserviceProxy.findCommentCheck();
        model.addAttribute("comments",comments);
        log.debug("[ClientWeb/ClientWebController/aboutMe] with userCurrent = "+userCurrent);
        return "aboutMe";
    }
    @RequestMapping("/account")
    public String account(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        List<TaskBean> tasks = microserviceProxy.getTaskNotPassed(userCurrent.getId());
                TaskBean task = null;
        if(tasks.size()>0){
            task = tasks.get(0);
        }
        model.addAttribute("task",task);
        log.debug("[ClientWeb/ClientWebController/account] with userCurrent = "+ userCurrent +" and task = "+task);
        return "account";
    }
    @RequestMapping("/accountInformation")
    public String accountInformation(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        AddressBean address = microserviceProxy.findAddressByUserId(userCurrent.getId());
        model.addAttribute("address",address);
        log.debug("[ClientWeb/ClientWebController/accountInformation] with userCurrent = "+ userCurrent +" and address = "+address);
        return "accountInformation";
    }
    @RequestMapping("/accountInformationEdit")
    public ModelAndView accountInformationEdit(HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        ModelAndView modelAndView = new ModelAndView("accountInformationEdit");
        AddressBean address = microserviceProxy.findAddressByUserId(userCurrent.getId());
        modelAndView.addObject("userCurrent",userCurrent);
        modelAndView.addObject("registerFormCheck",registerFormCheck);
        RegisterForm registerForm = new RegisterForm();
        registerForm.setUser(userCurrent);
        registerForm.setAddress(address);
        modelAndView.addObject("registerForm",registerForm);
        log.debug("[ClientWeb/ClientWebController/accountInformationEdit] with userCurrent = "+ userCurrent +" and address = "+address);
        return modelAndView;
    }
    @RequestMapping(value = "/accountInformationEditCheck" ,method = RequestMethod.POST)
    public String checkUserAndAddressEdit(Model model, HttpServletRequest request, @ModelAttribute("registerForm") RegisterForm registerForm){
        registerFormCheck.evaluateEdit(registerForm);
        log.debug("[ClientWeb/ClientWebController/accountInformationEditCheck] Result = "+ registerFormCheck.validate());
        if (registerFormCheck.validate()){
            UserBean userCurrent = registerForm.getUser();
            AddressBean address = registerForm.getAddress();
            address.setUser(userCurrent);
            microserviceProxy.addUser(userCurrent);
            microserviceProxy.addAddress(address);
            return "redirect:/accountInformation";
        }
        return "redirect:/accountInformationEdit";
    }
    @RequestMapping("/appointment/{id}")
    public String appointment(Model model, HttpServletRequest request,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/appointment/{id}] with id = "+ id + " and userCurrent = "+userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED )
            return "redirect:/logIn";
        model.addAttribute("today",new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,2);
        model.addAttribute("date48",new java.sql.Date(calendar.getTime().getTime()));
        TaskBean task = microserviceProxy.findTaskById(id);
        if(userCurrent.getId() != task.getCustomer().getId())
            return "redirect:/appointments";
        model.addAttribute("task",task);
        log.debug("[ClientWeb/ClientWebController/appointment/{id}] with task = "+ task );
        return "appointment";
    }
    @RequestMapping("/appointmentEdit/{id}")
    public ModelAndView appointmentEdit(HttpServletRequest request,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/appointmentEdit/{id}] with id = "+ id + " and userCurrent = "+userCurrent);
        ModelAndView modelAndView = new ModelAndView("appointmentEdit");
        TaskBean task = microserviceProxy.findTaskById(id);
        modelAndView.addObject("userCurrent",userCurrent);
        modelAndView.addObject("task",task);
        log.debug("[ClientWeb/ClientWebController/appointmentEdit/{id}] with task = "+ task );
        return modelAndView;
    }
    @RequestMapping("/appointmentEditCheck")
    public String appointmentEditCheck(HttpServletRequest request,@ModelAttribute TaskBean task){
        log.debug("[ClientWeb/ClientWebController/appointmentEditCheck] with task = "+ task);
        task.setLawyer(microserviceProxy.findUserById(task.getLawyer().getId()));
        task.setCustomer(getUserSession(request));
        if (task.getDescription().length() < 500 && task.getDate().after(new java.sql.Date(Calendar.getInstance().getTime().getTime())))
        {
            microserviceProxy.updateTask(task);
        }
        return "redirect:/appointments";
    }
    @RequestMapping(value = "/deleteTask/{id}")
    public String deleteTask(@PathVariable(name = "id") Long id){
        log.debug("[ClientWeb/ClientWebController/deleteTask/{id}] with id = "+ id);
        microserviceProxy.deleteTask(id);
        return "redirect:/appointments";
    }
    @RequestMapping("/appointments")
    public String appointments(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/appointments] with userCurrent = "+userCurrent);
        List<TaskBean> taskPassed = microserviceProxy.getTaskPassed(userCurrent.getId());
        List<TaskBean> taskNotPassed = microserviceProxy.getTaskNotPassed(userCurrent.getId());
        List<TaskBean> taskNotAccepted = microserviceProxy.getTaskNotAccepted(userCurrent.getId());
        model.addAttribute("taskNotPassed",taskNotPassed);
        model.addAttribute("taskNotAccepted",taskNotAccepted);
        model.addAttribute("taskPassed",taskPassed);
        log.debug("[ClientWeb/ClientWebController/appointments] with taskNotPassed = "+taskNotPassed);
        log.debug("[ClientWeb/ClientWebController/appointments] with taskNotAccepted = "+taskNotAccepted);
        log.debug("[ClientWeb/ClientWebController/appointments] with taskNotPassed = "+taskPassed);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        return "appointments";
    }
    @RequestMapping("/bill/{id}")
    public String bill(Model model, HttpServletRequest request,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/bill/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        BillBean bill = microserviceProxy.findBillById(id);
        if(bill == null || bill.getCustomer().getId() != userCurrent.getId())
            return "redirect:/bills";
        model.addAttribute("bill",bill);
        log.debug("[ClientWeb/ClientWebController/bill/{id}] with bill = "+bill);
        return "bill";
    }
    @RequestMapping("/bills")
    public String bills(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/bills] with userCurrent = "+userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        model.addAttribute("billsNotPaid",microserviceProxy.findByCustomerAndNotPaid(userCurrent.getId()));
        model.addAttribute("billsPaid",microserviceProxy.findByCustomerAndPaid(userCurrent.getId()));
        return "bills";
    }
    @RequestMapping("/contact")
    public String contact(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/contact] with userCurrent = "+userCurrent);
        ContactBean contact = new ContactBean();
        model.addAttribute("contact",contact);
        model.addAttribute("contactFormCheck",contactFormCheck);

        return "contact";
    }
    @RequestMapping(value = "/contactCheck", method = RequestMethod.POST)
    public String contactCheck(Model model, HttpServletRequest request,@ModelAttribute("contact") ContactBean contact){
        log.debug("[ClientWeb/ClientWebController/contactCheck] with contact = "+contact);
        contactFormCheck.evaluate(contact);
        if(contactFormCheck.validate()){
            microserviceProxy.addContact(contact);
            return "redirect:/index";
        }
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        model.addAttribute("contact",contact);
        model.addAttribute("contactFormCheck",contactFormCheck);
        return "contact";
    }
    @RequestMapping("/faq")
    public String faq(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/faq] with userCurrent = "+userCurrent);
        ContactBean contact = new ContactBean();
        model.addAttribute("contact",contact);
        return "faq";
    }
    @RequestMapping("/logIn")
    public String login(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent", userCurrent);
        log.debug("[ClientWeb/ClientWebController/logIn] with userCurrent = "+userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "logIn";
        return "redirect:/index";
    }
    @RequestMapping(value = "/logInCheck", method = RequestMethod.POST)
    public String logInCheckPage(HttpServletRequest request, Model model, @ModelAttribute("user") UserBean user){
        log.debug("[ClientWeb/ClientWebController/logInCheck] with user = "+user);
        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(10*60*60);
        UserBean userCurrent = (UserBean) session.getAttribute("userCurrent");
        user.setPassword(encryptPassword.encrypt(user));
        if (userCurrent == null) {
            UserBean userSearch = microserviceProxy.findByUsername(user.getUsername());
            if(userSearch != null){
                if (user.getPassword().equals(userSearch.getPassword())) {
                    model.addAttribute("userCurrent", userSearch);
                    session.setAttribute("userClientCurrent", userSearch);
                    return "redirect:/index";
                }
            }
            model.addAttribute("userCurrent", new UserBean());
            return "logIn";
        }
        return "redirect:/";

    }
    @RequestMapping("/logOut")
    public String logoutAndViewHomepage( HttpServletRequest request) {
        log.debug("[ClientWeb/ClientWebController/logOut]");
        HttpSession session = request.getSession();
        session.removeAttribute("userClientCurrent");
        return "redirect:/index";
    }
    @RequestMapping("/message")
    public String message(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/message] with userCurrent = "+userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        List<MessageBean> messageList = microserviceProxy.findMessageByCustomer(userCurrent.getId());
        List<UserBean> lawyerList = microserviceProxy.findLawyer();
        model.addAttribute("lawyerList",lawyerList);
        model.addAttribute("messageList",messageList);
        model.addAttribute("newMessage",new MessageForm());
        return "message";
    }

    @RequestMapping(value = "/messageCheck", method = RequestMethod.POST)
    public String messageCheck(@ModelAttribute("newMessage") MessageForm messageForm,HttpServletRequest request){
        log.debug("[ClientWeb/ClientWebController/messageCheck] with messageForm = "+messageForm);
        if(messageForm.getMessage().getDescription().length()>=3 && messageForm.getMessage().getDescription().length()<=1000){
            UserBean userCurrent = getUserSession(request);
            messageForm.getMessage().setCustomer(userCurrent);
            messageForm.getMessage().setLawyer(microserviceProxy.findUserById(messageForm.getIdLawyer()));
            messageForm.getMessage().setDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            microserviceProxy.addMessage(messageForm.getMessage());
        }
        return "redirect:/message";
    }

    @RequestMapping(value = "/paid/{id}")
    public String paid(@PathVariable Long id, Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/paid/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("bill",microserviceProxy.findBillById(id));
        return "paid";
    }

    @RequestMapping(value = "/isPaid/{id}")
    public String isPaid(@PathVariable Long id){
        log.debug("[ClientWeb/ClientWebController/isPaid/{id}] with id = "+id);
        BillBean bill = microserviceProxy.findBillById(id);
        bill.setPaid(true);
        microserviceProxy.paid(bill);
        return "redirect:/bills";
    }


    @RequestMapping("/practice")
    public String practice(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/practice] with userCurrent = "+userCurrent);
        return "practice";
    }
    @RequestMapping("/practiceSinglePage")
    public String praticeSinglePage(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/practiceSinglePage] with userCurrent = "+userCurrent);
        return "practiceSinglePage";
    }
    @RequestMapping("/rdv")
    public String rdv(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/rdv] with userCurrent = "+userCurrent);
        if(userCurrent.getRole() == Role.NOT_CONNECTED)
            return "redirect:/logIn";
        model.addAttribute("userCurrent",userCurrent);
        java.sql.Date today = new java.sql.Date(new Date().getTime());
        ArrayList hoursAvailable = microserviceProxy.getWeek(today);
        List<java.sql.Date> dates = microserviceProxy.getDates(today);
        int numDay = microserviceProxy.numDate(today);
        java.sql.Date actualDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("actualDate",actualDate);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("today",today);
        model.addAttribute("stringDate",microserviceProxy.getString(today));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }
    @RequestMapping("/rdvNextAvailable")
    public String rdvNextAvailable(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/rdvNextAvailable] with userCurrent = "+userCurrent);
        java.sql.Date NextDate = microserviceProxy.getLastAvailable();
        ArrayList hoursAvailable = microserviceProxy.getWeek(NextDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(NextDate);
        int numDay = microserviceProxy.numDate(NextDate);
        java.sql.Date actualDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        model.addAttribute("actualDate",actualDate);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("today",NextDate);
        model.addAttribute("stringDate",microserviceProxy.getString(NextDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvNext/{date}")
    public String rdvNextByDate(Model model, HttpServletRequest request, @PathVariable(name = "date") java.sql.Date date){
        java.sql.Date actualDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/rdvNext/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,microserviceProxy.getSizeDate());
        java.sql.Date NextDate = new java.sql.Date(cal.getTimeInMillis());
        ArrayList hoursAvailable = microserviceProxy.getWeek(NextDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(NextDate);
        int numDay = microserviceProxy.numDate(NextDate);
        model.addAttribute("actualDate",actualDate);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("today",NextDate);
        model.addAttribute("stringDate",microserviceProxy.getString(NextDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvLast/{date}")
    public String rdvLastByDate(Model model, HttpServletRequest request, @PathVariable(name = "date") java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent",userCurrent);
        log.debug("[ClientWeb/ClientWebController/rdvLast/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,-microserviceProxy.getSizeDate());
        java.sql.Date nextDate = new java.sql.Date(cal.getTimeInMillis());
        java.sql.Date actualDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if(nextDate.before(actualDate)){
            return "redirect:/rdv";
        }
        ArrayList hoursAvailable = microserviceProxy.getWeek(nextDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(nextDate);
        int numDay = microserviceProxy.numDate(nextDate);
        model.addAttribute("actualDate",actualDate);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("today",nextDate);
        model.addAttribute("stringDate",microserviceProxy.getString(nextDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping(value = "/rdvDay/{date}")
    public ModelAndView showRdvDayPage(HttpServletRequest request, @PathVariable(name = "date") java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/rdvDay/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        ModelAndView modelAndView = new ModelAndView("rdvDay");
        List<Integer> hoursAvailable = microserviceProxy.findByDate(date);
        String dateString = microserviceProxy.nameDate(date);
        TaskForm taskForm = new TaskForm();
        List<UserBean> lawyerList = microserviceProxy.findLawyer();
        modelAndView.addObject("hoursAvailable",hoursAvailable);
        modelAndView.addObject("date",date);
        modelAndView.addObject("dateString",dateString);
        modelAndView.addObject("userCurrent",userCurrent);
        modelAndView.addObject("lawyerList",lawyerList);
        modelAndView.addObject("taskForm",taskForm);
        modelAndView.addObject("taskFormCheck",taskFormCheck);
        return modelAndView;
    }

    @PostMapping(value = "/taskCheck")
    public ModelAndView taskCheckAndSaveTask(Model model, HttpServletRequest request, @ModelAttribute("taskForm") TaskForm taskForm){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/taskCheck] with userCurrent = "+userCurrent);
        ModelAndView modelAndView = new ModelAndView(index(model,request));
        modelAndView.addObject("userCurrent",userCurrent);
        taskForm.getTask().setCustomer(userCurrent);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        if (taskForm.getTask().getDate().before(date)){
            modelAndView = new ModelAndView(rdv(model,request));
            return modelAndView;
        }
        if(taskForm.getIdLawyer() != null) {
            taskForm.getTask().setLawyer(microserviceProxy.findUserById(taskForm.getIdLawyer()));
        }
        taskFormCheck.init();
        taskFormCheck.evaluate(taskForm,microserviceProxy.findUserById(taskForm.getIdLawyer()),userCurrent);
        if(taskFormCheck.validate()) {
            taskForm.getTask().setAccepted(false);
            taskForm.getTask().setDuration(1);
            taskForm.getTask().setType(TaskType.MEETING);
            microserviceProxy.addTask(taskForm.getTask());
            TaskBean task = taskForm.getTask();
            String text;
            text="Vous venez de prendre rendez-vous le " +
                    task.getDate() +" de "+ task.getHour()+"h00 à "+(task.getHour()+task.getDuration())+"h00."+
                    " Celui-ci a bien été pris en compte et est en attente d'acceptation! \n" +" Voici un récapitulatif : \n"+
                    "\nJour : "+task.getDate()+
                    "\nHeure : "+task.getHour()+"h00 "+
                    "\nDurée : "+task.getDuration()+"h00 "+
                    "\nRaison : "+task.getReason()+
                    "\nDescription : "+task.getDescription()+
                    "\nAvocat : "+task.getLawyer().getFirstname()+" "+task.getLawyer().getName();
            if(task.isVisio()){
                text = text + "\nEn visio-conférence";
                    }
            else {
                text= text +"\nEn présentiel au ---Address---";
            }
            text = text+"\nCordialement," + task.getLawyer().getFirstname()+" "+task.getLawyer().getName();

            String customer = microserviceProxy.findAddressByUserId(task.getCustomer().getId()).getMail();
            String lawyer = microserviceProxy.findAddressByUserId(task.getLawyer().getId()).getMail();
            microserviceProxy.sendMessage("Demande de Rendez-vous le "+task.getDate(),text,customer,lawyer);
            return new ModelAndView("redirect:/appointments");
        }
        modelAndView = new ModelAndView("rdvDay");
        List<Integer> hoursAvailable = microserviceProxy.findByDate(taskForm.getTask().getDate());
        String dateString = microserviceProxy.nameDate(taskForm.getTask().getDate());
        List<UserBean> lawyerList = microserviceProxy.findLawyer();
        modelAndView.addObject("hoursAvailable",hoursAvailable);
        modelAndView.addObject("date",taskForm.getTask().getDate());
        modelAndView.addObject("dateString",dateString);
        modelAndView.addObject("lawyerList",lawyerList);
        modelAndView.addObject("taskForm",taskForm);
        modelAndView.addObject("taskFormCheck",taskFormCheck);
        return modelAndView;
    }
    @RequestMapping("/register")
    public String register(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/register] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent",userCurrent);
        if (userCurrent.getRole() != Role.NOT_CONNECTED)
            return "redirect:/index";
        RegisterForm registerForm = new RegisterForm();
        model.addAttribute("registerForm",registerForm);
        model.addAttribute("registerFormCheck",registerFormCheck);
        return "register";
    }
    @RequestMapping("/registerCheck")
    public String viewRegisterCheckPageAndSaveUserAddress(Model model, HttpServletRequest request, @ModelAttribute("registerForm") RegisterForm registerForm) {
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/registerCheck] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        registerForm.getUser().setRole(Role.CLIENT);
        registerFormCheck.init();
        if(registerForm.getUser().getUsername() == ""){
            registerFormCheck.setPseudoEmpty(true);
            registerFormCheck.setPseudoSize(true);
            model.addAttribute("registerFormCheck", registerFormCheck);;
            model.addAttribute("registerForm",registerForm);
            return "register";
        }
        if(registerForm.getAddress().getMail() == ""){
            registerFormCheck.setMailEmpty(true);
            registerFormCheck.setMailSize(true);
            model.addAttribute("registerFormCheck", registerFormCheck);
            model.addAttribute("registerForm",registerForm);
            return "register";
        }
        registerFormCheck.evaluate(registerForm,request,microserviceProxy.findByUsername(registerForm.getUser().getUsername()),microserviceProxy.findByMail(registerForm.getAddress().getMail()));
        if (RegisterFormCheck.validate()) {
            registerForm.getUser().setPassword(encryptPassword.encrypt(registerForm.getUser()));
            microserviceProxy.addUser(registerForm.getUser());
            registerForm.getAddress().setUser(microserviceProxy.findByUsername(registerForm.getUser().getUsername()));
            microserviceProxy.addAddress(registerForm.getAddress());
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(10*60*60);
            session.removeAttribute("userClientCurrent");
            registerForm.setUser(microserviceProxy.findByUsername(registerForm.getUser().getUsername()));
            session.setAttribute("userClientCurrent", registerForm.getUser());
            return "redirect:/index";
        } else {
            model.addAttribute("registerFormCheck", registerFormCheck);
            model.addAttribute("registerForm",registerForm);
            return "register";
        }
    }
    @RequestMapping("/testimonials")
    public String testimonials(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/testimonials] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent",userCurrent);
        model.addAttribute("comments",microserviceProxy.findCommentCheck());
        model.addAttribute("lawyerList",microserviceProxy.findLawyer());
        model.addAttribute("newMessage",new MessageForm());
        return "testimonials";
    }
    @RequestMapping(value = "/testimonialCheck", method = RequestMethod.POST)
    public String testimonialCheck(@ModelAttribute("newMessage") MessageForm messageForm,HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientWeb/ClientWebController/testimonialCheck] with userCurrent = "+userCurrent);
        if(messageForm.getMessage().getDescription().length()>=3 && messageForm.getMessage().getDescription().length()<=500){
            CommentBean commentBean = new CommentBean();
            commentBean.setCheck(false);
            commentBean.setDescription(messageForm.getMessage().getDescription());
            commentBean.setCustomer(userCurrent);
            if(messageForm.getIdLawyer() != 0)
                commentBean.setLawyer(microserviceProxy.findUserById(messageForm.getIdLawyer()));
            else
                commentBean.setLawyer(null);
            microserviceProxy.addComment(commentBean);
        }
        return "redirect:/testimonials";
    }

    @RequestMapping("errors")
    public String errorPage(){
        log.debug("[ClientWeb/ClientWebController/error]");
        return "error";
    }

    /**
     * Check if a session of User exist then get User Session
     * else create New User with role.not_connected
     * @param request request
     * @return user
     */
    public UserBean getUserSession(HttpServletRequest request){
        log.debug("[ClientWeb/ClientWebController/getUserSession()]");
        HttpSession session = request.getSession();
        UserBean user = (UserBean) session.getAttribute("userClientCurrent");
        if(user == null)
            user = new UserBean();
        return user;
    }
}

package com.oc.ClientWeb.beans;

import com.oc.ClientWeb.enums.Role;
import lombok.extern.slf4j.Slf4j;

/**
 * i.e User of microservice
 */
@Slf4j
public class UserBean {
    private Long id;
    private String name;
    private String firstname;
    private String username;
    private String password;
    private Role role;

    public UserBean() {
        log.debug("[ClientWeb/UserBean()] instantiation of a new userBean");
        this.role = Role.NOT_CONNECTED;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}

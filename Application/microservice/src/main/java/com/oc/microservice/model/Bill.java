package com.oc.microservice.model;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.sql.Date;

/**
 * Bill characterized by
 * id
 * paid if the bill is paid
 * price of bill
 * Date of issue
 * customer
 * lawyer
 * service = subject
 * description describe with precision
 * seen if the lawyer takes note of the payment
 */
@Slf4j
@Entity
@Table(name="bill")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private boolean paid;
    private double price;
    private Date date;
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private User customer;
    @ManyToOne
    @JoinColumn(name = "id_lawyer")
    private User lawyer;
    private String service;
    private String description;
    private boolean seen;

    public Bill() {
        log.debug("[Microservice/Bill()] instantiation of a new bill");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public User getLawyer() {
        return lawyer;
    }

    public void setLawyer(User lawyer) {
        this.lawyer = lawyer;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", paid=" + paid +
                ", price=" + price +
                ", date=" + date +
                ", customer=" + customer +
                ", lawyer=" + lawyer +
                ", service=" + service +
                ", description=" + description +
                ", seen=" + seen +
                '}';
    }
}

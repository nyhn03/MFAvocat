package com.oc.ClientWeb.unit.beans;

import com.oc.ClientWeb.beans.UserBean;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;

public class UserBeanTestUnit {
    @Test
    public void validateSettersAndGetters() {
        final PojoClass userPojo = PojoClassFactory.getPojoClass(UserBean.class);

        final Validator validator = ValidatorBuilder.create()
                .with(new SetterTester(), new GetterTester())
                .build();
        validator.validate(userPojo);
    }
}

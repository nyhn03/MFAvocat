package com.oc.microservice.controller;

import com.oc.microservice.dao.*;
import com.oc.microservice.exception.ImpossibleAddBillException;
import com.oc.microservice.exception.ImpossibleAddContactException;
import com.oc.microservice.exception.ImpossibleAddMessageException;
import com.oc.microservice.model.*;
import com.oc.microservice.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class AlertController {
    @Autowired
    BillDao billDao;

    @Autowired
    MessageDao messageDao;

    @Autowired
    CommentDao commentDao;

    @Autowired
    ContactDao contactDao;

    @Autowired
    TaskDao taskDao;

    @Autowired
    MailService mailService;

    @GetMapping("/alertBill")
    public List<Bill> alertBill(){
        log.debug("[Microservice/AlertController/alertBill]");
        List<Bill> result = billDao.findBillForAlert();;
        log.debug("[Microservice/AlertController/alertBill] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/alertMessage/{idLawyer}")
    public List<Message> alertMessage(@PathVariable Long idLawyer){
        log.debug("[Microservice/AlertController/alertMessage/{idLawyer}] with idLawyer = " +idLawyer);
        List<Message> result = messageDao.findNewMessageByLawyer(idLawyer);
        log.debug("[Microservice/AlertController/alertMessage/{idLawyer}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/alertComment/{idLawyer}")
    public List<Comment> alertComment(@PathVariable Long idLawyer){
        log.debug("[Microservice/AlertController/alertComment/{idLawyer}] with idLawyer = " +idLawyer);
        List<Comment> result = commentDao.findCommentByNotCheck(idLawyer);
        log.debug("[Microservice/AlertController/alertComment/{idLawyer}]  Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/alertContact")
    public List<Contact> alertContact(){
        log.debug("[Microservice/AlertController/alertContact]");
        List<Contact> result = contactDao.findContactByNotSeen();
        log.debug("[Microservice/AlertController/alertContact] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/alertTask/{idLawyer}")
    public List<Task> alertTask(@PathVariable Long idLawyer){
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        log.debug("[Microservice/AlertController/alertTask/{idLawyer}] with idLawyer = " +idLawyer+" and date = "+date);
        List<Task> result = taskDao.findTaskNotAcceptedByLawyer(date,idLawyer);
        log.debug("[Microservice/AlertController/alertTask/{idLawyer}] Return -> \n"+result +" \n");
        return result;
    }

    /**
     * When bill is seen, value seen is updated to true
     * @param id id of bill
     * @return ResponseEntity
     * @throws ImpossibleAddBillException Error
     */
    @PostMapping("/alertBillSeen/{id}")
    public ResponseEntity<Bill> billSeen(@PathVariable Long id) throws ImpossibleAddBillException {
        Optional<Bill> bill = billDao.findById(id);
        bill.get().setSeen(true);
        Bill newBill = billDao.save(bill.get());
        log.debug("[Microservice/AlertController/alertBillSeen/{id}] with id = " +id +" and bill = " + newBill);
        if(newBill == null) throw new ImpossibleAddBillException("Mise à jour impossible");
        return new ResponseEntity<Bill>(bill.get(), HttpStatus.CREATED);
    }

    /**
     * When contact is seen, value seen is updated to true
     * @param id id of contact
     * @return ResponseEntity
     * @throws ImpossibleAddContactException Error
     */
    @PostMapping("/alertContactSeen/{id}")
    public ResponseEntity<Contact> contactSeen(@PathVariable Long id) throws ImpossibleAddContactException {
        Optional<Contact> contact = contactDao.findById(id);
        contact.get().setSeen(true);
        Contact newContact = contactDao.save(contact.get());
        log.debug("[Microservice/AlertController/alertContactSeen/{id}] with id = " +id+" and Contact = "+newContact);
        if(newContact == null) throw new ImpossibleAddContactException("Mise à jour impossible");
        return new ResponseEntity<Contact>(contact.get(), HttpStatus.CREATED);
    }

    /**
     * When message is seen, value seen is updated to true
     * @param id id of message
     * @return ResponseEntity
     * @throws ImpossibleAddMessageException Error
     */
    @PostMapping("/alertMessageSeen/{id}")
    public ResponseEntity<Message> messageSeen(@PathVariable Long id) throws ImpossibleAddMessageException {
        Optional<Message> message = messageDao.findById(id);
        message.get().setSeen(true);
        Message newMessage = messageDao.save(message.get());
        log.debug("[Microservice/AlertController/alertMessageSeen/{id}] with id = " +id+" and message = "+newMessage);
        if(newMessage == null) throw new ImpossibleAddMessageException("Mise à jour impossible");
        return new ResponseEntity<Message>(message.get(), HttpStatus.CREATED);
    }
    @PostMapping("/sendMessage/{subject}/{text}/{from}/{to}")
    public void sendMessage(@PathVariable String subject,@PathVariable String text,@PathVariable String from,@PathVariable String to){
        MailService.sendMessage(subject,text,from,to);
    }

}

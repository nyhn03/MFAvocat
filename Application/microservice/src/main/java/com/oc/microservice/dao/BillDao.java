package com.oc.microservice.dao;

import com.oc.microservice.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface BillDao extends JpaRepository<Bill,Long> {
    /**
     * Give all bill paid for a customer
     * @param idUser id of customer
     * @return List of bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = true AND bill.customer.id = :idUser")
    List<Bill> findBillByCustomerAndPaid(@Param("idUser") Long idUser);

    /**
     * Give all bill not paid for a customer
     * @param idUser id of customer
     * @return List of bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = false AND bill.customer.id = :idUser")
    List<Bill> findBillByCustomerAndNotPaid(@Param("idUser") Long idUser);

    /**
     * Give all bill for a customer
     * @param idUser id of customer
     * @return list of bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.customer.id = :idUser")
    List<Bill> findBillByCustomer(@Param("idUser") Long idUser);

    /**
     * Give all bill paid order by date
     * @return List of Bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = true order by bill.date")
    List<Bill> findBillByPaid();

    /**
     * Give all bill Not Paid order by date
     * @return List of Bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = false order by bill.date")
    List<Bill> findBillByNotPaid();

    /**
     * Give all bill paid between two date
     * @param min min date
     * @param max max date
     * @return list of bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = true and bill.date between :min and :max")
    List<Bill> findBillByMonth(@Param("min")Date min,@Param("max")Date max);

    /**
     * Give all bill not paid between two date
     * @param min min date
     * @param max max date
     * @return list of bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = false and bill.date between :min and :max")
    List<Bill> findBillByMonthNotPaid(@Param("min")Date min,@Param("max")Date max);

    /**
     * Give all bill paid and not seen by lawyer
     * @return list of bill
     */
    @Query("SELECT bill FROM Bill bill WHERE bill.paid = true AND bill.seen = false")
    List<Bill> findBillForAlert();
}

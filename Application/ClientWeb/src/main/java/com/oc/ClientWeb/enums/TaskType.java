package com.oc.ClientWeb.enums;

/**
 * TaskType matches to type of tasks
 */
public enum TaskType {
    MEETING,
    HOLIDAY,
    AUDIENCE,
    OTHER,
    NO_CHOICE
}

package com.oc.ClientWeb.model;

import com.oc.ClientWeb.beans.MessageBean;
import lombok.extern.slf4j.Slf4j;

/**
 * MessageForm is a form to create a message
 * message
 * idLawyer for add lawyer after
 */
@Slf4j
public class MessageForm {
    private MessageBean message;
    private Long idLawyer;

    public MessageForm() {
        log.debug("[ClientWeb/MessageForm()] instantiation of a new messageForm");
        message = new MessageBean();
    }

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public Long getIdLawyer() {
        return idLawyer;
    }

    public void setIdLawyer(Long idLawyer) {
        this.idLawyer = idLawyer;
    }
}

package com.oc.microservice.service;

import com.oc.microservice.dao.TaskDao;
import com.oc.microservice.model.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Slf4j
@Service
public class TaskService {
    @Autowired
    TaskDao taskDao;

    private static int NB_DAY = 7;
    private static int MIN_HOUR = 8;

    public static int getNbDay() {
        return NB_DAY;
    }
    public static int getMinHour() {
        return MIN_HOUR;
    }
    public static int getMaxHour() {
        return MAX_HOUR;
    }

    private static int MAX_HOUR = 17;
    private static String[] dates = {
            "Samedi",
            "Dimanche",
            "Lundi",
            "Mardi",
            "Mercredi",
            "Jeudi",
            "Vendredi",
    } ;
    private static String[] mouths = {
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
    } ;


    /**
     * get the number of the day
     * (0 :saturday
     *  1 :sunday
     *  2 :Monday
     *  3 :tuesday
     *  4 :wednesday
     *  5 :thursday
     *  6 :friday
     * @param date date
     * @return result
     */
    public int sayNumDayDate(Date date) {
        log.debug("[Microservice/TaskService/sayNumDayDate(Date)] with date = "+date);
        int result;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        result = cal.get(Calendar.DAY_OF_WEEK);
        log.debug("[Microservice/TaskService/sayNumDayDate(Date)]  Return -> \n"+result +" \n");
        return result;
    }

    /**
     * Get all task between first date to Date firstDate + NB_Day
     * @param date date
     * @return TaskList
     */
    public ArrayList getWeek(Date date){
        log.debug("[Microservice/TaskService/getWeek(Date)] with date = "+date);
        ArrayList arrayList = new ArrayList();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        for (int i = 0; i< NB_DAY;i++){
            arrayList.add(taskDao.findByDateOrderHour(date));
            cal.add(Calendar.DATE,1);
            date = new Date(cal.getTimeInMillis());
        }
        log.debug("[Microservice/TaskService/getWeek(Date)] Return -> \n"+arrayList +" \n");
        return arrayList;
    }

    /**
     * Get Hours available by day for each day selected
     * @param date date
     * @return ArrayList of List of Integer
     */
    public ArrayList getHoursAvailableForWeek(Date date){
        log.debug("[Microservice/TaskService/getHoursAvailableForWeek(Date)] with date = "+date);
        ArrayList arrayList = new ArrayList();
        ArrayList tasks = getWeek(date);
        for (int i = 0 ; i < tasks.size() ; i++){
            arrayList.add(hoursAvailable((List<Task>) tasks.get(i)));
        }
        log.debug("[Microservice/TaskService/getHoursAvailableForWeek(Date)] Return -> \n"+arrayList +" \n");
        return arrayList;
    }

    /**
     * Get hours available for a day
     * @param tasks list of tasks of a day
     * @return List of integer (= hour available)
     */
    public List<Integer> hoursAvailable(List<Task> tasks){
        log.debug("[Microservice/TaskService/hoursAvailable(List<Task>)] with tasks = "+tasks);
        List<Integer> hoursAvailable = initHour();
        for( int i= 0; i < tasks.size(); i++){
            if(tasks.get(i).getDuration() == 24)
                return null;
            for(int j=0; j< hoursAvailable.size();j++) {
                if (tasks.get(i).getHour() == hoursAvailable.get(j)) {
                    hoursAvailable.remove(j);
                    int duree = tasks.get(i).getDuration();
                    while(duree > 1){
                        if(hoursAvailable.size()<=j)
                            break;
                        hoursAvailable.remove(j);
                        duree--;
                    }
                }
            }
        }
        log.debug("[Microservice/TaskService/hoursAvailable(List<Task>)] Return -> \n"+hoursAvailable +" \n");
        return hoursAvailable;
    }

    /**
     * Function to init a list of integer
     * (= all hour between min_hour and max_hour)
     * @return list of hours of a day
     */
    public List<Integer> initHour(){
        log.debug("[Microservice/TaskService/initHour()]");
        List<Integer> hoursAvailable = new ArrayList<Integer>();
        for( int i = MIN_HOUR; i<= MAX_HOUR; i++){
            hoursAvailable.add(i);
        }
        log.debug("[Microservice/TaskService/initHour()] Return -> \n"+hoursAvailable +" \n");
        return hoursAvailable;
    };

    /**
     * Create a list of Nb_day between date = first date and end_date = firstdate + nb_day
     * @param date first date
     * @return List of date
     */
    public ArrayList getDates(Date date){
        log.debug("[Microservice/TaskService/getDates(Date)] with date = "+date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        ArrayList result = new ArrayList();
        for(int i = 0; i < NB_DAY; i++){
            result.add(date);
            cal.add(Calendar.DATE,1);
            date = new Date(cal.getTimeInMillis());
        }
        log.debug("[Microservice/TaskService/getDates(Date)] Return -> \n"+result +" \n");
        return result;
    }

    /**
     * function to list a string date
     * Format : Jour DD mois année
     * @param date first date
     * @return list of string
     */
    public ArrayList getStringDate(Date date){
        log.debug("[Microservice/TaskService/getStringDate(Date)] with date = "+date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        ArrayList result = new ArrayList();
        for(int i = 0; i < NB_DAY; i++){
            result.add(dates[cal.get(Calendar.DAY_OF_WEEK)%7]+" "+ cal.get(Calendar.DATE)+" "+ mouths[cal.get(Calendar.MONTH)%12]+" "+cal.get(Calendar.YEAR));
            cal.add(Calendar.DATE,1);
        }
        log.debug("[Microservice/TaskService/getStringDate(Date)] Return -> \n"+result +" \n");
        return result;
    }

    /**
     * Function to return a string date for one date
     * @param date date
     * @return string format : Jour DD mois année
     */
    public String stringDate(Date date){
        log.debug("[Microservice/TaskService/stringDate(Date)] with date = "+date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String result;
        result = dates[cal.get(Calendar.DAY_OF_WEEK)%7]+" "+ cal.get(Calendar.DATE)+" "+ mouths[cal.get(Calendar.MONTH)%12]+" "+cal.get(Calendar.YEAR);
        log.debug("[Microservice/TaskService/stringDate(Date)]  Return -> \n"+result +" \n");
        return result;
    }


}

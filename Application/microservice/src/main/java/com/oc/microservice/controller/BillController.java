package com.oc.microservice.controller;

import com.oc.microservice.dao.BillDao;
import com.oc.microservice.exception.ImpossibleAddBillException;
import com.oc.microservice.model.Bill;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class BillController {
    @Autowired
    BillDao billDao;

    @GetMapping(value = "/billNotPaid/{idCustomer}")
    public List<Bill> findByCustomerAndNotPaid(@PathVariable Long idCustomer){
        log.debug("[Microservice/BillController/billNotPaid/{idCustomer}] with idCustomer = "+idCustomer);
        List<Bill> result = billDao.findBillByCustomerAndNotPaid(idCustomer);
        log.debug("[Microservice/BillController/billNotPaid/{idCustomer}] Return -> \n"+result +" \n");
        return result;
    }
    @GetMapping(value = "/billPaid/{idCustomer}")
    public List<Bill> findByCustomerAndPaid(@PathVariable Long idCustomer){
        log.debug("[Microservice/BillController/billPaid/{idCustomer}] with idCustomer = "+idCustomer);
        List<Bill> result = billDao.findBillByCustomerAndPaid(idCustomer);
        log.debug("[Microservice/BillController/billPaid/{idCustomer}] Return -> \n"+result +" \n");
        return result;
    }
    @GetMapping(value = "/billByUser/{idCustomer}")
    public List<Bill> findfindBillByUser(@PathVariable Long idCustomer){
        log.debug("[Microservice/BillController/billByUser/{idCustomer}] with idCustomer = "+idCustomer);
        List<Bill> result = billDao.findBillByCustomer(idCustomer);
        log.debug("[Microservice/BillController/billByUser/{idCustomer}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value ="/bill/{id}")
    public Optional<Bill> findBillById(@PathVariable Long id){
        log.debug("[Microservice/BillController/bill/{id}] with id = "+id);
        Optional<Bill> result = billDao.findById(id);
        log.debug("[Microservice/BillController/bill/{id}] Return -> \n"+result +" \n");
        return result;
    }

    @PostMapping("/isPaid")
    public ResponseEntity<Bill> paid(@RequestBody Bill bill) throws ImpossibleAddBillException {
        Bill newBill = billDao.save(bill);
        log.debug("[Microservice/BillController/isPaid] with bill = "+newBill);
        if(newBill == null) throw new ImpossibleAddBillException("impossible de modifier cette facture");
        return new ResponseEntity<>(bill, HttpStatus.CREATED);
    }

    @PostMapping("/addBill")
    public ResponseEntity<Bill> addBill(@RequestBody Bill bill) throws ImpossibleAddBillException {
        Bill newBill = billDao.save(bill);
        log.debug("[Microservice/BillController/addBill] with bill = "+newBill);

        if(newBill == null) throw new ImpossibleAddBillException("impossible d'ajouter cette facture");
        return new ResponseEntity<>(bill, HttpStatus.CREATED);
    }

    @GetMapping(value = "/billNotPaid")
    public List<Bill> findNotPaid(){
        log.debug("[Microservice/BillController/billNotPaid]");
        List<Bill> result = billDao.findBillByNotPaid();
        log.debug("[Microservice/BillController/billNotPaid] Return -> \n"+result +" \n");
        return result;
    }
    @GetMapping(value = "/billPaid")
    public List<Bill> findByPaid(){
        log.debug("[Microservice/BillController/billPaid]");
        List<Bill> result = billDao.findBillByPaid();
        log.debug("[Microservice/BillController/billPaid] Return -> \n"+result +" \n");
        return result;
    }

    /**
     * Give price total of all bill by month
     * @param month month
     * @return decimal
     */
    @GetMapping(value ="/billMonth/{month}")
    public double findSumBillMonth(@PathVariable int month){
        log.debug("[Microservice/BillController/billMonth/{month}] with month = "+month);
        Calendar calMin = Calendar.getInstance();
        Calendar calMax = Calendar.getInstance();

        calMin.set(Calendar.DATE,1);
        calMax.set(Calendar.DATE,1);
        calMin.set(Calendar.MONTH,month);
        calMax.set(Calendar.MONTH,month+1);

        Date dateMin= new Date(calMin.getTime().getTime());
        Date dateMax= new Date(calMax.getTime().getTime());

        List<Bill> bills = billDao.findBillByMonth(dateMin,dateMax);
        double priceTotal=0;
        for(int i=0;i<bills.size();i++){
            priceTotal += bills.get(i).getPrice();
        }
        log.debug("[Microservice/BillController/billMonth/{month}] Return -> \n"+priceTotal +" \n");
        return priceTotal;
    }

    /**
     * Give price total of all bill not paid by month
     * @param month month
     * @return decimal
     */
    @GetMapping(value ="/billMonthNotPaid/{month}")
    public double findSumBillMonthNotPaid(@PathVariable int month){
        log.debug("[Microservice/BillController/billMonthNotPaid/{month}] with month = "+month);

        Calendar calMin = Calendar.getInstance();
        Calendar calMax = Calendar.getInstance();

        calMin.set(Calendar.DATE,1);
        calMax.set(Calendar.DATE,1);
        calMin.set(Calendar.MONTH,month);
        calMax.set(Calendar.MONTH,month+1);

        Date dateMin= new Date(calMin.getTime().getTime());
        Date dateMax= new Date(calMax.getTime().getTime());

        List<Bill> bills = billDao.findBillByMonthNotPaid(dateMin,dateMax);
        double priceTotal=0;
        for(int i=0;i<bills.size();i++){
            priceTotal += bills.get(i).getPrice();
        }
        log.debug("[Microservice/BillController/billMonthNotPaid/{month}] Return -> \n"+priceTotal +" \n");
        return priceTotal;
    }

    @GetMapping("deleteBill/{id}")
    public void deleteBill(@PathVariable Long id){
        log.debug("[Microservice/BillController/deleteBill/{id}] with id = "+id);
        billDao.deleteById(id);
    }
}

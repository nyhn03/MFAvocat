package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.BillDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.model.Bill;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class BillDaoTestIT {
    @Autowired
    BillDao billDao;

    @Autowired
    UserDao userDao;

    @Test
    public void saveBillTest_bill_noReturn(){
        Bill bill = new Bill();
        bill.setCustomer(userDao.getOne(1L));
        bill.setLawyer(userDao.getOne(1L));
        bill.setPrice(55.55);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 13);

        bill.setDate(new Date(cal.getTime().getTime()));
        bill.setPaid(false);
        bill.setSeen(false);
        bill.setService("test");

        billDao.save(bill);
        Bill billSearch = billDao.findBillByCustomer(1L).get(0);
        boolean testCheck = false;
        if(
                bill.getCustomer().getId().equals(billSearch.getCustomer().getId())
                && bill.getLawyer().getId().equals(billSearch.getLawyer().getId())
                && bill.getPrice() == billSearch.getPrice()
                && bill.isPaid() == billSearch.isPaid()
                && bill.isSeen() == billSearch.isSeen()
                && bill.getService().equals(billSearch.getService())
        )
            testCheck = true;

        Assert.assertTrue(testCheck);
        billDao.delete(billSearch);
    }

    @Test
    public void findBillByCustomerAndPaid_idUser_ListBill(){
        List<Bill> bills = billDao.findBillByCustomerAndPaid(2L);
        Assert.assertEquals(3,bills.size());
        boolean testCheck = bills.get(0).isPaid()
                && bills.get(0).getPrice() == 15.0
                && bills.get(0).getCustomer().getId().equals(2L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillByCustomerAndNotPaid_idUser_ListBill(){
        List<Bill> bills = billDao.findBillByCustomerAndNotPaid(2L);
        Assert.assertEquals(3,bills.size());
        boolean testCheck = !bills.get(0).isPaid()
                && bills.get(0).getPrice() == 10.0
                && bills.get(0).getCustomer().getId().equals(2L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && !bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillByCustomer_idUser_ListBill(){
        List<Bill> bills = billDao.findBillByCustomer(2L);
        Assert.assertEquals(6,bills.size());
        boolean testCheck = !bills.get(0).isPaid()
                && bills.get(0).getPrice() == 10.0
                && bills.get(0).getCustomer().getId().equals(2L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && !bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillByPaid_no_ListBill(){
        List<Bill> bills = billDao.findBillByPaid();
        Assert.assertEquals(5,bills.size());
        boolean testCheck = bills.get(0).isPaid()
                && bills.get(0).getPrice() == 45.0
                && bills.get(0).getCustomer().getId().equals(4L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && !bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillByNotPaid_no_ListBill(){
        List<Bill> bills = billDao.findBillByNotPaid();
        Assert.assertEquals(5,bills.size());
        boolean testCheck = !bills.get(0).isPaid()
                && bills.get(0).getPrice() == 40.0
                && bills.get(0).getCustomer().getId().equals(4L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillByMonth_DateMinAndDateMax_ListBill(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date dateMin = new Date(cal.getTime().getTime());
        cal.add(Calendar.MONTH,1);
        Date dateMax = new Date(cal.getTime().getTime());

        List<Bill> bills = billDao.findBillByMonth(dateMin,dateMax);
        Assert.assertEquals(3,bills.size());
        boolean testCheck = bills.get(0).isPaid()
                && bills.get(0).getPrice() == 15.0
                && bills.get(0).getCustomer().getId().equals(2L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillByMonthNotPaid_DateMinAndDateMax_ListBill(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date dateMin = new Date(cal.getTime().getTime());
        cal.add(Calendar.MONTH,1);
        Date dateMax = new Date(cal.getTime().getTime());

        List<Bill> bills = billDao.findBillByMonthNotPaid(dateMin,dateMax);
        Assert.assertEquals(3,bills.size());
        boolean testCheck = !bills.get(0).isPaid()
                && bills.get(0).getPrice() == 10.0
                && bills.get(0).getCustomer().getId().equals(2L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && !bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findBillForAlert_no_ListBill(){
        List<Bill> bills = billDao.findBillForAlert();
        Assert.assertEquals(2,bills.size());
        boolean testCheck = bills.get(0).isPaid()
                && bills.get(0).getPrice() == 25.0
                && bills.get(0).getCustomer().getId().equals(2L)
                && bills.get(0).getLawyer().getId().equals(1l)
                && bills.get(0).getService().equals("Service test")
                && bills.get(0).getDescription().equals("Description Test")
                && !bills.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }
}

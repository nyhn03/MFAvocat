package com.oc.microservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ImpossibleAddTaskException extends Throwable {
    public ImpossibleAddTaskException(String message) {
        super(message);
    }
}

package com.oc.ClientWeb.unit.service;

import com.oc.ClientWeb.beans.TaskBean;
import com.oc.ClientWeb.beans.UserBean;
import com.oc.ClientWeb.enums.Reason;
import com.oc.ClientWeb.enums.Role;
import com.oc.ClientWeb.enums.TaskType;
import com.oc.ClientWeb.model.TaskForm;
import com.oc.ClientWeb.service.TaskFormCheck;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;


import org.junit.Assert;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.util.Calendar;

@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class TaskFormCheckTestUnit {
    @InjectMocks
    TaskFormCheck taskFormCheck;

    public TaskBean createTask(){
        TaskBean task = new TaskBean();
        UserBean user = new UserBean();
        user.setId(1l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        lawyer.setRole(Role.LAWYER);
        task.setCustomer(user);
        task.setLawyer(lawyer);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 13);
        Date date = new Date(cal.getTime().getTime());
        task.setDate(date);
        task.setDuration(2);
        task.setHour(11);
        task.setReason(Reason.ADVICE);
        task.setType(TaskType.MEETING);
        task.setDescription("description");
        return task;
    }

    @Test
    public void validateSettersAndGetters() {
        final PojoClass userPojo = PojoClassFactory.getPojoClass(TaskFormCheck.class);

        final Validator validator = ValidatorBuilder.create()
                .with(new SetterTester(), new GetterTester())
                .build();
        validator.validate(userPojo);
    }

    @Test
    public void validateTest_no_boolean(){
        TaskForm taskForm = new TaskForm();
        taskForm.setTask(createTask());
        taskForm.setIdLawyer(2l);
        UserBean user = new UserBean();
        user.setId(1l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        lawyer.setRole(Role.LAWYER);
        taskFormCheck.evaluate(taskForm,lawyer,user);
        Assert.assertTrue(taskFormCheck.validate());
    }

    @Test
    public void validateTestCustomerError_no_boolean(){
        TaskForm taskForm = new TaskForm();
        taskForm.setTask(createTask());
        taskForm.setIdLawyer(2l);
        UserBean user = new UserBean();
        user.setId(3l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        lawyer.setRole(Role.LAWYER);
        taskFormCheck.evaluate(taskForm,lawyer,user);
        Assert.assertTrue(!taskFormCheck.validate());
    }

    @Test
    public void validateTestLawyerError_no_boolean(){
        TaskForm taskForm = new TaskForm();
        taskForm.setTask(createTask());
        taskForm.setIdLawyer(2l);
        UserBean user = new UserBean();
        user.setId(1l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        taskFormCheck.evaluate(taskForm,lawyer,user);
        Assert.assertTrue(!taskFormCheck.validate());
    }

    @Test
    public void validateTestReasonError_no_boolean(){
        TaskForm taskForm = new TaskForm();
        taskForm.setTask(createTask());
        taskForm.setIdLawyer(2l);
        UserBean user = new UserBean();
        user.setId(1l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        lawyer.setRole(Role.LAWYER);
        taskForm.getTask().setReason(Reason.NO_CHOICE);
        taskFormCheck.evaluate(taskForm,lawyer,user);
        Assert.assertTrue(!taskFormCheck.validate());
    }

    @Test
    public void validateTestHourError_no_boolean(){
        TaskForm taskForm = new TaskForm();
        taskForm.setTask(createTask());
        taskForm.setIdLawyer(2l);
        UserBean user = new UserBean();
        user.setId(1l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        lawyer.setRole(Role.LAWYER);
        taskForm.getTask().setHour(-1);
        taskFormCheck.evaluate(taskForm,lawyer,user);
        Assert.assertTrue(!taskFormCheck.validate());
    }

    @Test
    public void validateTestDescriptionError_no_boolean(){
        TaskForm taskForm = new TaskForm();
        taskForm.setTask(createTask());
        taskForm.setIdLawyer(2l);
        UserBean user = new UserBean();
        user.setId(1l);
        UserBean lawyer = new UserBean();
        lawyer.setId(2l);
        lawyer.setRole(Role.LAWYER);
        taskForm.getTask().setDescription("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                +"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        taskFormCheck.evaluate(taskForm,lawyer,user);
        Assert.assertTrue(!taskFormCheck.validate());
    }
}

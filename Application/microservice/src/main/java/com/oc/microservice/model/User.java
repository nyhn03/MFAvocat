package com.oc.microservice.model;
import com.oc.microservice.enums.Role;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Set;

/**
 * User characterized by
 * id
 * Name
 * Firstname
 * Username
 * Password
 * Role enum of client / lawyer / developper / no_connected
 */
@Slf4j
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Length(min = 3,max = 50,message = "Le nom doit Ãªtre compris entre 3 et 50 caractÃ¨res.")
    private String name;
    @Length(min = 3,max = 50,message = "Le prÃ©nom doit Ãªtre compris entre 3 et 50 caractÃ¨res.")
    private String firstname;
    @Length(min = 3,max = 50,message = "Le pseudo doit Ãªtre compris entre 3 et 50 caractÃ¨res.")
    private String username;
    @Length(min = 3,max = 100,message = "Le mot de passe doit Ãªtre compris entre 3 et 100 caractÃ¨res.")
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(length = 13)
    private Role role;

    public User() {
        log.debug("[Microservice/User()] instantiation of a new user");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

}

package com.oc.ClientWeb.beans;

import com.oc.ClientWeb.enums.Reason;
import com.oc.ClientWeb.enums.TaskType;
import lombok.extern.slf4j.Slf4j;

import java.sql.Date;

/**
 * i.e Task of microservice
 */
@Slf4j
public class TaskBean {
    private Long id;
    private TaskType type;
    private Date date;
    private int hour;
    private int duration;
    private Reason reason;
    private String description;
    private boolean accepted;
    private UserBean lawyer;
    private UserBean customer;
    private boolean visio;
    private String link;

    public TaskBean() {
        log.debug("[ClientWeb/TaskBean()] instantiation of a new taskBean");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public UserBean getLawyer() {
        return lawyer;
    }

    public void setLawyer(UserBean lawyer) {
        this.lawyer = lawyer;
    }

    public UserBean getCustomer() {
        return customer;
    }

    public void setCustomer(UserBean customer) {
        this.customer = customer;
    }

    public boolean isVisio() {
        return visio;
    }

    public void setVisio(boolean visio) {
        this.visio = visio;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "TaskBean{" +
                "id=" + id +
                ", type=" + type +
                ", date=" + date +
                ", hour=" + hour +
                ", duration=" + duration +
                ", reason=" + reason +
                ", description='" + description + '\'' +
                ", accepted=" + accepted +
                ", lawyer=" + lawyer +
                ", customer=" + customer +
                ", visio=" + visio +
                ", link=" + link +
                '}';
    }
}

package com.oc.ClientAdmin.service;

import com.oc.ClientAdmin.beans.UserBean;
import com.oc.ClientAdmin.enums.Reason;
import com.oc.ClientAdmin.enums.Role;
import com.oc.ClientAdmin.enums.TaskType;
import com.oc.ClientAdmin.model.TaskForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * TaskFormCheck characterized by
 *
 * lawyerError error of lawyer
 * hourNoSelected No hour selected
 * descriptionSize description is greater than or equal 500
 * typeNoChoice No type selected
 * durationError No duration selected
 * i.e ClientWebTaskFormCheck
 */
@Slf4j
@Service
public class TaskFormCheck {
    private static boolean lawyerError;
    private static boolean typeNoChoice;
    private static boolean hourNoSelected;
    private static boolean descriptionSize;
    private static boolean durationError;

    public TaskFormCheck() {
        log.debug("[ClientAdmin/TaskFormCheck/TaskFormCheck()]");
        init();
    }

    public void init(){
        log.debug("[ClientAdmin/TaskFormCheck/init()]");
        lawyerError = false;
        typeNoChoice = false;
        hourNoSelected = false;
        descriptionSize = false;
        durationError = false;
    }

    public void evaluate(TaskForm taskForm, UserBean lawyer) {
        log.debug("[ClientAdmin/TaskFormCheck/evaluate(TaskForm,UserBean)] with taskForm = "+taskForm+" and lawyer = "+lawyer);
        init();
        lawyerCheck(taskForm,lawyer);
        typeCheck(taskForm);
        hourCheck(taskForm);
        descriptionCheck(taskForm);
        durationCheck(taskForm);
    }

    public static boolean validate(){
        log.debug("[ClientAdmin/TaskFormCheck/validate()]");
        if(lawyerError)
            return false;
        if(typeNoChoice)
            return false;
        if(hourNoSelected)
            return false;
        if(descriptionSize)
            return false;
        if(durationError)
            return false;
        log.debug("[ClientAdmin/TaskFormCheck/validate()] Return -> true");
        return true;
    }

    private void hourCheck(TaskForm taskForm) {
        log.debug("[ClientAdmin/TaskFormCheck/hourCheck(TaskForm)] with taskForm = "+taskForm );
        if(taskForm.getTask().getHour() == -1 && (taskForm.hourForced > 24 || taskForm.hourForced <0))
            hourNoSelected = true;
    }

    /**
     * check if duration is > 0
     * @param taskForm
     */
    private void durationCheck(TaskForm taskForm) {
        log.debug("[ClientAdmin/TaskFormCheck/durationCheck(TaskForm)] with taskForm = "+taskForm );
        if(taskForm.task.getDuration() <1)
            durationError = true;
    }

    /**
     * Check if type is diff of No_Choice
     * @param taskForm
     */
    private void typeCheck(TaskForm taskForm) {
        log.debug("[ClientAdmin/TaskFormCheck/typeCheck(TaskForm)] with taskForm = "+taskForm );
        if(taskForm.getTask().getType() == TaskType.NO_CHOICE)
            typeNoChoice = true;
    }


    private void descriptionCheck(TaskForm taskForm) {
        log.debug("[ClientAdmin/TaskFormCheck/descriptionCheck(TaskForm)] with taskForm = "+taskForm );
        if(taskForm.getTask().getDescription().length() >= 500)
            descriptionSize = true;
    }


    private void lawyerCheck(TaskForm taskForm, UserBean lawyer) {
        log.debug("[ClientAdmin/TaskFormCheck/lawyerCheck(TaskForm,UserBean)] with taskForm = "+taskForm+" and lawyer = "+lawyer );
        if(lawyer.getId() != taskForm.getTask().getLawyer().getId() || lawyer.getRole() != Role.LAWYER)
            lawyerError = true;
    }



    public static boolean isLawyerError() {
        return lawyerError;
    }

    public static void setLawyerError(boolean lawyerError) {
        TaskFormCheck.lawyerError = lawyerError;
    }

    public static boolean isTypeNoChoice() {
        return typeNoChoice;
    }

    public static void setTypeNoChoice(boolean typeNoChoice) {
        TaskFormCheck.typeNoChoice = typeNoChoice;
    }

    public static boolean isHourNoSelected() {
        return hourNoSelected;
    }

    public static void setHourNoSelected(boolean hourNoSelected) {
        TaskFormCheck.hourNoSelected = hourNoSelected;
    }

    public static boolean isDescriptionSize() {
        return descriptionSize;
    }

    public static void setDescriptionSize(boolean descriptionSize) {
        TaskFormCheck.descriptionSize = descriptionSize;
    }

    public static boolean isDurationError() {
        return durationError;
    }

    public static void setDurationError(boolean durationError) {
        TaskFormCheck.durationError = durationError;
    }

    @Override
    public String toString() {
        String result ="lawyerError = " + lawyerError+
                "\n typeNoChoice = " + typeNoChoice+
                "\n hourNoSelected = " + hourNoSelected+
                "\n durationError = " + durationError+
                "\n descriptionSize = "+ descriptionSize;
        return result;
    }
}

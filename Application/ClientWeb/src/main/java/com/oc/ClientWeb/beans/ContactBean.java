package com.oc.ClientWeb.beans;

import lombok.extern.slf4j.Slf4j;

/**
 * i.e Contact of microservice
 */
@Slf4j
public class ContactBean {
    private Long id;
    private String name;
    private String mail;
    private String subject;
    private String message;
    private boolean seen;

    public ContactBean() {
        log.debug("[ClientWeb/ContactBean()] instantiation of a new contactBean");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "ContactBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", seen=" + seen +
                '}';
    }
}

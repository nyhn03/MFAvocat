package com.oc.microservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ImpossibleAddUserException extends Throwable {
    public ImpossibleAddUserException(String message) {
        super(message);
    }
}

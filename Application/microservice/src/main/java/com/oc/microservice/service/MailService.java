package com.oc.microservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Slf4j
@Service
public class MailService {
    public static void sendMessage(String subject, String text, String from, String to){
        log.debug("[Microservice/MailService/sendMessage(subject,text,from,to)] with subject = "+subject+" ,text = "+text+" ,from = "+from+" and to = "+to);
        final String username = "duboisgeoffrey03@gmail.com";
        final String password = "cupzrancbukvyisl";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(to)
            );
            message.setSubject(subject);
            message.setText(text);

            //Transport.send(message); Mail stopper pour éviter de polluer.

            log.debug("[Microservice/MailService/sendMessage(subject,text,from,to)] -> send message");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

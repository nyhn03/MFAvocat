package com.oc.ClientAdmin.enums;

/**
 * Reason matches to reason of tasks
 */
public enum Reason {
    NO_CHOICE,
    DIVORCE,
    ADVICE,
    SUCCESSION,
    CRIMINAL,
    FAMILY,
    CHILD_CARE,
    ALIMONY,
    CONSTRUCTION,
    INTELLECTUAL_PROPERTY,
    OTHER
}

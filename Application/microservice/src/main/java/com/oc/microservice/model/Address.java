package com.oc.microservice.model;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * Address characterized by
 * id
 * num of street
 * name of street
 * ZipCode
 * City
 * phone of customer
 * mail of customer
 * User
 */
@Slf4j
@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int num;
    @Length(min = 3,max = 50,message = "Le nom doit Ãªtre compris entre 3 et 50 caractÃ¨res.")
    private String name;
    @Length(min = 5,max = 5,message = "Le zip code doit faire 5 caractÃ¨res.")
    private String zipCode;
    @Length(min = 3,max = 50,message = "Le pays doit Ãªtre compris entre 3 et 50 caractÃ¨res.")
    private String city;
    @Length(min = 10,max = 12,message = "Le numéro de téléphone doit Ãªtre compris entre 10 et 12 caractÃ¨res (10 si format 06xxxxxxxx, 12 si format +33 6xxxxxxxx .")
    private String phone;
    @Length(min = 3,max = 50,message = "Le mail doit Ãªtre compris entre 3 et 50 caractÃ¨res.")
    private String mail;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address() {
        log.debug("[Microservice/Address()] instantiation of a new address");
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", num=" + num +
                ", name='" + name + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", phone='" + phone + '\'' +
                ", mail='" + mail + '\'' +
                ", user=" + user +
                '}';
    }
}

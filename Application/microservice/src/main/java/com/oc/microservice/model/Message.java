package com.oc.microservice.model;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Date;

/**
 * message characterized by
 * id
 * date of issue
 * description = message
 * lawyer
 * customer
 * type  true for a message sent by customer, and false for lawyer
 * seen if lawyer take notes of message
 */
@Slf4j
@Entity
@Table(name = "message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date date;
    @Length(min = 3,max = 1000,message = "La description doit être compris entre 3 et 1000 caractères.")
    private String description;
    @ManyToOne
    @JoinColumn(name = "id_lawyer")
    private User lawyer;
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private User customer;
    // true for a message sent by customer, and false for lawyer
    private boolean type;
    private boolean seen;

    public Message() {
        log.debug("[Microservice/Message()] instantiation of a new message");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getLawyer() {
        return lawyer;
    }

    public void setLawyer(User lawyer) {
        this.lawyer = lawyer;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", lawyer=" + lawyer +
                ", customer=" + customer +
                ", type=" + type +
                ", seen=" + seen +
                '}';
    }
}

-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: db_mfavocat
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (13,53,'rue Chanzy','13300','Salon de Provence','0623524897','mylenefernandez.avocat@gmail.com',38),(14,592,'RUE SAINT PIERRE','13010','MARSEILLE','0605187091','duboisgeoffrey03@gmail.com',39),(15,7,'Boulevard Polo','13013','Marseille','0667801953','camille.ponte.cp@gmail.com',40),(16,23,'Allée des fauvettes','62121','Pas de calais','0607080910','Frypon@gmail.com',41),(17,12,'impasse du muguet','13012','Marseille','0404040404','celia@gmail.com',42),(18,33,'avenue des lecques','13120','St scyr','0606060606','long@gmail.com',43);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (1,1,27.00,'2021-04-26',39,38,'Forfait du 21/04',NULL,1),(2,1,46.00,'2021-04-26',40,38,'Forfait du 13/04',NULL,1),(3,1,510.00,'2021-03-15',41,38,'Forfait du 7/02',NULL,0),(4,1,320.00,'2021-04-27',43,38,'Forfait du 25/04',NULL,0),(5,0,79.00,'2021-04-30',39,38,'Forfait du 30/04',NULL,0),(15,1,950.00,'2021-05-19',39,38,'Forfait du 19/05','Tribunal + traitement',1),(17,0,25.00,'2021-05-27',41,38,'Rendez vous du 15 mars 2021','Visio',0),(18,0,650.00,'2021-05-27',43,38,'Forfait du 13/05','Acte 1+ acte 2',0);
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,'Post emensos insuperabilis expeditionis eventus languentibus partium animis, quas periculorum varietas fregerat et laborum, nondum tubarum cessante clangore vel milite locato per stationes hibernas, fortunae saevientis procellae tempestates alias rebus infudere communibus per multa illa et dira facinora Caesaris Galli, qui ex squalore imo miseriarum in aetatis adultae primitiis ad principale culmen insperato saltu provectus ultra terminos potestatis delatae procurrens asperitate nimia cuncta foedabat. propinquitate enim regiae stirpis gentilitateque etiam tum Constantini nominis efferebatur in fastus, si plus valuisset, ausurus hostilia in auctorem suae felicitatis, ut videbatur.',1,38,39),(2,'Nihil morati post haec militares avidi saepe turbarum adorti sunt Montium primum, qui divertebat in proximo, levi corpore senem atque morbosum, et hirsutis resticulis cruribus eius innexis divaricaturn sine spiramento ullo ad usque praetorium traxere praefecti.',1,38,40),(3,'Postremo ad id indignitatis est ventum, ut cum peregrini ob formidatam haut ita dudum alimentorum inopiam pellerentur ab urbe praecipites, sectatoribus disciplinarum liberalium inpendio paucis sine respiratione ulla extrusis, tenerentur minimarum adseclae veri, quique id simularunt ad tempus, et tria milia saltatricum ne interpellata quidem cum choris totidemque remanerent magistris.',1,38,41),(4,'Dum apud Persas, ut supra narravimus, perfidia regis motus agitat insperatos, et in eois tractibus bella rediviva consurgunt, anno sexto decimo et eo diutius post Nepotiani exitium, saeviens per urbem aeternam urebat cuncta Bellona, ex primordiis minimis ad clades excita luctuosas, quas obliterasset utinam iuge silentium! ne forte paria quandoque temptentur, plus exemplis generalibus nocitura quam delictis.',1,38,43),(5,'Sed tamen haec cum ita tutius observentur, quidam vigore artuum inminuto rogati ad nuptias ubi aurum dextris manibus cavatis offertur, inpigre vel usque Spoletium pergunt. haec nobilium sunt instituta.',0,38,39),(6,'Cum saepe multa, tum memini domi in hemicyclio sedentem, ut solebat, cum et ego essem una et pauci admodum familiares, in eum sermonem illum incidere qui tum forte multis erat in ore. Meministi enim profecto, Attice, et eo magis, quod P. Sulpicio utebare multum, cum is tribunus plebis capitali odio a Q. Pompeio, qui tum erat consul, dissideret, quocum coniunctissime et amantissime vixerat, quanta esset hominum vel admiratio vel querella.',0,38,40),(7,'Post hoc impie perpetratum quod in aliis quoque iam timebatur, tamquam licentia crudelitati indulta per suspicionum nebulas aestimati quidam noxii damnabantur. quorum pars necati, alii puniti bonorum multatione actique laribus suis extorres nullo sibi relicto praeter querelas et lacrimas, stipe conlaticia victitabant, et civili iustoque imperio ad voluntatem converso cruentam, claudebantur opulentae domus et clarae.',0,38,41),(8,'Et prima post Osdroenam quam, ut dictum est, ab hac descriptione discrevimus, Commagena, nunc Euphratensis, clementer adsurgit, Hierapoli, vetere Nino et Samosata civitatibus amplis inlustris.',1,38,43);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (6,'Dubois Geoffrey','duboisgeoffrey03@gmail.com','autiste','dzere',0),(7,'PONTE Camille','camille.ponte.cp@gmail.com','sdqs','sdqfsf',0),(8,'SAINT PIERRE','swannou62@hotmail.fr','autiste','aaaadza',0);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (16,'2021-05-19','Bonjour, j\'ai une erreur dans le payement',38,39,1,1),(17,'2021-05-19','Suite à la demande, j\'ai rééditer le ...',38,39,0,1),(18,'2021-05-27','Bonjour, alors au niveau de la consultation par ... , Le verdict est passé en .... pour une durée indéterminé. Le prélèvement de .... sera automatisé par .... En attente du verdict final, je vous souhaite ...',38,39,0,1),(19,'2021-05-27','Merci , Cetero fuisset te Epicuri aut a deterritum doctrinis De minime artibus politus De Epicuri esse Epicuri politus politus deterritum instructior deterritum quamquam satis deterruisset enim igitur enim appellantur politus est tenent appellantur video quod artibus necesse eruditi ipse non studiis appellantur satis inquam est iis doctrinis qui fuisset studiis necesse.\r\n\r\n',38,39,1,0),(20,'2021-05-27','test',38,43,0,1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (80,'MEETING','2021-05-21',15,1,'ALIMONY','',1,38,39,0,NULL),(82,'MEETING','2021-05-20',10,1,'SUCCESSION','azae',1,38,39,0,NULL),(83,'MEETING','2021-05-21',14,1,'INTELLECTUAL_PROPERTY','zezzzz',1,38,39,1,NULL),(84,'MEETING','2021-05-20',9,1,'FAMILY','po',0,38,39,0,NULL),(86,'MEETING','2021-05-28',12,1,'ALIMONY','',1,42,39,0,NULL),(88,'MEETING','2021-05-28',15,1,'CONSTRUCTION','allouette',1,38,39,1,NULL),(89,'MEETING','2021-06-02',12,1,'CHILD_CARE','sandman',1,38,39,1,NULL),(90,'AUDIENCE','2021-05-31',13,4,'CRIMINAL','',0,38,40,0,NULL),(91,'MEETING','2021-05-31',8,3,'ADVICE','iiii',1,38,43,1,NULL),(92,'HOLIDAY','2021-05-29',0,24,'NO_CHOICE','',0,38,NULL,0,NULL),(93,'HOLIDAY','2021-05-30',0,24,'NO_CHOICE','',0,38,NULL,0,NULL),(94,'AUDIENCE','2021-05-31',11,2,'CRIMINAL','',0,38,43,0,NULL),(95,'MEETING','2021-05-31',17,1,'CRIMINAL','',1,38,43,0,NULL),(96,'AUDIENCE','2021-06-02',14,5,'ADVICE','',0,38,NULL,0,NULL),(97,'AUDIENCE','2021-06-01',15,5,'NO_CHOICE','',0,38,NULL,0,NULL);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (38,'Fernandez','Mylène','mymy','3e8738ad025865addb12e21c79c1f1e4','LAWYER'),(39,'Dubois','Geoffrey','geo','8ac48b780a569a2fabfe7ce1018e8b50','CLIENT'),(40,'Ponte','Camille','cam','e0cdee00b05d237f53140af5ab43d7d5','CLIENT'),(41,'Murphy','Fripouille','frypon','e0ff5d96f8fb65a953c5a5b2158e0e23','CLIENT'),(42,'Domanech','Célia','double','56f9504d21135de69a29f634514f2b57','LAWYER'),(43,'Long','michael','mika','1f7a711390f46cb21bed3923fc6a7096','CLIENT');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-01  9:50:04

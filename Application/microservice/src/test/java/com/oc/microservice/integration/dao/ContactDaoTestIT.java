package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.ContactDao;
import com.oc.microservice.model.Contact;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class ContactDaoTestIT {
    @Autowired
    ContactDao contactDao;

    @Test
    public void saveContactTest_contact_noReturn(){
        Contact contact = new Contact();
        contact.setName("frip");
        contact.setMail("test@gmail.com");
        contact.setSubject("test");
        contact.setMessage("Message Test");
        contact.setSeen(false);

        contactDao.save(contact);
        Contact contactSearch = contactDao.findContactByNotSeen().get(3);
        boolean testCheck = !contactSearch.isSeen()
                && contactSearch.getName().equals(contact.getName())
                && contactSearch.getMail().equals(contact.getMail())
                && contactSearch.getSubject().equals(contact.getSubject())
                && contactSearch.getMessage().equals(contact.getMessage());
        Assert.assertTrue(testCheck);
        contactDao.delete(contact);
    }

    @Test
    public void findContactByNotSeenTest_No_ListContact(){
        List<Contact> contacts = contactDao.findContactByNotSeen();
        Assert.assertEquals(3,contacts.size());
        boolean testCheck = contacts.get(0).getId().equals(11l)
                && contacts.get(0).getName().equals("Domanech")
                && contacts.get(0).getMessage().equals("message6")
                && contacts.get(0).getMail().equals("mail6@gmail.com")
                && contacts.get(0).getSubject().equals("sujet6")
                && !contacts.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findContactBySeenTest_No_ListContact(){
        List<Contact> contacts = contactDao.findContactBySeen();
        Assert.assertEquals(5,contacts.size());
        boolean testCheck = contacts.get(0).getId().equals(6l)
                && contacts.get(0).getName().equals("Dubois")
                && contacts.get(0).getMessage().equals("message1")
                && contacts.get(0).getMail().equals("mail1@gmail.com")
                && contacts.get(0).getSubject().equals("sujet1")
                && contacts.get(0).isSeen();
        Assert.assertTrue(testCheck);
    }
}

package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.UserDao;
import com.oc.microservice.enums.Role;
import com.oc.microservice.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class UserDaoTestIT {
    @Autowired
    UserDao userDao;

    @Test
    public void saveUserTest_User_noReturn(){
        User user = new User();
        user.setFirstname("FirstnameTest");
        user.setName("NameTest");
        user.setRole(Role.CLIENT);
        user.setUsername("Test");
        user.setPassword("Test123");

        userDao.save(user);
        User userSearch = userDao.findByUsername("Test");
        boolean testCheck = false;
        if (user.getFirstname().equals(userSearch.getFirstname())
                && user.getName().equals(userSearch.getName())
                && user.getRole().equals(userSearch.getRole())
                && user.getUsername().equals(userSearch.getUsername())
                && user.getPassword().equals(userSearch.getPassword())) {
            testCheck = true;
        }
        Assert.assertTrue(testCheck);
        userDao.delete(userSearch);
    }

    @Test
    public void findUserByUsernameTest_Username_User(){

        User user = userDao.findByUsername("geo");
        boolean testCheck = false;
        if (user.getFirstname().equals("Geoffrey")
                && user.getName().equals("Dubois")
                && user.getRole().equals(Role.CLIENT)
                && user.getUsername().equals("geo")
                && user.getPassword().equals("8ac48b780a569a2fabfe7ce1018e8b50")
                && user.getId().equals(2l)) {
            testCheck = true;
        }
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findLawyerTest_No_ListUser(){

        List<User> users = userDao.findLawyer();
        boolean testCheck = false;
        Assert.assertEquals(1,users.size());
        if (users.get(0).getFirstname().equals("Mylène")
                && users.get(0).getName().equals("Fernandez")
                && users.get(0).getRole().equals(Role.LAWYER)
                && users.get(0).getUsername().equals("mymy")
                && users.get(0).getPassword().equals("3e8738ad025865addb12e21c79c1f1e4")
                && users.get(0).getId().equals(1l)
        ) {
            testCheck = true;
        }
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findAllClientTest_No_ListUser(){

        List<User> users = userDao.findAllClient();
        boolean testCheck = false;
        Assert.assertEquals(3,users.size());
        if (users.get(0).getFirstname().equals("Geoffrey")
                && users.get(0).getName().equals("Dubois")
                && users.get(0).getRole().equals(Role.CLIENT)
                && users.get(0).getUsername().equals("geo")
                && users.get(0).getPassword().equals("8ac48b780a569a2fabfe7ce1018e8b50")
                && users.get(0).getId().equals(2l)
        ) {
            testCheck = true;
        }
        Assert.assertTrue(testCheck);
    }
}

package com.oc.ClientWeb.service;

import com.oc.ClientWeb.beans.UserBean;
import com.oc.ClientWeb.enums.Reason;
import com.oc.ClientWeb.enums.Role;
import com.oc.ClientWeb.model.TaskForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * TaskFormCheck characterized by
 *
 * customerError error of customer
 * lawyerError error of lawyer
 * reasonNoChoice No reason selected
 * hourNoSelected No hour selected
 * descriptionSize description is greater than or equal to 500
 */
@Slf4j
@Service
public class TaskFormCheck {
    private static boolean customerError;
    private static boolean lawyerError;
    private static boolean reasonNoChoice;
    private static boolean hourNoSelected;
    private static boolean descriptionSize;

    @Override
    public String toString() {
        return "customerError = " + customerError +"\n"
                +"lawyerError = " + lawyerError+"\n"
                +"reasonNoChoice = " + reasonNoChoice+"\n"
                +"hourNoSelected = " + hourNoSelected+"\n"
                +"descriptionSize = " + descriptionSize;
    }

    public TaskFormCheck() {
        log.debug("[ClientWeb/TaskFormCheck/TaskFormCheck()] instanciation ");
        init();
    }

    /**
     * Make all boolean to false
     */
    public void init(){
        log.debug("[ClientWeb/TaskFormCheck/init()]");
        customerError = false;
        lawyerError = false;
        reasonNoChoice = false;
        hourNoSelected = false;
        descriptionSize = false;
    }

    /**
     * init and check all param
     * @param taskForm TaskForm to check
     * @param lawyer lawyer of ref
     * @param customer customer of ref
     */
    public void evaluate(TaskForm taskForm,UserBean lawyer, UserBean customer) {
        log.debug("[ClientWeb/TaskFormCheck/evaluate(TaskForm,UserBean,UserBean)] with taskForm = "+taskForm+" and lawyer = "+lawyer+" and customer = "+ customer);
        init();
        customerCheck(taskForm,customer);
        lawyerCheck(taskForm,lawyer);
        reasonCheck(taskForm);
        hourCheck(taskForm);
        descriptionCheck(taskForm);
    }

    /**
     * if one of boolean is true then an error exist then return false
     * @return boolean
     */
    public static boolean validate(){
        log.debug("[ClientWeb/TaskFormCheck/validate()]");
        if(customerError)
            return false;
        if(lawyerError)
            return false;
        if(reasonNoChoice)
            return false;
        if(hourNoSelected)
            return false;
        if(descriptionSize)
            return false;
        log.debug("[ClientWeb/TaskFormCheck/validate()] Return -> true");
        return true;
    }

    /**
     * Check if hour are selected
     * @param taskForm form to check
     */
    private void hourCheck(TaskForm taskForm) {
        log.debug("[ClientWeb/TaskFormCheck/hourCheck(TaskForm)] with taskForm = "+taskForm);
        if(taskForm.getTask().getHour() == -1)
            hourNoSelected = true;
    }

    /**
     * check if Reason is selected
     * @param taskForm form to check
     */
    private void reasonCheck(TaskForm taskForm) {
        log.debug("[ClientWeb/TaskFormCheck/reasonCheck(TaskForm)] with taskForm = "+taskForm);
        if(taskForm.getTask().getReason() == Reason.NO_CHOICE)
            reasonNoChoice = true;
    }


    /**
     * check if description is not >= 500
     * @param taskForm form to check
     */
    private void descriptionCheck(TaskForm taskForm) {
        log.debug("[ClientWeb/TaskFormCheck/descriptionCheck(TaskForm)] with taskForm = "+taskForm);
        if(taskForm.getTask().getDescription().length() >= 500)
            descriptionSize = true;
    }


    /**
     * check if taskform.idLawyer = lawyer.getId and lawyer.role = lawyer
     * @param taskForm form to check
     * @param lawyer lawyer expected
     */
    private void lawyerCheck(TaskForm taskForm, UserBean lawyer) {
        log.debug("[ClientWeb/TaskFormCheck/lawyerCheck(TaskForm,UserBean)] with taskForm = "+taskForm+" and lawyer = "+lawyer );
        if(lawyer.getId() != taskForm.getIdLawyer() || lawyer.getRole() != Role.LAWYER)
            lawyerError = true;
    }

    /**
     * check if taskform.task.customer.id = customer.getId and lawyer.role = lawyer
     * @param taskForm form to check
     * @param customer customer expected
     */
    private void customerCheck(TaskForm taskForm, UserBean customer) {
        log.debug("[ClientWeb/TaskFormCheck/customerCheck(TaskForm,UserBean)] with taskForm = "+taskForm+" and customer = "+customer );
        if(customer.getId() != taskForm.getTask().getCustomer().getId())
            customerError = true;
    }

    public static boolean isCustomerError() {
        return customerError;
    }

    public static void setCustomerError(boolean customerError) {
        TaskFormCheck.customerError = customerError;
    }

    public static boolean isLawyerError() {
        return lawyerError;
    }

    public static void setLawyerError(boolean lawyerError) {
        TaskFormCheck.lawyerError = lawyerError;
    }

    public static boolean isReasonNoChoice() {
        return reasonNoChoice;
    }

    public static void setReasonNoChoice(boolean reasonNoChoice) {
        TaskFormCheck.reasonNoChoice = reasonNoChoice;
    }

    public static boolean isHourNoSelected() {
        return hourNoSelected;
    }

    public static void setHourNoSelected(boolean hourNoSelected) {
        TaskFormCheck.hourNoSelected = hourNoSelected;
    }

    public static boolean isDescriptionSize() {
        return descriptionSize;
    }

    public static void setDescriptionSize(boolean descriptionSize) {
        TaskFormCheck.descriptionSize = descriptionSize;
    }


}

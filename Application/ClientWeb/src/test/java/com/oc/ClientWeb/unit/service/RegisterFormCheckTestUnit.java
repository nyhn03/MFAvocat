package com.oc.ClientWeb.unit.service;

import com.oc.ClientWeb.service.RegisterFormCheck;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.util.Calendar;

@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class RegisterFormCheckTestUnit {
    @InjectMocks
    RegisterFormCheck registerFormCheck;

    @Test
    public void validateSettersAndGetters() {
        final PojoClass userPojo = PojoClassFactory.getPojoClass(RegisterFormCheck.class);

        final Validator validator = ValidatorBuilder.create()
                .with(new SetterTester(), new GetterTester())
                .build();
        validator.validate(userPojo);
    }

    @Test
    public void validateTest_no_boolean(){
        Assert.assertTrue(registerFormCheck.validate());
    }

    @Test
    public void nameCheck_emptyName_true(){
        registerFormCheck.nameCheck("");
        Assert.assertTrue(registerFormCheck.isNameEmpty());
    }

    @Test
    public void nameCheck_sizeName_true(){
        registerFormCheck.nameCheck("a");
        Assert.assertTrue(registerFormCheck.isNameSize());
    }

    @Test
    public void firstnameCheck_emptyFirstname_true(){
        registerFormCheck.firstnameCheck("");
        Assert.assertTrue(registerFormCheck.isFirstnameEmpty());
    }

    @Test
    public void firstnameCheck_sizeFirstname_true(){
        registerFormCheck.firstnameCheck("a");
        Assert.assertTrue(registerFormCheck.isFirstnameSize());
    }

    @Test
    public void addressNumCheck_zero_true(){
        registerFormCheck.addressNumCheck(0);
        Assert.assertTrue(registerFormCheck.isAddressNumEmpty());
    }

    @Test
    public void addressNameCheck_emptyAddressName_true(){
        registerFormCheck.addressNameCheck("");
        Assert.assertTrue(registerFormCheck.isAddressNameEmpty());
    }

    @Test
    public void addressNameCheck_sizeAddressName_true(){
        registerFormCheck.addressNameCheck("a");
        Assert.assertTrue(registerFormCheck.isAddressNameSize());
    }

    @Test
    public void addressCityCheck_emptyAddressCity_true(){
        registerFormCheck.addressCityCheck("");
        Assert.assertTrue(registerFormCheck.isAddressCityEmpty());
    }

    @Test
    public void addressCityCheck_sizeAddressCity_true(){
        registerFormCheck.addressCityCheck("a");
        Assert.assertTrue(registerFormCheck.isAddressCitySize());
    }

    @Test
    public void zipCheck_emptyZip_true(){
        registerFormCheck.zipCheck("");
        Assert.assertTrue(registerFormCheck.isZipEmpty());
    }

    @Test
    public void zipCheck_sizeZip_true(){
        registerFormCheck.zipCheck("a");
        Assert.assertTrue(registerFormCheck.isZipSize());
    }
    @Test
    public void mailCheck_emptyMail_true(){
        registerFormCheck.mailCheck("","");
        Assert.assertTrue(registerFormCheck.isMailEmpty());
        Assert.assertTrue(registerFormCheck.isMailCheckEmpty());
    }

    @Test
    public void mailCheck_sizeMail_true(){
        registerFormCheck.mailCheck("a","a");
        Assert.assertTrue(registerFormCheck.isMailSize());
    }
    @Test
    public void mailCheck_differentMail_true(){
        registerFormCheck.mailCheck("abc@mail.com","ab@mail.com");
        Assert.assertTrue(registerFormCheck.isMailDifferent());
    }
    @Test
    public void phoneCheck_emptyPhone_true(){
        registerFormCheck.phoneCheck("");
        Assert.assertTrue(registerFormCheck.isPhoneEmpty());
    }

    @Test
    public void phoneCheck_sizePhone_true(){
        registerFormCheck.phoneCheck("0606060606060606");
        Assert.assertTrue(registerFormCheck.isPhoneSize());
    }
    @Test
    public void usernameCheck_emptyUsername_true(){
        registerFormCheck.usernameCheck("");
        Assert.assertTrue(registerFormCheck.isPseudoEmpty());
    }

    @Test
    public void usernameCheck_sizeUsername_true(){
        registerFormCheck.usernameCheck("lo");
        Assert.assertTrue(registerFormCheck.isPseudoSize());
    }

    @Test
    public void passwordCheck_emptyPassword_true(){
        registerFormCheck.passwordCheck("","");
        Assert.assertTrue(registerFormCheck.isPasswordEmpty());
        Assert.assertTrue(registerFormCheck.isPasswordCheckEmpty());
    }

    @Test
    public void passwordCheck_sizePassword_true(){
        registerFormCheck.passwordCheck("lo","lo");
        Assert.assertTrue(registerFormCheck.isPasswordSize());
    }
    @Test
    public void passwordCheck_differentPassword_true(){
        registerFormCheck.passwordCheck("lo123","lo1234");
        Assert.assertTrue(registerFormCheck.isPasswordDifferent());
    }
}

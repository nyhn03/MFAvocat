package com.oc.ClientAdmin.service;

import com.oc.ClientAdmin.beans.TaskBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Function to help task class
 */
@Slf4j
@Service
public class TaskService {
    /**
     * Calculate the pourcent of task done for a list of task
     * @param taskList List of task
     * @return int
     */
    public int pourcent(List<TaskBean> taskList){
        log.debug("[ClientAdmin/TaskService/pourcent(List<TaskBean>)] with tasklist = "+taskList );
        double division;
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 1 ){
            hour += 12;
        }
        double count = 0.0;
        for(int i=0; i<taskList.size();i++){
            if(taskList.get(i).getHour()<hour)
                count++;
        }
        double size = (double)taskList.size();
        division =count / size;
        int result =(int) (division*100);
        log.debug("[ClientAdmin/TaskService/pourcent(List<TaskBean>)] Return -> \n"+result+"\n" );
        return result;
    }

    /**
     * Return a task in progress
     * @param tasks List of Tasks
     * @return task
     */
    public TaskBean currentTask(List<TaskBean> tasks){
        log.debug("[ClientAdmin/TaskService/currentTask(List<TaskBean>)] with tasks = "+tasks );
        TaskBean task = null;
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 1 ){
            hour += 12;
        }
        for(int i =0; i < tasks.size();i++){
            if(tasks.get(i).getHour() <= hour && tasks.get(i).getHour()+tasks.get(i).getDuration()> hour){
                task = tasks.get(i);
            }
        }
        log.debug("[ClientAdmin/TaskService/currentTask(List<TaskBean>)] Return -> \n"+task+"\n" );
        return task;
    }

    /**
     * Return next task
     * @param tasks List of Tasks
     * @return task
     */
    public TaskBean nextTask(List<TaskBean> tasks){
        log.debug("[ClientAdmin/TaskService/nextTask(List<TaskBean>)] with tasks = "+tasks );
        TaskBean task = null;
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 1 ){
            hour += 12;
        }
        List<TaskBean> taskNotPassed = new ArrayList<>();
        for(int i =0; i < tasks.size();i++){
            if(tasks.get(i).getHour()+tasks.get(i).getDuration() > hour+tasks.get(i).getDuration()){
                taskNotPassed.add(tasks.get(i));
            }
        }
        if(taskNotPassed.size() > 0)
            task=taskNotPassed.get(0);
        for (int j = 0; j < taskNotPassed.size();j++){
            if(taskNotPassed.get(j).getHour() < task.getHour()){
                task = taskNotPassed.get(j);
            }
        }
        log.debug("[ClientAdmin/TaskService/nextTask(List<TaskBean>)] Return -> \n"+task+"\n" );
        return task;
    }
}

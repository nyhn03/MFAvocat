package com.oc.ClientAdmin.service;

import com.oc.ClientAdmin.beans.AddressBean;
import com.oc.ClientAdmin.beans.UserBean;
import com.oc.ClientAdmin.model.RegisterForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * RegisterFormCheck is characterized by
 * boolean for address attribute
 * and user attribute
 * i.e ClientWeb RegisterFormCheck
 */
@Slf4j
@Service
public class RegisterFormCheck {
    private static boolean nameSize;
    private static boolean nameEmpty;
    private static boolean firstnameSize;
    private static boolean firstnameEmpty;
    private static boolean addressNameSize;
    private static boolean addressNameEmpty;
    private static boolean addressNumEmpty;
    private static boolean addressCitySize;
    private static boolean addressCityEmpty;
    private static boolean zipSize;
    private static boolean zipEmpty;
    private static boolean mailSize;
    private static boolean mailExisting;
    private static boolean mailDifferent;
    private static boolean mailEmpty;
    private static boolean mailCheckEmpty;
    private static boolean phoneSize;
    private static boolean phoneEmpty;
    private static boolean pseudoSize;
    private static boolean pseudoEmpty;
    private static boolean pseudoExisting;
    private static boolean passwordSize;
    private static boolean passwordEmpty;
    private static boolean passwordDifferent;
    private static boolean passwordCheckEmpty;

    public RegisterFormCheck() {
        log.debug("[ClientAdmin/RegisterFormCheck/RegisterFormCheck()]");
        init();
    }

    public void init(){
        log.debug("[ClientAdmin/RegisterFormCheck/init()]");
        this.nameSize = false;
        this.nameEmpty = false;
        this.firstnameSize = false;
        this.firstnameEmpty = false;
        this.addressNameSize = false;
        this.addressNameEmpty = false;
        this.addressNumEmpty = false;
        this.addressCitySize = false;
        this.addressCityEmpty = false;
        this.zipSize = false;
        this.zipEmpty = false;
        this.mailSize = false;
        this.mailExisting = false;
        this.mailDifferent = false;
        this.mailEmpty = false;
        this.mailCheckEmpty = false;
        this.phoneSize = false;
        this.phoneEmpty = false;
        this.pseudoSize = false;
        this.pseudoEmpty = false;
        this.pseudoExisting = false;
        this.passwordSize = false;
        this.passwordEmpty = false;
        this.passwordDifferent = false;
        this.passwordCheckEmpty = false;
    }

    /**
     * This function initialize attributes
     * And evaluate all user attributes
     * @param registerForm  is a information of user to evaluate
     * @param request request of form to collect mailCheck and passwordCheck
     * @param addressMailSearch is a User research in database by mail
     * @param userPseudoSearch is a User research in database by pseudo
     */
    public void evaluate(RegisterForm registerForm, HttpServletRequest request, UserBean userPseudoSearch, AddressBean addressMailSearch){
        log.debug("[ClientAdmin/RegisterFormCheck/evaluate(RegisterForm,UserBean,AddressBean)] with registerForm = "+registerForm+" and userPseudoSearch = "+userPseudoSearch+" and addressMailSearch = "+addressMailSearch);
        init();
        nameCheck(registerForm.getUser().getName());
        firstnameCheck(registerForm.getUser().getFirstname());
        addressNumCheck(registerForm.getAddress().getNum());
        addressNameCheck(registerForm.getAddress().getName());
        addressCityCheck(registerForm.getAddress().getCity());
        zipCheck(registerForm.getAddress().getZipCode());
        if(addressMailSearch != null)
            mailExisting = true;
        else
            mailCheck(registerForm.getAddress().getMail(), request.getParameter("mail2"));
        phoneCheck(registerForm.getAddress().getPhone());
        if(userPseudoSearch != null)
            pseudoExisting = true;
        else
            usernameCheck(registerForm.getUser().getUsername());
        passwordCheck(registerForm.getUser().getPassword(),request.getParameter("password2"));
    }

    public void evaluateEdit(RegisterForm registerForm){
        log.debug("[ClientAdmin/RegisterFormCheck/evaluateEdit(RegisterForm)] with registerForm = "+registerForm);
        init();
        nameCheck(registerForm.getUser().getName());
        firstnameCheck(registerForm.getUser().getFirstname());
        addressNumCheck(registerForm.getAddress().getNum());
        addressNameCheck(registerForm.getAddress().getName());
        addressCityCheck(registerForm.getAddress().getCity());
        zipCheck(registerForm.getAddress().getZipCode());
        phoneCheck(registerForm.getAddress().getPhone());
    }

    /**
     * Validate return false if one of boolean is true else return true
     * @return Boolean of acceptance
     */
    public static boolean validate(){
        log.debug("[ClientAdmin/RegisterFormCheck/validate()]");
        if(nameSize)
            return false;
        if(nameEmpty)
            return false;
        if(firstnameSize)
            return false;
        if(firstnameEmpty)
            return false;
        if(addressNameSize)
            return false;
        if(addressNameEmpty)
            return false;
        if(addressCitySize)
            return false;
        if(addressCityEmpty)
            return false;
        if(addressNumEmpty)
            return false;
        if(zipSize)
            return false;
        if(zipEmpty)
            return false;
        if(mailSize)
            return false;
        if(mailEmpty)
            return false;
        if(mailExisting)
            return false;
        if(mailDifferent)
            return false;
        if(mailCheckEmpty)
            return false;
        if(phoneSize)
            return false;
        if(phoneEmpty)
            return false;
        if(pseudoSize)
            return false;
        if(pseudoEmpty)
            return false;
        if(pseudoExisting)
            return false;
        if(passwordSize)
            return false;
        if(passwordEmpty)
            return false;
        if(passwordDifferent)
            return false;
        if(passwordCheckEmpty)
            return false;
        log.debug("[ClientAdmin/RegisterFormCheck/validate()] Return -> true");
        return true;
    }
    /**
     * Check the string name
     * If is not null and between 3 and 50 characters
     * @param name is a name of user
     */
    public void nameCheck(String name){
        log.debug("[ClientAdmin/RegisterFormCheck/nameCheck(String)] with name = "+name);
        if(name.length() == 0)
            nameEmpty = true;
        if(name.length() <= 3 || name.length() >= 50)
            nameSize = true;
    }

    /**
     * Check the string firstname
     * If is not null and between 3 and 50 characters
     * @param firstname is a firstname of user
     */
    public void firstnameCheck(String firstname){
        log.debug("[ClientAdmin/RegisterFormCheck/firstnameCheck(String)] with firstname = "+firstname);
        if(firstname.length() == 0)
            firstnameEmpty = true;
        if(firstname.length() <= 3 || firstname.length() >= 50)
            firstnameSize = true;
    }

    /**
     * Check the string address
     * If is not egal to 0
     * @param num is a num of address of user
     */
    public void addressNumCheck(int num){
        log.debug("[ClientAdmin/RegisterFormCheck/addressNumCheck(int)] with num = "+num);
        if(num == 0)
            addressNumEmpty = true;
    }

    /**
     * Check the string address
     * If is not null and between 3 and 50 characters
     * @param name is a name of address of user
     */
    public void addressNameCheck(String name){
        log.debug("[ClientAdmin/RegisterFormCheck/addressNameCheck(String)] with name = "+name);
        if(name.length() == 0)
            addressNameEmpty = true;
        if(name.length() <= 3 || name.length() >= 50)
            addressNameSize = true;
    }

    /**
     * Check the string address
     * If is not null and between 3 and 50 characters
     * @param city is a city of address of user
     */
    public void addressCityCheck(String city){
        log.debug("[ClientAdmin/RegisterFormCheck/addressCityCheck(String)] with city = "+city);
        if(city.length() == 0)
            addressCityEmpty = true;
        if(city.length() <= 3 || city.length() >= 50)
            addressCitySize = true;
    }

    /**
     * Check the string zip
     * If is not null and egal to 5 characters
     * @param zip is a zip of user
     */
    public void zipCheck(String zip){
        log.debug("[ClientAdmin/RegisterFormCheck/zipCheck(String)] with zip = "+zip);
        if(zip.length() == 0)
            zipEmpty = true;
        if(zip.length() != 5)
            zipSize = true;
    }

    /**
     * Check the string mail and compare with mailconfirm
     * If is not null and between 3 and 50 characters
     * @param mail is a mail of user
     * @param mailConfirm is a mailcheck by request
     */
    public void mailCheck(String mail, String mailConfirm){
        log.debug("[ClientAdmin/RegisterFormCheck/mailCheck(String,String)] with mail = "+mail+" and mailConfirm = "+mailConfirm);
        if(mail.length() == 0)
            mailEmpty = true;
        if(mail.length() <= 3 || mail.length() >= 50)
            mailSize = true;
        if(mailConfirm.length() == 0)
            mailCheckEmpty = true;
        if(!mail.equals(mailConfirm))
            mailDifferent = true;
    }

    /**
     * Check the string phone
     * If is not null and different of 10 et 12 characters
     * @param phone is a phone of user
     */
    public void phoneCheck(String phone){
        log.debug("[ClientAdmin/RegisterFormCheck/phoneCheck(String)] with phone = "+phone);
        if(phone.length() == 0)
            phoneEmpty = true;
        if(phone.length() != 10 && phone.length() != 12)
            phoneSize = true;
    }

    /**
     * Check the string pseudo
     * If is not null and between 3 and 50 characters
     * @param pseudo is a pseudo of user
     */
    public void usernameCheck(String pseudo){
        log.debug("[ClientAdmin/RegisterFormCheck/usernameCheck(String)] with pseudo = "+pseudo);
        if(pseudo.length() == 0)
            pseudoEmpty = true;
        if(pseudo.length() <= 3 || pseudo.length() >= 50)
            pseudoSize = true;
    }

    /**
     * Check the string password and compare with passwordconfirm
     * If is not null and between 3 and 100 characters
     * @param password is a password of user
     * @param passwordConfirm is a passwordcheck by request
     */
    public void passwordCheck(String password, String passwordConfirm){
        log.debug("[ClientAdmin/RegisterFormCheck/passwordCheck(String,String)] with password = "+password+" and passwordConfirm = "+passwordConfirm);
        if(password.length() == 0)
            passwordEmpty = true;
        if(password.length() <= 3 || password.length() >= 100)
            passwordSize = true;
        if(passwordConfirm.length() == 0)
            passwordCheckEmpty = true;
        if(!password.equals(passwordConfirm))
            passwordDifferent = true;
    }

    @Override
    public String toString() {
        return "RegisterFormCheck{" +
                "nameSize=" + nameSize +
                ", nameEmpty=" + nameEmpty +
                ", firstnameSize=" + firstnameSize +
                ", firstnameEmpty=" + firstnameEmpty +
                ", addressNameSize=" + addressNameSize +
                ", addressNameEmpty=" + addressNameEmpty +
                ", addressNumEmpty=" + addressNumEmpty +
                ", addressCitySize=" + addressCitySize +
                ", addressCityEmpty=" + addressCityEmpty +
                ", zipSize=" + zipSize +
                ", zipEmpty=" + zipEmpty +
                ", mailSize=" + mailSize +
                ", mailExisting=" + mailExisting +
                ", mailDifferent=" + mailDifferent +
                ", mailEmpty=" + mailEmpty +
                ", mailCheckEmpty=" + mailCheckEmpty +
                ", phoneSize=" + phoneSize +
                ", phoneEmpty=" + phoneEmpty +
                ", pseudoSize=" + pseudoSize +
                ", pseudoEmpty=" + pseudoEmpty +
                ", pseudoExisting=" + pseudoExisting +
                ", passwordSize=" + passwordSize +
                ", passwordEmpty=" + passwordEmpty +
                ", passwordDifferent=" + passwordDifferent +
                ", passwordCheckEmpty=" + passwordCheckEmpty +
                '}';
    }

    public boolean isNameSize() {
        return nameSize;
    }

    public void setNameSize(boolean nameSize) {
        this.nameSize = nameSize;
    }

    public boolean isNameEmpty() {
        return nameEmpty;
    }

    public void setNameEmpty(boolean nameEmpty) {
        this.nameEmpty = nameEmpty;
    }

    public boolean isFirstnameSize() {
        return firstnameSize;
    }

    public void setFirstnameSize(boolean firstnameSize) {
        this.firstnameSize = firstnameSize;
    }

    public boolean isFirstnameEmpty() {
        return firstnameEmpty;
    }

    public void setFirstnameEmpty(boolean firstnameEmpty) {
        this.firstnameEmpty = firstnameEmpty;
    }

    public boolean isAddressNameSize() {
        return addressNameSize;
    }

    public void setAddressNameSize(boolean addressNameSize) {
        this.addressNameSize = addressNameSize;
    }

    public boolean isAddressNameEmpty() {
        return addressNameEmpty;
    }

    public void setAddressNameEmpty(boolean addressNameEmpty) {
        this.addressNameEmpty = addressNameEmpty;
    }

    public boolean isAddressNumEmpty() {
        return addressNumEmpty;
    }

    public void setAddressNumEmpty(boolean addressNumEmpty) {
        this.addressNumEmpty = addressNumEmpty;
    }

    public boolean isAddressCitySize() {
        return addressCitySize;
    }

    public void setAddressCitySize(boolean addressCitySize) {
        this.addressCitySize = addressCitySize;
    }

    public boolean isAddressCityEmpty() {
        return addressCityEmpty;
    }

    public void setAddressCityEmpty(boolean addressCityEmpty) {
        this.addressCityEmpty = addressCityEmpty;
    }

    public boolean isZipSize() {
        return zipSize;
    }

    public void setZipSize(boolean zipSize) {
        this.zipSize = zipSize;
    }

    public boolean isZipEmpty() {
        return zipEmpty;
    }

    public void setZipEmpty(boolean zipEmpty) {
        this.zipEmpty = zipEmpty;
    }

    public boolean isMailSize() {
        return mailSize;
    }

    public void setMailSize(boolean mailSize) {
        this.mailSize = mailSize;
    }

    public boolean isMailExisting() {
        return mailExisting;
    }

    public void setMailExisting(boolean mailExisting) {
        this.mailExisting = mailExisting;
    }

    public boolean isMailDifferent() {
        return mailDifferent;
    }

    public void setMailDifferent(boolean mailDifferent) {
        this.mailDifferent = mailDifferent;
    }

    public boolean isMailEmpty() {
        return mailEmpty;
    }

    public void setMailEmpty(boolean mailEmpty) {
        this.mailEmpty = mailEmpty;
    }

    public boolean isMailCheckEmpty() {
        return mailCheckEmpty;
    }

    public void setMailCheckEmpty(boolean mailCheckEmpty) {
        this.mailCheckEmpty = mailCheckEmpty;
    }

    public boolean isPhoneSize() {
        return phoneSize;
    }

    public void setPhoneSize(boolean phoneSize) {
        this.phoneSize = phoneSize;
    }

    public boolean isPhoneEmpty() {
        return phoneEmpty;
    }

    public void setPhoneEmpty(boolean phoneEmpty) {
        this.phoneEmpty = phoneEmpty;
    }

    public boolean isPseudoSize() {
        return pseudoSize;
    }

    public void setPseudoSize(boolean pseudoSize) {
        this.pseudoSize = pseudoSize;
    }

    public boolean isPseudoEmpty() {
        return pseudoEmpty;
    }

    public void setPseudoEmpty(boolean pseudoEmpty) {
        this.pseudoEmpty = pseudoEmpty;
    }

    public boolean isPseudoExisting() {
        return pseudoExisting;
    }

    public void setPseudoExisting(boolean pseudoExisting) {
        this.pseudoExisting = pseudoExisting;
    }

    public boolean isPasswordSize() {
        return passwordSize;
    }

    public void setPasswordSize(boolean passwordSize) {
        this.passwordSize = passwordSize;
    }

    public boolean isPasswordEmpty() {
        return passwordEmpty;
    }

    public void setPasswordEmpty(boolean passwordEmpty) {
        this.passwordEmpty = passwordEmpty;
    }

    public boolean isPasswordDifferent() {
        return passwordDifferent;
    }

    public void setPasswordDifferent(boolean passwordDifferent) {
        this.passwordDifferent = passwordDifferent;
    }

    public boolean isPasswordCheckEmpty() {
        return passwordCheckEmpty;
    }

    public void setPasswordCheckEmpty(boolean passwordCheckEmpty) {
        this.passwordCheckEmpty = passwordCheckEmpty;
    }

    public void evaluateModif(RegisterForm registerForm) {
        init();
        nameCheck(registerForm.getUser().getName());
        firstnameCheck(registerForm.getUser().getFirstname());
        addressNumCheck(registerForm.getAddress().getNum());
        addressNameCheck(registerForm.getAddress().getName());
        addressCityCheck(registerForm.getAddress().getCity());
        zipCheck(registerForm.getAddress().getZipCode());
        phoneCheck(registerForm.getAddress().getPhone());
    }
}

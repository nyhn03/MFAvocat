package com.oc.microservice.model;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * contact characterized by
 * id
 * name
 * mail
 * subject
 * message
 * seen if the lawyer see note
 */
@Slf4j
@Entity
@Table(name="contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Length(min=3, max = 50,message = "Le nom doit être compris entre 3 et 50 caractères.")
    private String name;
    @Length(min=3, max = 50,message = "La mail doit être compris entre 3 et 50 caractères.")
    private String mail;
    @Length(min=3, max = 100,message = "La sujet doit être compris entre 3 et 100 caractères.")
    private String subject;
    @Length(min=3, max = 1000,message = "La description doit être compris entre 3 et 1000 caractères.")
    private String message;
    private boolean seen;

    public Contact() {
        log.debug("[Microservice/Contact()] instantiation of a new contact");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", seen=" + seen +
                '}';
    }
}

package com.oc.ClientAdmin.controller;
import com.oc.ClientAdmin.beans.*;
import com.oc.ClientAdmin.enums.Reason;
import com.oc.ClientAdmin.enums.Role;
import com.oc.ClientAdmin.model.BillForm;
import com.oc.ClientAdmin.model.RegisterForm;
import com.oc.ClientAdmin.model.TaskForm;
import com.oc.ClientAdmin.proxies.MicroserviceProxy;
import com.oc.ClientAdmin.service.EncryptPassword;
import com.oc.ClientAdmin.service.RegisterFormCheck;
import com.oc.ClientAdmin.service.TaskFormCheck;
import com.oc.ClientAdmin.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
public class ClientAdminController {

    @Autowired
    RegisterFormCheck registerFormCheck;

    @Autowired
    MicroserviceProxy microserviceProxy;

    @Autowired
    EncryptPassword encryptPassword;

    @Autowired
    TaskService taskService;

    @Autowired
    TaskFormCheck taskFormCheck;

    public static String days[] = {"Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"};
    public static String month[] = {"Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"};
    private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

    @RequestMapping("/")
    public String homepage(){
        log.debug("[ClientAdmin/ClientAdminController/]");
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String index(Model model,HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/index] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent",userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "redirect:/logIn";
        List<TaskBean> tasks = microserviceProxy.findTaskByDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        model.addAttribute("nbCustomers",microserviceProxy.findCustomers().size());
        model.addAttribute("pourcent", taskService.pourcent(tasks));
        alert(model,userCurrent);
        model.addAttribute("currentTask",taskService.currentTask(tasks));
        model.addAttribute("nextTask",taskService.nextTask(tasks));
        return "index";
    }

    @RequestMapping("/addCustomer")
    public String addCustomer(HttpServletRequest request,Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/addCustomer] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        RegisterForm registerForm = new RegisterForm();
        model.addAttribute("registerForm",registerForm);
        model.addAttribute("registerFormCheck",registerFormCheck);
        alert(model,userCurrent);
        return "addCustomer";
    }

    @RequestMapping("/registerCheck")
    public String viewRegisterCheckPageAndSaveUserAddress(Model model, HttpServletRequest request, @ModelAttribute("registerForm") RegisterForm registerForm) {
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/registerCheck] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        registerForm.getUser().setRole(Role.CLIENT);
        registerFormCheck.init();
        if(registerForm.getUser().getUsername() == ""){
            registerFormCheck.setPseudoEmpty(true);
            registerFormCheck.setPseudoSize(true);
            model.addAttribute("registerFormCheck", registerFormCheck);;
            model.addAttribute("registerForm",registerForm);
            return "addCustomer";
        }
        if(registerForm.getAddress().getMail() == ""){
            registerFormCheck.setMailEmpty(true);
            registerFormCheck.setMailSize(true);
            model.addAttribute("registerFormCheck", registerFormCheck);
            model.addAttribute("registerForm",registerForm);
            return "addCustomer";
        }
        registerFormCheck.evaluate(registerForm,request,microserviceProxy.findByUsername(registerForm.getUser().getUsername()),microserviceProxy.findByMail(registerForm.getAddress().getMail()));
        if (RegisterFormCheck.validate()) {
            registerForm.getUser().setPassword(encryptPassword.encrypt(registerForm.getUser()));
            microserviceProxy.addUser(registerForm.getUser());
            registerForm.getAddress().setUser(microserviceProxy.findByUsername(registerForm.getUser().getUsername()));
            microserviceProxy.addAddress(registerForm.getAddress());
            return "redirect:/index";
        } else {
            model.addAttribute("registerFormCheck", registerFormCheck);
            model.addAttribute("registerForm",registerForm);

            return "addCustomer";
        }
    }

    @RequestMapping("/addLawyer")
    public String addLawyer(HttpServletRequest request,Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/addLawyer] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        RegisterForm registerForm = new RegisterForm();
        model.addAttribute("registerForm",registerForm);
        model.addAttribute("registerFormCheck",registerFormCheck);
        return "addLawyer";
    }

    @RequestMapping("/registerCheckLawyer")
    public String viewRegisterCheckLawyerPageAndSaveUserAddress(Model model, HttpServletRequest request, @ModelAttribute("registerForm") RegisterForm registerForm) {
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/registerCheckLawyer] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        registerForm.getUser().setRole(Role.LAWYER);
        registerFormCheck.init();
        if(registerForm.getUser().getUsername() == ""){
            registerFormCheck.setPseudoEmpty(true);
            registerFormCheck.setPseudoSize(true);
            model.addAttribute("registerFormCheck", registerFormCheck);;
            model.addAttribute("registerForm",registerForm);
            return "addLawyer";
        }
        if(registerForm.getAddress().getMail() == ""){
            registerFormCheck.setMailEmpty(true);
            registerFormCheck.setMailSize(true);
            model.addAttribute("registerFormCheck", registerFormCheck);
            model.addAttribute("registerForm",registerForm);
            return "addLawyer";
        }
        registerFormCheck.evaluate(registerForm,request,microserviceProxy.findByUsername(registerForm.getUser().getUsername()),microserviceProxy.findByMail(registerForm.getAddress().getMail()));
        if (RegisterFormCheck.validate()) {
            registerForm.getUser().setPassword(encryptPassword.encrypt(registerForm.getUser()));
            microserviceProxy.addUser(registerForm.getUser());
            registerForm.getAddress().setUser(microserviceProxy.findByUsername(registerForm.getUser().getUsername()));
            microserviceProxy.addAddress(registerForm.getAddress());
            return "redirect:/index";
        } else {
            model.addAttribute("registerFormCheck", registerFormCheck);
            model.addAttribute("registerForm",registerForm);
            return "addLawyer";
        }
    }


    @RequestMapping("/addBill")
    public String addBill(HttpServletRequest request,Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/addBill] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("billForm",new BillForm());
        model.addAttribute("Customers",microserviceProxy.findAllCustomers());
        return "addBill";
    }

    @RequestMapping("/billCheck")
    public String billCheck(HttpServletRequest request,Model model,@ModelAttribute("billForm") BillForm billForm){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/billCheck] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        if(billForm.getBill().getPrice() <= 0 || billForm.getBill().getDescription().length() > 500 || billForm.getBill().getService().length() > 50 || billForm.getBill().getService().length() == 0)
            return "redirect:/addBill";
        BillBean newBill;
        newBill = billForm.getBill();
        newBill.setCustomer(microserviceProxy.findUserById(billForm.getIdCustomer()));
        newBill.setLawyer(userCurrent);
        newBill.setPaid(false);
        newBill.setDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        microserviceProxy.addBill(newBill);
        return "redirect:/billNotPaid";
    }

    @RequestMapping(value = "/billPaid/{id}")
    public String isPaid(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/billPaid/{id}] with id = "+id);
        BillBean bill = microserviceProxy.findBillById(id);
        bill.setPaid(true);
        microserviceProxy.paid(bill);
        return "redirect:/billNotPaid";
    }

    @RequestMapping(value = "/deleteBill/{id}")
    public String deleteBill(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/deleteBill/{id}] with id = "+id);
        BillBean bill = microserviceProxy.findBillById(id);
        if(!bill.isPaid())
            microserviceProxy.deleteBill(id);
        return "redirect:/billNotPaid";
    }

    @RequestMapping("/bill/{id}")
    public String displayBill(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/bill/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        BillBean bill = microserviceProxy.findBillById(id);
        model.addAttribute("bill",bill);
        model.addAttribute("address",microserviceProxy.findAddressByUserId(bill.getCustomer().getId()));
        model.addAttribute("days",days);
        model.addAttribute("month",month);
        microserviceProxy.billSeen(id);
        return "bill";
    }

    @RequestMapping("/billNext/{date}")
    public String displayNextBill(HttpServletRequest request, Model model, @PathVariable java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent", userCurrent);
        log.debug("[ClientAdmin/ClientAdminController/billNext/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH,calendar.get(Calendar.MONTH)-1);
        model.addAttribute("billPaid",decimalFormat.format(microserviceProxy.findSumBillMonth(calendar.get(Calendar.MONTH))));
        model.addAttribute("billNotPaid",decimalFormat.format(microserviceProxy.findSumBillMonthNotPaid(calendar.get(Calendar.MONTH))));
        model.addAttribute("month",month);
        model.addAttribute("date",new java.sql.Date(calendar.getTime().getTime()));
        return "bills";
    }

    @RequestMapping("/billLast/{date}")
    public String displayLastBill(HttpServletRequest request, Model model, @PathVariable java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/billLast/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH,calendar.get(Calendar.MONTH)+1);
        model.addAttribute("billPaid",decimalFormat.format(microserviceProxy.findSumBillMonth(calendar.get(Calendar.MONTH))));
        model.addAttribute("billNotPaid",decimalFormat.format(microserviceProxy.findSumBillMonthNotPaid(calendar.get(Calendar.MONTH))));
        model.addAttribute("month",month);
        model.addAttribute("date",new java.sql.Date(calendar.getTime().getTime()));
        return "bills";
    }

    @RequestMapping("/bills")
    public String displayBills(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/bills] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar calendar = Calendar.getInstance();
        model.addAttribute("billPaid",decimalFormat.format(microserviceProxy.findSumBillMonth(calendar.get(Calendar.MONTH))));
        model.addAttribute("billNotPaid",decimalFormat.format(microserviceProxy.findSumBillMonthNotPaid(calendar.get(Calendar.MONTH))));
        model.addAttribute("month",month);
        model.addAttribute("date",new java.sql.Date(calendar.getTime().getTime()));
        return "bills";
    }

    @RequestMapping("/billPaid")
    public String displayBillPaid(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/billPaid] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("bills",microserviceProxy.findByPaid());
        model.addAttribute("month",month);
        return "billPaid";
    }

    @RequestMapping("/billByUser/{id}")
    public String displayBillByUser(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/billByUser/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("bills",microserviceProxy.findBillByUser(id));
        model.addAttribute("month",month);
        return "billByUser";
    }

    @RequestMapping("/billNotPaid")
    public String displayBillNotPaid(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/billNotPaid] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("bills",microserviceProxy.findNotPaid());
        model.addAttribute("month",month);
        return "billNotPaid";
    }

    @RequestMapping("/commentAccepted/{id}")
    public String commentAccepted(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/commentAccepted/{id}] with id = "+id);
        CommentBean comment = microserviceProxy.findCommentById(id);
        microserviceProxy. updateComment(comment);
        return "redirect:/testimonials";
    }

    @RequestMapping("/commentPutOnHold/{id}")
    public String commentPutOnHold(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/commentPutOnHold/{id}] with id = "+id);
        CommentBean comment = microserviceProxy.findCommentById(id);
        microserviceProxy.putOnHoldComment(comment);
        return "redirect:/testimonials";
    }
    @RequestMapping("/commentRefused/{id}")
    public String commentDeleted(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/commentRefused/{id}] with id = "+id);
        microserviceProxy.deleteComment(id);
        return "redirect:/testimonials";
    }

    @RequestMapping("ContactsSeen")
    public String displayContactsSeen(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/ContactsSeen] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("contacts",microserviceProxy.findContactBySeen());
        return "contactsSeen";
    }
    @RequestMapping("contacts")
    public String displayContacts(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/contacts] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("contacts",microserviceProxy.findContactByNotSeen());
        return "contacts";
    }

    @RequestMapping("alert")
    public String displayAlert(HttpServletRequest request, Model model) {
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/alert] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if (userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("month",month);
        return "alert";
    }
    @RequestMapping("/contact/{id}")
    public String displayContactById(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/contact/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("contact",microserviceProxy.findContactById(id));
        microserviceProxy.contactSeen(id);
        return "contact";
    }

    @RequestMapping("/deleteContact/{id}")
    public String deleteContactById(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/deleteContact/{id}] with id = "+id);
        microserviceProxy.deleteContact(id);
        return "redirect:/contacts";
    }

    @RequestMapping("/isSeen/{id}")
    public String contactIsSeen(@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/isSeen/{id}] with id = "+id);
        microserviceProxy.contactIsSeen(id);
        return "redirect:/contacts";
    }

    @RequestMapping("/customers")
    public String displayCustomers(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/customers] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("customers",microserviceProxy.findAllCustomers());
        return "customers";
    }

    @RequestMapping("/customer/{id}")
    public String displayCustomer(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/customer/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("customer",microserviceProxy.findAddressByUserId(id));
        return "customer";
    }

    @RequestMapping("/day")
    public String displayDay(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/day] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        java.sql.Date date = new java.sql.Date(new Date().getTime());
        List<TaskBean> taskList = microserviceProxy.findTaskByDate(date);
        model.addAttribute("taskList",taskList);
        model.addAttribute("month",month);
        model.addAttribute("days",days);
        model.addAttribute("dayCurrent",date);
        return "day";
    }
    @RequestMapping("/dayNext/{date}")
    public String displayNextDayDate(HttpServletRequest request, Model model, @PathVariable java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/dayNext/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,-1);
        java.sql.Date nextDate = new java.sql.Date(cal.getTimeInMillis());
        List<TaskBean> taskList = microserviceProxy.findTaskByDate(nextDate);
        model.addAttribute("taskList",taskList);
        model.addAttribute("month",month);
        model.addAttribute("days",days);
        model.addAttribute("dayCurrent",nextDate);
        return "day";
    }

    @RequestMapping("/dayLast/{date}")
    public String displayLastDayDate(HttpServletRequest request, Model model, @PathVariable java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/dayLast/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,1);
        java.sql.Date lastDate = new java.sql.Date(cal.getTimeInMillis());
        List<TaskBean> taskList = microserviceProxy.findTaskByDate(lastDate);
        model.addAttribute("taskList",taskList);
        model.addAttribute("month",month);
        model.addAttribute("days",days);
        model.addAttribute("dayCurrent",lastDate);
        return "day";
    }

    @RequestMapping("/editUser/{id}")
    public ModelAndView editUserInformation(HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/editUser/{id}] with userCurrent = "+userCurrent);
        ModelAndView modelAndView = new ModelAndView("logIn");
        modelAndView.addObject("billsAlert",microserviceProxy.alertBill());
        modelAndView.addObject("messagesAlert",microserviceProxy.alertMessage(userCurrent.getId()));
        modelAndView.addObject("commentsAlert",microserviceProxy.alertComment(userCurrent.getId()));
        modelAndView.addObject("contactsAlert",microserviceProxy.alertContact());
        modelAndView.addObject("tasksAlert",microserviceProxy.alertTask(userCurrent.getId()));
        modelAndView.addObject("month",month);
        if(userCurrent.getRole() != Role.LAWYER){
            modelAndView.addObject("userCurrent",userCurrent);
            return modelAndView;
        }

        modelAndView = new ModelAndView("editUser");
        modelAndView.addObject("billsAlert",microserviceProxy.alertBill());
        modelAndView.addObject("messagesAlert",microserviceProxy.alertMessage(userCurrent.getId()));
        modelAndView.addObject("commentsAlert",microserviceProxy.alertComment(userCurrent.getId()));
        modelAndView.addObject("contactsAlert",microserviceProxy.alertContact());
        modelAndView.addObject("tasksAlert",microserviceProxy.alertTask(userCurrent.getId()));
        modelAndView.addObject("month",month);
        modelAndView.addObject("userCurrent",userCurrent);
        modelAndView.addObject("registerFormCheck",registerFormCheck);
        RegisterForm registerForm = new RegisterForm();
        registerForm.setUser(userCurrent);
        registerForm.setAddress(microserviceProxy.findAddressByUserId(userCurrent.getId()));
        modelAndView.addObject("registerForm",registerForm);
        return modelAndView;
    }

    @RequestMapping(value = "/editUserCheck" ,method = RequestMethod.POST)
    public String checkUserAndAddressEdit(@ModelAttribute("registerForm") RegisterForm registerForm){
        log.debug("[ClientAdmin/ClientAdminController/editUserCheck] with registerForm = "+registerForm);
        registerFormCheck.evaluateEdit(registerForm);
        if (registerFormCheck.validate()){
            UserBean userCurrent = registerForm.getUser();
            AddressBean address = registerForm.getAddress();
            address.setUser(userCurrent);
            microserviceProxy.addUser(userCurrent);
            microserviceProxy.addAddress(address);
            return "redirect:/profil";
        }
        return "redirect:/editUser";
    }

    @RequestMapping("/logIn")
    public String login(Model model,HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/logIn] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        return "redirect:/index";
    }

    @RequestMapping(value = "/logInCheck", method = RequestMethod.POST)
    public String logInCheckPage(HttpServletRequest request, Model model, @ModelAttribute("user") UserBean user){
        HttpSession session = request.getSession();
        log.debug("[ClientAdmin/ClientAdminController/logInCheck]");
        UserBean userCurrent = (UserBean) session.getAttribute("userCurrent");
        user.setPassword(encryptPassword.encrypt(user));
        if (userCurrent == null) {
            if(user.getUsername() != "") {
                UserBean userSearch = microserviceProxy.findByUsername(user.getUsername());
                if (userSearch != null) {
                    if (user.getPassword().equals(userSearch.getPassword()) && userSearch.getRole() == Role.LAWYER) {
                        model.addAttribute("userCurrent", userSearch);
                        session.setAttribute("userCurrent", userSearch);
                        return "redirect:/index";
                    }
                }
            }
            model.addAttribute("userCurrent", new UserBean());
            model.addAttribute("message","Accès non autorisé : Problème d'authentification.");
            return "logIn";
        }
        return "redirect:/";
    }


    @RequestMapping("/logOut")
    public String logoutAndViewHomepage( HttpServletRequest request) {
        log.debug("[ClientAdmin/ClientAdminController/logOut]");
        HttpSession session = request.getSession();
        session.removeAttribute("userCurrent");
        return "redirect:/logIn";
    }

    @RequestMapping("/messages")
    public String messages(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/messages] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        return "messages";
    }

    @RequestMapping("/message/{id}")
    public String message(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/message/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        List<MessageBean> messages = microserviceProxy.findMessageByCustomer(id,userCurrent.getId());
        model.addAttribute("messages",messages);
        model.addAttribute("newMessage",new MessageBean());
        model.addAttribute("month",month);
        model.addAttribute("user",microserviceProxy.findUserById(id));
        for (int i = 0; i < messages.size() ; i++){
            microserviceProxy.messageSeen(messages.get(i).getId());
        }
        return "message";
    }

    @RequestMapping(value = "/messageCheck/{id}", method = RequestMethod.POST)
    public String messageCheck(@ModelAttribute("newMessage") MessageBean message,HttpServletRequest request,@PathVariable Long id){
        log.debug("[ClientAdmin/ClientAdminController/messageCheck/{id}] with id = "+id+" and message = "+message);
        if(message.getDescription().length()>=3 && message.getDescription().length()<=1000){
            UserBean userCurrent = getUserSession(request);
            message.setCustomer(microserviceProxy.findUserById(id));
            message.setLawyer(userCurrent);
            message.setSeen(true);
            message.setType(false);
            message.setDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            message.setId(null);
            microserviceProxy.addMessage(message);
        }
        return "redirect:/message/"+id;
    }

    @RequestMapping("/profil")
    public String profil(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/profil] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        alert(model,userCurrent);
        model.addAttribute("address",microserviceProxy.findAddressByUserId(userCurrent.getId()));
        return "profil";
    }

    @RequestMapping("/rdv")
    public String rdv(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdv] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        java.sql.Date today = new java.sql.Date(new Date().getTime());
        ArrayList hoursAvailable = microserviceProxy.getWeek(today);
        List<java.sql.Date> dates = microserviceProxy.getDates(today);
        int numDay = microserviceProxy.numDate(today);
        model.addAttribute("tasks",null);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",today);
        model.addAttribute("stringDate",microserviceProxy.getString(today));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvDay/{dateSelect}")
    public String rdvDaySelect(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateSelect){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvDay/{dateSelect}] with userCurrent = "+userCurrent+" and dateSelect = "+dateSelect);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        java.sql.Date today = new java.sql.Date(new Date().getTime());
        ArrayList hoursAvailable = microserviceProxy.getWeek(today);


        List<java.sql.Date> dates = microserviceProxy.getDates(today);
        int numDay = microserviceProxy.numDate(today);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        List<Integer> hoursAvailableByDate = microserviceProxy.findByDate(dateSelect);
        model.addAttribute("hoursAvailableByDate",hoursAvailableByDate);
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("tasks",microserviceProxy.findTaskByDate(dateSelect));
        model.addAttribute("dateSelect",dateSelect);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",today);
        model.addAttribute("stringDate",microserviceProxy.getString(today));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvDay/null")
    public String rdvDaySelect(){
        log.debug("[ClientAdmin/ClientAdminController/rdvDay/null]");
        return "redirect:/rdv";
    }

    @RequestMapping("/rdvNextAvailable/{dateSelect}")
    public String rdvNextAvailable(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateSelect){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvNextAvailable/{dateSelect}] with userCurrent = "+userCurrent+" and dateSelect = "+dateSelect);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("tasks",microserviceProxy.findTaskByDate(dateSelect));
        model.addAttribute("dateSelect",dateSelect);
        java.sql.Date NextDate = microserviceProxy.getLastAvailable();
        ArrayList hoursAvailable = microserviceProxy.getWeek(NextDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(NextDate);
        List<Integer> hoursAvailableByDate = microserviceProxy.findByDate(dateSelect);
        model.addAttribute("hoursAvailableByDate",hoursAvailableByDate);
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        int numDay = microserviceProxy.numDate(NextDate);
        model.addAttribute("dayCurrent",NextDate);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("today",NextDate);
        model.addAttribute("stringDate",microserviceProxy.getString(NextDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvNextAvailable/null")
    public String rdvNextAvailable(Model model, HttpServletRequest request){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvNextAvailable/null] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("tasks",null);
        model.addAttribute("dateSelect",null);
        java.sql.Date NextDate = microserviceProxy.getLastAvailable();
        ArrayList hoursAvailable = microserviceProxy.getWeek(NextDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(NextDate);
        int numDay = microserviceProxy.numDate(NextDate);
        model.addAttribute("dayCurrent",NextDate);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("today",NextDate);
        model.addAttribute("stringDate",microserviceProxy.getString(NextDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvDate/{dateWeek}/{dateSelect}")
    public String rdvSelect(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateWeek, @PathVariable java.sql.Date dateSelect){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvDate/{dateWeek}/{dateSelect}] with userCurrent = "+userCurrent+" and dateWeek = "+dateWeek+" and dateSelect = "+dateSelect);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        List<TaskBean> tasks = microserviceProxy.findTaskByDate(dateSelect);
        model.addAttribute("tasks",tasks);
        model.addAttribute("dateSelect",dateSelect);
        ArrayList hoursAvailable = microserviceProxy.getWeek(dateWeek);
        List<java.sql.Date> dates = microserviceProxy.getDates(dateWeek);
        List<Integer> hoursAvailableByDate = microserviceProxy.findByDate(dateSelect);
        model.addAttribute("hoursAvailableByDate",hoursAvailableByDate);
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        int numDay = microserviceProxy.numDate(dateWeek);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",dateWeek);
        model.addAttribute("stringDate",microserviceProxy.getString(dateWeek));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvNext/{dateWeek}/{dateSelect}")
    public String rdvNextSelect(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateWeek, @PathVariable java.sql.Date dateSelect){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvNext/{dateWeek}/{dateSelect}] with userCurrent = "+userCurrent+" and dateWeek = "+dateWeek+" and dateSelect = "+dateSelect);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateWeek);
        cal.add(Calendar.DATE,microserviceProxy.getSizeDate());
        java.sql.Date newDate = new java.sql.Date(cal.getTimeInMillis());
        alert(model,userCurrent);
        List<TaskBean> tasks = microserviceProxy.findTaskByDate(dateSelect);
        model.addAttribute("tasks",tasks);
        model.addAttribute("dateSelect",dateSelect);
        ArrayList hoursAvailable = microserviceProxy.getWeek(newDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(newDate);
        int numDay = microserviceProxy.numDate(newDate);
        List<Integer> hoursAvailableByDate = microserviceProxy.findByDate(dateSelect);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        model.addAttribute("hoursAvailableByDate",hoursAvailableByDate);
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",newDate);
        model.addAttribute("stringDate",microserviceProxy.getString(newDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }
    @RequestMapping("/rdvLast/{dateWeek}/{dateSelect}")
    public String rdvLastSelect(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateWeek, @PathVariable java.sql.Date dateSelect){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvLast/{dateWeek}/{dateSelect}] with userCurrent = "+userCurrent+" and dateWeek = "+dateWeek+" and dateSelect = "+dateSelect);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateWeek);
        cal.add(Calendar.DATE,-microserviceProxy.getSizeDate());
        java.sql.Date newDate = new java.sql.Date(cal.getTimeInMillis());
        alert(model,userCurrent);
        List<TaskBean> tasks = microserviceProxy.findTaskByDate(dateSelect);
        model.addAttribute("tasks",tasks);
        model.addAttribute("dateSelect",dateSelect);
        ArrayList hoursAvailable = microserviceProxy.getWeek(newDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(newDate);
        int numDay = microserviceProxy.numDate(newDate);
        List<Integer> hoursAvailableByDate = microserviceProxy.findByDate(dateSelect);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        model.addAttribute("hoursAvailableByDate",hoursAvailableByDate);
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",newDate);
        model.addAttribute("stringDate",microserviceProxy.getString(newDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/rdvNext/{dateWeek}/null")
    public String rdvNextNull(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateWeek){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvNext/{dateWeek}/null] with userCurrent = "+userCurrent+" and dateWeek = "+dateWeek);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateWeek);
        cal.add(Calendar.DATE,microserviceProxy.getSizeDate());
        java.sql.Date newDate = new java.sql.Date(cal.getTimeInMillis());
        alert(model,userCurrent);
        ArrayList hoursAvailable = microserviceProxy.getWeek(newDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(newDate);
        int numDay = microserviceProxy.numDate(newDate);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",newDate);
        model.addAttribute("stringDate",microserviceProxy.getString(newDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }
    @RequestMapping("/rdvLast/{dateWeek}/null")
    public String rdvLastNull(Model model, HttpServletRequest request,@PathVariable java.sql.Date dateWeek){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/rdvLast/{dateWeek}/null] with userCurrent = "+userCurrent+" and dateWeek = "+dateWeek);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateWeek);
        cal.add(Calendar.DATE,-microserviceProxy.getSizeDate());
        java.sql.Date newDate = new java.sql.Date(cal.getTimeInMillis());
        alert(model,userCurrent);
        ArrayList hoursAvailable = microserviceProxy.getWeek(newDate);
        List<java.sql.Date> dates = microserviceProxy.getDates(newDate);
        int numDay = microserviceProxy.numDate(newDate);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("days",days);
        model.addAttribute("numDay",numDay);
        model.addAttribute("dates",dates);
        model.addAttribute("dayCurrent",newDate);
        model.addAttribute("stringDate",microserviceProxy.getString(newDate));
        model.addAttribute("hoursAvailable",hoursAvailable);
        return "rdv";
    }

    @RequestMapping("/taskCheck/{dateWeek}/{dateSelect}")
    public String rdvCheck(@ModelAttribute("taskForm") TaskForm taskForm,Model model, HttpServletRequest request,@PathVariable java.sql.Date dateWeek, @PathVariable java.sql.Date dateSelect){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/taskCheck/{dateWeek}/{dateSelect}] with userCurrent = "+userCurrent+" and dateWeek = "+dateWeek+" and dateSelect = "+dateSelect+" and taskForm = "+taskForm);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        taskForm.task.setLawyer(userCurrent);
        taskForm.task.setCustomer(microserviceProxy.findUserById(taskForm.idCustomer));
        if (taskForm.hourForced < 24 && taskForm.hourForced >= 0)
            taskForm.task.setHour(taskForm.hourForced);
        taskFormCheck.evaluate(taskForm,userCurrent);
        if(taskFormCheck.validate()){
            List<TaskBean> taskProblems = microserviceProxy.findTaskInTask(dateSelect,taskForm.task.getHour(),taskForm.task.getDuration());
            if(taskProblems.size() == 0 ){
                microserviceProxy.updateTask(taskForm.task);
                taskFormCheck.init();
                alert(model,userCurrent);
                List<TaskBean> tasks = microserviceProxy.findTaskByDate(dateSelect);
                model.addAttribute("tasks",tasks);
                model.addAttribute("dateSelect",dateSelect);
                model.addAttribute("customers",microserviceProxy.findCustomers());
                model.addAttribute("hoursAvailableByDate",microserviceProxy.findByDate(dateSelect));
                model.addAttribute("taskForm",taskForm);
                model.addAttribute("taskFormCheck",taskFormCheck);
                model.addAttribute("days",days);
                model.addAttribute("numDay", microserviceProxy.numDate(dateWeek));
                model.addAttribute("dates",microserviceProxy.getDates(dateWeek));
                model.addAttribute("dayCurrent",dateWeek);
                model.addAttribute("stringDate",microserviceProxy.getString(dateWeek));
                model.addAttribute("hoursAvailable",microserviceProxy.getWeek(dateWeek));
                return "rdv";
            }
            else{
                alert(model,userCurrent);
                model.addAttribute("month",month);
                model.addAttribute("days",days);
                model.addAttribute("dateSelect",dateSelect);
                model.addAttribute("task",taskForm.getTask());
                model.addAttribute("taskList",taskProblems);
                model.addAttribute("dayCurrent",dateWeek);
                return "taskProblem";
            }
        }
        return "redirect:/rdv";
    }

    @RequestMapping("/addTask/{dateWeek}/{dateSelect}")
    public String addTask(@ModelAttribute("task") TaskBean task,@PathVariable java.sql.Date dateWeek, @PathVariable java.sql.Date dateSelect,Model model, HttpServletRequest request){
        log.debug("[ClientAdmin/ClientAdminController/addTask/{dateWeek}/{dateSelect}] with dateWeek = "+dateWeek+" and dateSelect = "+dateSelect);
        task.setLawyer(microserviceProxy.findUserById(task.getLawyer().getId()));
        task.setCustomer(microserviceProxy.findUserById(task.getCustomer().getId()));
        List<TaskBean> taskProblems = microserviceProxy.findTaskInTask(dateSelect,task.getHour(),task.getDuration());
        for (TaskBean taskDelete: taskProblems
             ) {
            microserviceProxy.deleteTask(taskDelete.getId());
        }
        microserviceProxy.updateTask(task);
        taskFormCheck.init();
        UserBean userCurrent = getUserSession(request);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        List<TaskBean> tasks = microserviceProxy.findTaskByDate(dateSelect);
        model.addAttribute("tasks",tasks);
        model.addAttribute("dateSelect",dateSelect);
        model.addAttribute("customers",microserviceProxy.findCustomers());
        model.addAttribute("hoursAvailableByDate",microserviceProxy.findByDate(dateSelect));
        model.addAttribute("taskForm",new TaskForm());
        model.addAttribute("taskFormCheck",taskFormCheck);
        model.addAttribute("days",days);
        model.addAttribute("numDay", microserviceProxy.numDate(dateWeek));
        model.addAttribute("dates",microserviceProxy.getDates(dateWeek));
        model.addAttribute("dayCurrent",dateWeek);
        model.addAttribute("stringDate",microserviceProxy.getString(dateWeek));
        model.addAttribute("hoursAvailable",microserviceProxy.getWeek(dateWeek));
        return "rdv";
    }

    @RequestMapping("/accepted/{idTask}")
    public String taskAccepted(@PathVariable Long idTask){
        log.debug("[ClientAdmin/ClientAdminController/accepted/{idTask}] with idTask = "+idTask);
        TaskBean task = microserviceProxy.findTaskById(idTask);
        String text;
        text="Le rendez-vous du " +
                task.getDate() +" de "+ task.getHour()+"h00 à "+(task.getHour()+task.getDuration())+"h00"+
                " a bien été pris en compte et validé, Pensez à ramener vos document! \n" +
                "Cordialement," + task.getLawyer().getFirstname()+" "+task.getLawyer().getName()
                ;
        String customer = microserviceProxy.findAddressByUserId(task.getCustomer().getId()).getMail();
        String lawyer = microserviceProxy.findAddressByUserId(task.getLawyer().getId()).getMail();
        microserviceProxy.sendMessage("Rendez-vous du "+task.getDate(),text,customer,lawyer);
        microserviceProxy.taskAccepted(idTask);
        return "redirect:/sevenDays";
    }

    @RequestMapping("/refused/{idTask}")
    public String taskRefused(@PathVariable Long idTask){
        log.debug("[ClientAdmin/ClientAdminController/refused/{idTask}] with idTask = "+idTask);
        microserviceProxy.deleteTask(idTask);
        return "redirect:/sevenDays";
    }

    @RequestMapping("/sevenDays")
    public String display7Days(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/sevenDays] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        java.sql.Date date = new java.sql.Date(new Date().getTime());
        ArrayList weekDate = microserviceProxy.getWeekDate(date);
        model.addAttribute("week",weekDate);
        Reason reason = Reason.DIVORCE;
        model.addAttribute("reason", reason);
        model.addAttribute("dayCurrent",date);
        model.addAttribute("days",days);
        model.addAttribute("month",month);
        return "sevenDays";
    }

    @RequestMapping("/task/{id}")
    public String displayTask(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/task/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        TaskBean task = microserviceProxy.findTaskById(id);
        model.addAttribute("task",task);
        model.addAttribute("days",days);
        model.addAttribute("month",month);
        if(task.getCustomer() != null)
            model.addAttribute("address",microserviceProxy.findAddressByUserId(task.getCustomer().getId()));
        return "task";
    }

    @RequestMapping("/taskByUser/{id}")
    public String displayTaskByUser(HttpServletRequest request, Model model,@PathVariable Long id){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/taskByUser/{id}] with userCurrent = "+userCurrent+" and id = "+id);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("taskList",microserviceProxy.getTaskByUser(id));
        model.addAttribute("days",days);
        model.addAttribute("month",month);
        model.addAttribute("customer",microserviceProxy.findUserById(id));
        return "taskByUser";
    }

    @RequestMapping("/testimonials")
    public String testimonials(HttpServletRequest request, Model model){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/testimonials] with userCurrent = "+userCurrent);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        model.addAttribute("commentsNotCheck",microserviceProxy.findCommentNotCheck(userCurrent.getId()));
        model.addAttribute("commentsCheck",microserviceProxy.findCommentCheck(userCurrent.getId()));
        model.addAttribute("month",month);

        return "testimonials";
    }


    @RequestMapping("/weekNext/{date}")
    public String displayNext7Days(HttpServletRequest request, Model model, @PathVariable java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/weekNext/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,-7);
        java.sql.Date nextDate = new java.sql.Date(cal.getTimeInMillis());
        ArrayList weekDate = microserviceProxy.getWeekDate(nextDate);
        model.addAttribute("week",weekDate);
        Reason reason = Reason.DIVORCE;
        model.addAttribute("reason", reason);
        model.addAttribute("date",date);
        model.addAttribute("dayCurrent",nextDate);
        model.addAttribute("days",days);
        model.addAttribute("month",month);
        return "sevenDays";
    }

    @RequestMapping("/weekLast/{date}")
    public String displayLast7Days(HttpServletRequest request, Model model,@PathVariable java.sql.Date date){
        UserBean userCurrent = getUserSession(request);
        log.debug("[ClientAdmin/ClientAdminController/weekLast/{date}] with userCurrent = "+userCurrent+" and date = "+date);
        model.addAttribute("userCurrent", userCurrent);
        if(userCurrent.getRole() != Role.LAWYER)
            return "logIn";
        alert(model,userCurrent);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,7);
        java.sql.Date lastDate = new java.sql.Date(cal.getTimeInMillis());
        ArrayList weekDate = microserviceProxy.getWeekDate(lastDate);
        model.addAttribute("week",weekDate);
        Reason reason = Reason.DIVORCE;
        model.addAttribute("reason", reason);
        model.addAttribute("date",date);
        model.addAttribute("dayCurrent",lastDate);
        model.addAttribute("days",days);
        model.addAttribute("month",month);
        return "sevenDays";
    }

    @RequestMapping("errors")
    public String errorPage(Model model, HttpServletRequest request){
        log.debug("[ClientAdmin/ClientAdminController/errors]");
        model.addAttribute("userCurrent",getUserSession(request));
        return "error";
    }

    public UserBean getUserSession(HttpServletRequest request){
        HttpSession session = request.getSession();
        log.debug("[ClientAdmin/ClientAdminController/getUserSession()]");
        UserBean user = (UserBean) session.getAttribute("userCurrent");
        if(user == null)
            user = new UserBean();
        return user;
    }

    public void alert(Model model,UserBean userCurrent){
        log.debug("[ClientAdmin/ClientAdminController/alert(UserBean)] with userCurrent = "+userCurrent);
        model.addAttribute("billsAlert",microserviceProxy.alertBill());
        model.addAttribute("messagesAlert",microserviceProxy.alertMessage(userCurrent.getId()));
        model.addAttribute("commentsAlert",microserviceProxy.alertComment(userCurrent.getId()));
        model.addAttribute("contactsAlert",microserviceProxy.alertContact());
        model.addAttribute("tasksAlert",microserviceProxy.alertTask(userCurrent.getId()));
        model.addAttribute("month",month);
    }


}

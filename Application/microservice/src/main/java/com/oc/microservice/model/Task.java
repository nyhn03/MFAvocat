package com.oc.microservice.model;

import com.oc.microservice.enums.Reason;
import com.oc.microservice.enums.TaskType;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Date;

/**
 * Task characterized by
 * id
 * type = enum of meeting / holiday/ other / audience
 * date of task
 * hour of task
 * duration
 * reason = enum of ( see Reason.java)
 * description describe the reason
 * accepted if the lawyer authorize the meeting
 * lawyer
 * customer
 * Visio true or false for in cabinet
 * link if visio for access to meeting inline
 */
@Slf4j
@Entity
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    @Column(length = 9)
    private TaskType type;
    private Date date;
    private int hour;
    private int duration;
    @Enumerated(EnumType.STRING)
    @Column(length = 22)
    private Reason reason;
    @Length(max = 500,message = "La description doit être inférieur à 500 caractÃ¨res.")
    private String description;
    private boolean accepted;
    @ManyToOne
    @JoinColumn(name = "id_lawyer")
    private User lawyer;
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private User customer;
    private boolean visio;
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public User getLawyer() {
        return lawyer;
    }

    public void setLawyer(User lawyer) {
        this.lawyer = lawyer;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public boolean isVisio() {
        return visio;
    }

    public void setVisio(boolean visio) {
        this.visio = visio;
    }

    public Task() {
        log.debug("[Microservice/Task()] instantiation of a new task");
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", type=" + type +
                ", date=" + date +
                ", hour=" + hour +
                ", duration=" + duration +
                ", reason=" + reason +
                ", description='" + description + '\'' +
                ", accepted=" + accepted +
                ", lawyer=" + lawyer +
                ", customer=" + customer +
                ", visio=" + visio +
                ", link=" + link +
                '}';
    }
}

package com.oc.microservice.integration.service;

import com.oc.microservice.dao.TaskDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.enums.Reason;
import com.oc.microservice.enums.TaskType;
import com.oc.microservice.model.Task;
import com.oc.microservice.service.TaskService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.relational.core.sql.In;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class TaskServiceTestIT {
    @Autowired
    TaskService taskService;

    @Test
    public void getWeekTest_date_ArrayList(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);

        Date date = new Date(cal.getTime().getTime());
        ArrayList arrayList = taskService.getWeek(date);
        Assert.assertEquals(7,arrayList.size());
        Assert.assertTrue(arrayList.get(0) != null);
    }

    @Test
    public void getHoursAvailableForWeek_date_ListInteger(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 4);
        cal.set(Calendar.DAY_OF_MONTH, 14);
        Date date = new Date(cal.getTime().getTime());

        List<Integer> result = taskService.getHoursAvailableForWeek(date);

        List<List<Integer>> list = new ArrayList<>();
        List<Integer> row1 = new ArrayList<>();
        row1.add(10);
        list.add(row1);
        row1.remove(0);
        row1.add(8);
        row1.add(9);
        row1.add(10);
        row1.add(11);
        row1.add(12);
        row1.add(13);
        row1.add(14);
        row1.add(15);
        row1.add(16);
        row1.add(17);
        list.add(row1);

        Assert.assertTrue(result.contains(list.get(0)));
        Assert.assertTrue(result.contains(list.get(1)));
        Assert.assertEquals(7,result.size());
    }
}

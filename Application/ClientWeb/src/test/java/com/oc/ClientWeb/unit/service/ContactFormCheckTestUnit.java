package com.oc.ClientWeb.unit.service;

import com.oc.ClientWeb.beans.ContactBean;
import com.oc.ClientWeb.service.ContactFormCheck;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class ContactFormCheckTestUnit {

    @InjectMocks
    ContactFormCheck contactFormCheck;

    @Test
    public void validateSettersAndGetters() {
        final PojoClass userPojo = PojoClassFactory.getPojoClass(ContactFormCheck.class);

        final Validator validator = ValidatorBuilder.create()
                .with(new SetterTester(), new GetterTester())
                .build();
        validator.validate(userPojo);
    }

    public ContactBean createContact(){
        ContactBean contact = new ContactBean();
        contact.setMessage("message");
        contact.setMail("mail@gmail.com");
        contact.setSubject("sujet");
        contact.setName("NomTest");
        contact.setSeen(true);
        return contact;
    }

    @Test
    public void validateTest_No_boolean(){
        Assert.assertTrue(contactFormCheck.validate());
    }

    @Test
    public void validateTestNameFalse_No_boolean(){
        ContactBean contact = createContact();
        contact.setName("na");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestNameEmpty_No_boolean(){
        ContactBean contact = createContact();
        contact.setName("");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestMailFalse_No_boolean(){
        ContactBean contact = createContact();
        contact.setName("ma");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestMailEmpty_No_boolean(){
        ContactBean contact = createContact();
        contact.setMail("");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestSubjetFalse_No_boolean(){
        ContactBean contact = createContact();
        contact.setSubject("ma");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestSubjetEmpty_No_boolean(){
        ContactBean contact = createContact();
        contact.setSubject("");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestMessageFalse_No_boolean(){
        ContactBean contact = createContact();
        contact.setMessage("ma");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }

    @Test
    public void validateTestMessageEmpty_No_boolean(){
        ContactBean contact = createContact();
        contact.setMessage("");
        contactFormCheck.evaluate(contact);
        Assert.assertTrue(!ContactFormCheck.validate());
    }
}

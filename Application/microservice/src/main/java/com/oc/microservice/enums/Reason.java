package com.oc.microservice.enums;

/**
 * Reason matches to reason of tasks
 */
public enum Reason {
    DIVORCE,
    ADVICE,
    SUCCESSION,
    CRIMINAL,
    FAMILY,
    CHILD_CARE,
    ALIMONY,
    CONSTRUCTION,
    INTELLECTUAL_PROPERTY,
    OTHER,
    NO_CHOICE
}

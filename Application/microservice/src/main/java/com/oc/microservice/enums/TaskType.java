package com.oc.microservice.enums;

/**
 * TaskType matches to type of tasks
 */
public enum TaskType {
    MEETING,
    HOLIDAY,
    AUDIENCE,
    OTHER,
    NO_CHOICE
}

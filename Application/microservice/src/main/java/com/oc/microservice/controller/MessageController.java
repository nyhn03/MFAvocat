package com.oc.microservice.controller;

import com.oc.microservice.dao.MessageDao;
import com.oc.microservice.exception.ImpossibleAddMessageException;
import com.oc.microservice.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class MessageController {
    @Autowired
    MessageDao messageDao;

    @GetMapping(value = "/messageCustomer/{idCustomer}")
    public List<Message> findMessageByCustomer(@PathVariable Long idCustomer){
        log.debug("[Microservice/MessageController/messageCustomer/{idCustomer}] with idCustomer = "+idCustomer);
        List<Message> result = messageDao.findMessageByCustomer(idCustomer);
        log.debug("[Microservice/MessageController/messageCustomer/{idCustomer}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/messageCustomerAndLawyer/{idCustomer}/{idLawyer}")
    public List<Message> findMessageByCustomer(@PathVariable Long idCustomer,@PathVariable Long idLawyer){
        log.debug("[Microservice/MessageController/messageCustomerAndLawyer/{idCustomer}/{idLawyer}] with idCustomer = "+idCustomer +" and idLawyer = " +idLawyer);
        List<Message> result = messageDao.findMessageByCustomerAndLawyer(idCustomer,idLawyer);
        log.debug("[Microservice/MessageController/messageCustomerAndLawyer/{idCustomer}/{idLawyer}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/messageLawyer/{idLawyer}")
    public List<Message> findNewMessageByLawyer(@PathVariable Long idLawyer){
        log.debug("[Microservice/MessageController/messageLawyer/{idLawyer}] with idLawyer = "+idLawyer);
        List<Message> result = messageDao.findNewMessageByLawyer(idLawyer);
        log.debug("[Microservice/MessageController/messageLawyer/{idLawyer}] Return -> \n"+result +" \n");
        return result;
    }
    @PostMapping("/message/addMessage")
    public ResponseEntity<Message> addMessage(@RequestBody Message message) throws ImpossibleAddMessageException {
        Message newMessage = messageDao.save(message);
        log.debug("[Microservice/MessageController/message/addMessage] with message = "+newMessage);
        if (newMessage == null) throw new ImpossibleAddMessageException("Impossible d'ajouter le Message");
        return new ResponseEntity<Message>(message, HttpStatus.CREATED);
    }
}

package com.oc.ClientAdmin.beans;

import lombok.extern.slf4j.Slf4j;

import java.sql.Date;

/**
 * i.e Bill of microservice
 */
@Slf4j
public class BillBean {
    private Long id;
    private boolean paid;
    private double price;
    private Date date;
    private UserBean customer;
    private UserBean lawyer;
    private String service;
    private String description;

    public BillBean() {
        log.debug("[ClientAdmin/BillBean()] instantiation of a new billBean");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserBean getCustomer() {
        return customer;
    }

    public void setCustomer(UserBean customer) {
        this.customer = customer;
    }

    public UserBean getLawyer() {
        return lawyer;
    }

    public void setLawyer(UserBean lawyer) {
        this.lawyer = lawyer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "BillBean{" +
                "id=" + id +
                ", paid=" + paid +
                ", price=" + price +
                ", date=" + date +
                ", customer=" + customer +
                ", lawyer=" + lawyer +
                ", service=" + service +
                ", description=" + description +
                '}';
    }
}

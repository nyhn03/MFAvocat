package com.oc.ClientWeb.proxies;

import com.oc.ClientWeb.beans.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


@FeignClient(name="microservice", url = "localhost:9001")
public interface MicroserviceProxy {
    @GetMapping(value = "/authentification/{username}")
    UserBean findByUsername(@PathVariable("username") String username);

    @GetMapping(value = "/authentification/lawyer")
    List<UserBean> findLawyer();

    @GetMapping("/authentification/AddressUser/{id}")
    AddressBean findAddressByUserId(@PathVariable Long id);

    @GetMapping("/authentification/Address")
    List<AddressBean> findAllAddress();

    @PostMapping("/authentification/addUser")
    ResponseEntity<UserBean> addUser(@RequestBody UserBean user);

    @PostMapping("/authentification/addAddress")
    ResponseEntity<AddressBean> addAddress(@RequestBody AddressBean address);

    @GetMapping("/authentification/mail/{mail}")
    AddressBean findByMail(@PathVariable String mail);

    @GetMapping(value = "/task/{date}")
    List<Integer> findByDate(@PathVariable Date date);

    @GetMapping(value = "/task/get/{id}")
    TaskBean findTaskById(@PathVariable Long id);

    @GetMapping(value = "/authentification/get/{id}")
    UserBean findUserById(@PathVariable Long id);

    @PostMapping("/task/addTask")
    ResponseEntity<TaskBean> addTask(@RequestBody TaskBean task);

    @GetMapping(value = "/deleteTask/{id}")
    void deleteTask(@PathVariable Long id);

    @PostMapping("/task/updateTask")
    ResponseEntity<TaskBean> updateTask(@RequestBody TaskBean task);

    @GetMapping(value = "/task/day/{date}")
    String nameDate(@PathVariable Date date);

    @GetMapping(value = "/task/NumDay/{date}")
    int numDate(@PathVariable Date date);

    @GetMapping(value = "/task/week/{date}")
    ArrayList getWeek(@PathVariable Date date);

    @GetMapping(value = "/task/getString/{date}")
    List<String> getString(@PathVariable Date date);

    @GetMapping(value = "/task/getDates/{date}")
    List<Date> getDates(@PathVariable Date date);

    @GetMapping(value = "/tast/lastAvailable")
    Date getLastAvailable();

    @GetMapping(value = "/sizeDate")
    int getSizeDate();

    @PostMapping("/contact/addContact")
    ResponseEntity<ContactBean> addContact(@RequestBody ContactBean contact);

    @GetMapping(value = "/messageCustomer/{idCustomer}")
    List<MessageBean> findMessageByCustomer(@PathVariable Long idCustomer);

    @PostMapping("/message/addMessage")
    ResponseEntity<MessageBean> addMessage(@RequestBody MessageBean message);

    @GetMapping(value = "/taskPassed/{idUser}")
    List<TaskBean> getTaskPassed(@PathVariable Long idUser);

    @GetMapping(value = "/taskNotPassed/{idUser}")
    List<TaskBean> getTaskNotPassed(@PathVariable Long idUser);

    @GetMapping(value = "/taskNotAccepted/{idUser}")
    List<TaskBean> getTaskNotAccepted(@PathVariable Long idUser);

    @GetMapping(value = "/commentCheck")
    List<CommentBean> findCommentCheck();

    @PostMapping("/comment/addComment")
    ResponseEntity<CommentBean> addComment(@RequestBody CommentBean comment);

    @GetMapping(value = "/billNotPaid/{idCustomer}")
    List<BillBean> findByCustomerAndNotPaid(@PathVariable Long idCustomer);

    @GetMapping(value = "/billPaid/{idCustomer}")
    List<BillBean> findByCustomerAndPaid(@PathVariable Long idCustomer);

    @GetMapping(value ="/bill/{id}")
    BillBean findBillById(@PathVariable Long id);

    @PostMapping("/isPaid")
    ResponseEntity<BillBean> paid(@RequestBody BillBean bill);

    @PostMapping("/sendMessage/{subject}/{text}/{from}/{to}")
    void sendMessage(@PathVariable String subject,@PathVariable String text,@PathVariable String from,@PathVariable String to);
}

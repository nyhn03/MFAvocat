package com.oc.microservice.controller;

import com.oc.microservice.dao.AddressDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.exception.ImpossibleAddAddressException;
import com.oc.microservice.exception.ImpossibleAddUserException;
import com.oc.microservice.model.Address;
import com.oc.microservice.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class AuthentificationController {
    @Autowired
    UserDao userDao;

    @Autowired
    AddressDao addressDao;

    @GetMapping("/customers")
    public List<Address> findAllCustomers(){
        log.debug("[Microservice/AuthentificationController/customer]");
        List<Address> result = addressDao.findAllAddressByClient();
        log.debug("[Microservice/AuthentificationController/customer] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/allCustomers")
    public List<User> findCustomers(){
        log.debug("[Microservice/AuthentificationController/allCustomers]");
        List<User> result = userDao.findAllClient();
        log.debug("[Microservice/AuthentificationController/allCustomers] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/authentification/get/{id}")
    public Optional<User> findUserById(@PathVariable Long id){
        log.debug("[Microservice/AuthentificationController/authentification/get/{id}] with id =" + id);
        Optional<User> result = userDao.findById(id);
        log.debug("[Microservice/AuthentificationController/authentification/get/{id}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/authentification/{username}")
    public User findByUsername(@PathVariable String username){
        log.debug("[Microservice/AuthentificationController/authentification/{username}] with username =" + username);
        User result = userDao.findByUsername(username);
        log.debug("[Microservice/AuthentificationController/authentification/{username}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/authentification/lawyer")
    public List<User> findLawyer(){
        log.debug("[Microservice/AuthentificationController/authentification/lawyer]");
        List<User> result = userDao.findLawyer();
        log.debug("[Microservice/AuthentificationController/authentification/lawyer] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/authentification/AddressUser/{id}")
    public Address findAddressByUserId(@PathVariable Long id){
        log.debug("[Microservice/AuthentificationController/authentification/AddressUser/{id}] with id = "+id);
        Address result =  addressDao.findAddressByUser(id);
        log.debug("[Microservice/AuthentificationController/authentification/AddressUser/{id}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/authentification/Address")
    public List<Address> findAllAddress(){
        log.debug("[Microservice/AuthentificationController/authentification/Address]");
        List<Address> result = addressDao.findAll();
        log.debug("[Microservice/AuthentificationController/authentification/Address] Return -> \n"+result +" \n");
        return result;
    }
    @GetMapping("/authentification/mail/{mail}")
    public Address findByMail(@PathVariable String mail){
        log.debug("[Microservice/AuthentificationController/authentification/mail/{mail}] with mail = "+mail);
        Address result = addressDao.findAddressByMail(mail);
        log.debug("[Microservice/AuthentificationController/authentification/mail/{mail}]  Return -> \n"+result +" \n");
        return result;
    }

    @PostMapping("/authentification/addUser")
    public ResponseEntity<User> addUser(@RequestBody User user) throws ImpossibleAddUserException {
        User newUser = userDao.save(user);
        log.debug("[Microservice/AuthentificationController/authentification/addUser] with User = "+newUser);
        if (newUser == null) throw new ImpossibleAddUserException("Impossible d'ajouter cette utilisateur");
        return new ResponseEntity<User>(user, HttpStatus.CREATED);
    }
    @PostMapping("/authentification/addAddress")
    public ResponseEntity<Address> addAddress(@RequestBody Address address) throws ImpossibleAddAddressException {
        Address newAddress = addressDao.save(address);
        log.debug("[Microservice/AuthentificationController/authentification/addAddress] with Address = "+newAddress);
        if (newAddress == null) throw new ImpossibleAddAddressException("Impossible d'ajouter cette adresse");
        return new ResponseEntity<Address>(address, HttpStatus.CREATED);
    }
}

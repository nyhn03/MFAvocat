package com.oc.ClientWeb.beans;


import lombok.extern.slf4j.Slf4j;

/**
 * i.e Address of microservice
 */
@Slf4j
public class AddressBean {
        private Long id;
        private int num;
        private String name;
        private String zipCode;
        private String city;
        private String phone;
        private String mail;
        private UserBean user;

    public AddressBean() {
        log.debug("[ClientWeb/AddressBean()] instantiation of a new addressBean");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "AddressBean{" +
                "id=" + id +
                ", num=" + num +
                ", name='" + name + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", phone='" + phone + '\'' +
                ", mail='" + mail + '\'' +
                ", user=" + user +
                '}';
    }
}

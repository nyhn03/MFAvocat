package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.CommentDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.model.Comment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class CommentDaoTestIT {
    @Autowired
    CommentDao commentDao;

    @Autowired
    UserDao userDao;

    @Test
    public void saveCommentTest_comment_noReturn(){
        Comment comment = new Comment();
        comment.setCustomer(userDao.getOne(1L));
        comment.setLawyer(userDao.getOne(2L));
        comment.setDescription("Enregistrement Test");
        comment.setValid(false);

        commentDao.save(comment);
        Comment commentSearch = commentDao.findCommentByNotCheck(2l).get(0);

        boolean testCheck = commentSearch.getCustomer().getId().equals(comment.getCustomer().getId())
                && commentSearch.getLawyer().getId().equals(comment.getLawyer().getId())
                && commentSearch.getDescription().equals(comment.getDescription())
                && !commentSearch.isValid();
        Assert.assertTrue(testCheck);
        commentDao.delete(comment);
    }

    @Test
    public void findCommentByCheck_No_ListComment(){
        List<Comment> comments = commentDao.findCommentByCheck();
        Assert.assertEquals(5,comments.size());
        boolean testCheck = comments.get(0).getId().equals(21l)
                && comments.get(0).isValid()
                && comments.get(0).getDescription().equals("hi")
                && comments.get(0).getLawyer().getId().equals(1l)
                && comments.get(0).getCustomer().getId().equals(4l);

        Assert.assertTrue(testCheck);
    }

    @Test
    public void findCommentByNotCheck_idLawyer_ListComment(){
        List<Comment> comments = commentDao.findCommentByNotCheck(1l);
        Assert.assertEquals(5,comments.size());
        boolean testCheck = comments.get(0).getId().equals(16l)
                && !comments.get(0).isValid()
                && comments.get(0).getDescription().equals("salutation")
                && comments.get(0).getLawyer().getId().equals(1l)
                && comments.get(0).getCustomer().getId().equals(2l);

        Assert.assertTrue(testCheck);
    }

    @Test
    public void findCommentByCheck_idLawyer_ListComment(){
        List<Comment> comments = commentDao.findCommentByCheck(1l);
        Assert.assertEquals(4,comments.size());
        boolean testCheck = comments.get(0).getId().equals(21l)
                && comments.get(0).isValid()
                && comments.get(0).getDescription().equals("hi")
                && comments.get(0).getLawyer().getId().equals(1l)
                && comments.get(0).getCustomer().getId().equals(4l);

        Assert.assertTrue(testCheck);
    }
}

package com.oc.microservice.enums;

/**
 * Role matches to role of User
 */
public enum Role {
    DEVELOPPER,
    LAWYER,
    CLIENT,
    NOT_CONNECTED
}

package com.oc.ClientWeb.model;

import com.oc.ClientWeb.beans.AddressBean;
import com.oc.ClientWeb.beans.UserBean;
import lombok.extern.slf4j.Slf4j;

/**
 * RegisterForm is a form to register a account
 * user and address
 */
@Slf4j
public class RegisterForm {
    private UserBean user;
    private AddressBean address;

    public RegisterForm() {
        log.debug("[ClientWeb/RegisterForm()] instantiation of a new registerForm");
        user = new UserBean();
        address = new AddressBean();
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public AddressBean getAddress() {
        return address;
    }

    public void setAddress(AddressBean address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "RegisterForm{" +
                "user=" + user +
                ", address=" + address +
                '}';
    }
}

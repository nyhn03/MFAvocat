package com.oc.microservice.unit.model;

import com.oc.microservice.model.Task;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;

public class TaskTestUnit {
    @Test
    public void validateSettersAndGetters() {
        final PojoClass userPojo = PojoClassFactory.getPojoClass(Task.class);

        final Validator validator = ValidatorBuilder.create()
                .with(new SetterTester(), new GetterTester())
                .build();
        validator.validate(userPojo);
    }
}

package com.oc.microservice.dao;

import com.oc.microservice.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentDao extends JpaRepository<Comment,Long> {
    /**
     * Give all comment check
     * @return list of comment
     */
    @Query("SELECT comment FROM Comment comment WHERE comment.valid = true")
    List<Comment> findCommentByCheck();

    /**
     * Give all comment not check for a lawyer
     * @param idLawyer id of lawyer
     * @return list of comment
     */
    @Query("SELECT comment FROM Comment comment WHERE comment.valid = false AND comment.lawyer.id = :idLawyer")
    List<Comment> findCommentByNotCheck(@Param("idLawyer") Long idLawyer);

    /**
     * Give all comment check for a lawyer
     * @param idLawyer if of lawyer
     * @return list of comment
     */
    @Query("SELECT comment FROM Comment comment WHERE comment.valid = true AND comment.lawyer.id = :idLawyer")
    List<Comment> findCommentByCheck(@Param("idLawyer") Long idLawyer);
}
package com.oc.microservice.dao;

import com.oc.microservice.model.Address;
import com.oc.microservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressDao extends JpaRepository<Address,Long> {
    /**
     * Give address of user
     * @param id of user
     * @return address data
     */
    @Query("SELECT address FROM Address address WHERE address.user.id = :id")
    Address findAddressByUser(@Param("id") Long id);

    /**
     * Give address of user
     * @param mail of user
     * @return address data
     */
    @Query("SELECT address FROM Address address WHERE address.mail = :mail")
    Address findAddressByMail(@Param("mail") String mail);

    /**
     * Give all address of client
     * @return address data of clients
     */
    @Query("SELECT address FROM Address address WHERE address.user.role = 'CLIENT'")
    List<Address> findAllAddressByClient();
}

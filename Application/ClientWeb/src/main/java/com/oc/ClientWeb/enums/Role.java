package com.oc.ClientWeb.enums;

/**
 * Role matches to role of User
 */
public enum Role {
    DEVELOPPER,
    LAWYER,
    CLIENT,
    NOT_CONNECTED
}

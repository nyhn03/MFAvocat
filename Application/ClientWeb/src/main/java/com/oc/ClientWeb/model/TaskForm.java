package com.oc.ClientWeb.model;

import com.oc.ClientWeb.beans.TaskBean;
import lombok.extern.slf4j.Slf4j;

/**
 * TaskForm is a form to create task
 * task for task wanted
 * idLawyer for add lawyer after
 */
@Slf4j
public class TaskForm {
    private TaskBean task;
    private Long idLawyer;

    public TaskForm() {
        log.debug("[ClientWeb/TaskForm()] instantiation of a new taskForm");
        task = new TaskBean();
    }

    public TaskBean getTask() {
        return task;
    }

    public void setTask(TaskBean task) {
        this.task = task;
    }

    public Long getIdLawyer() {
        return idLawyer;
    }

    public void setIdLawyer(Long idLawyer) {
        this.idLawyer = idLawyer;
    }
}

package com.oc.microservice.controller;

import com.oc.microservice.dao.TaskDao;
import com.oc.microservice.dao.UserDao;
import com.oc.microservice.enums.TaskType;
import com.oc.microservice.exception.ImpossibleAddTaskException;
import com.oc.microservice.model.Task;
import com.oc.microservice.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class TaskController {
    private static int MIN_HOUR = 8;
    private static int MAX_HOUR = 17;
    @Autowired
    TaskService taskService;

    @Autowired
    UserDao userDao;

    @Autowired
    TaskDao taskDao;

    @GetMapping(value = "/task/tasks")
    public List<Task> findAllTasks(){
        log.debug("[Microservice/TaskController/task/tasks]");
        List<Task> result = taskDao.findAll();
        log.debug("[Microservice/TaskController/task/tasks] Return -> \n"+result +" \n");
        return result;
    }
    @GetMapping(value = "/task/get/{id}")
    public Optional<Task> findTaskById(@PathVariable Long id){
        log.debug("[Microservice/TaskController/task/get/{id}] with id = "+ id);
        Optional<Task> result = taskDao.findById(id);
        log.debug("[Microservice/TaskController/task/get/{id}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/task/{date}")
    public List<Integer> findByDate(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/{date}] with date = "+ date);
        List<Task> tasks = taskDao.findByDate(date);
        List<Integer> hoursAvailable = taskService.hoursAvailable(tasks);
        if(hoursAvailable == null || hoursAvailable.size() == 0)
            return null;
        log.debug("[Microservice/TaskController/task/{date}] Return -> \n"+hoursAvailable +" \n");
        return hoursAvailable;
    }

    @PostMapping("/task/updateTask")
    public ResponseEntity<Task> updateTask(@RequestBody Task task) throws ImpossibleAddTaskException {
        Task newTask = taskDao.save(task);
        log.debug("[Microservice/TaskController/task/updateTask] with task = "+newTask);
        if (newTask == null) throw new ImpossibleAddTaskException("Impossible de modifier cette tâche");
        return new ResponseEntity<Task>(task, HttpStatus.CREATED);
    }

    /**
     * Check :
     *  if date is less than today then impossible
     *  if holiday then list of Task = null else impossible
     *  if hour is less than 8 || hour is greater than 18 then impossible
     *  if hour = task.hour is impossible
     *  if hour is less than task.hour is less than hour+duration
     *  if hour is greater than task.hour+duration is less than hour+duration
     *  if ok then save
     * @param task task
     * @return Responsive
     * @throws ImpossibleAddTaskException ImpossibleAddTaskException
     */
    @PostMapping("/task/addTask")
    public ResponseEntity<Task> addTask(@RequestBody Task task) throws ImpossibleAddTaskException {
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        if(task.getDate().before(date))
        {
            log.debug("[Microservice/TaskController/task/addTask] error because task.getDate().before(date) = "+task.getDate().before(date));
            return new ResponseEntity<Task>(task,HttpStatus.UNAUTHORIZED);
        }
        List<Task> tasks = taskDao.findByDate(task.getDate());
        if(task.getType() == TaskType.HOLIDAY){
            if(tasks.isEmpty()){
                Task newTask = taskDao.save(task);
                log.debug("[Microservice/TaskController/task/addTask] with task = "+newTask);
                if (newTask == null) throw new ImpossibleAddTaskException("Impossible d'ajouter cette tâche");
                return new ResponseEntity<Task>(task, HttpStatus.CREATED);
            }
        }
        if(task.getHour() < 8 && task.getType()==TaskType.MEETING){
            log.debug("[Microservice/TaskController/task/addTask] error because task.getHour() < 8 && task.getType()==TaskType.MEETING = "+(task.getHour() < 8 && task.getType()==TaskType.MEETING));
            return new ResponseEntity<Task>(task,HttpStatus.UNAUTHORIZED);
        }
        if(task.getHour()+task.getDuration() > 18 && task.getType()==TaskType.MEETING){
            log.debug("[Microservice/TaskController/task/addTask] error because task.getHour()+task.getDuration() > 18 && task.getType()==TaskType.MEETING = "+(task.getHour()+task.getDuration() > 18 && task.getType()==TaskType.MEETING));
            return new ResponseEntity<Task>(task,HttpStatus.UNAUTHORIZED);
        }
        for(int i = 0; i < tasks.size();i++){
            if(tasks.get(i).getHour() == task.getHour()){
                log.debug("[Microservice/TaskController/task/addTask] error because tasks.get(i).getHour() == task.getHour() = "+(tasks.get(i).getHour() == task.getHour()));
                return new ResponseEntity<Task>(task,HttpStatus.UNAUTHORIZED);
            }
        }
        int duration = task.getDuration();
        for(;duration>1;duration--){
            for(int i = 0; i < tasks.size();i++) {
                for (int hourWatching = 1; hourWatching < duration; hourWatching++){
                    if (tasks.get(i).getHour() == task.getHour() + hourWatching) {
                        log.debug("[Microservice/TaskController/task/addTask] error because tasks.get(i).getHour() == task.getHour() + hourWatching = "+(tasks.get(i).getHour() == task.getHour() + hourWatching));
                        return new ResponseEntity<Task>(task, HttpStatus.UNAUTHORIZED);
                    }
                }
            }
        }
        Task newTask = taskDao.save(task);
        log.debug("[Microservice/TaskController/task/addTask] with task = "+newTask);
        if (newTask == null) throw new ImpossibleAddTaskException("Impossible d'ajouter cette tâche");
        return new ResponseEntity<Task>(task, HttpStatus.CREATED);
    }

    @GetMapping(value = "/deleteTask/{id}")
    public void deleteTask(@PathVariable Long id){
        log.debug("[Microservice/TaskController/deleteTask/{id}] with id = "+ id);
        taskDao.deleteById(id);
    }

    @GetMapping(value = "/task/day/{date}")
    public String nameDate(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/day/{date}] with date = "+ date);
        String result = taskService.stringDate(date);
        log.debug("[Microservice/TaskController/task/day/{date}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/task/NumDay/{date}")
    public int numDate(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/NumDay/{date}] with date = "+ date);
        int result = taskService.sayNumDayDate(date);
        log.debug("[Microservice/TaskController/task/NumDay/{date}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/task/week/{date}")
    public ArrayList getWeek(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/week/{date}] with date = "+ date);
        ArrayList result = taskService.getHoursAvailableForWeek(date);
        log.debug("[Microservice/TaskController/task/week/{date}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/task/weekDate/{date}")
    public ArrayList getWeekDate(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/weekDate/{date}] with date = "+ date);
        ArrayList result = taskService.getWeek(date);
        log.debug("[Microservice/TaskController/task/weekDate/{date}] Return -> \n"+result +" \n");
        return result;
    }

    /**
     * Give the first date with hour available is greater than today
     * @return Date
     */
    @GetMapping(value = "/tast/lastAvailable")
    public Date getLastAvailable(){
        log.debug("[Microservice/TaskController/task/lastAvailable]");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,1);
        Date today = new Date(calendar.getTime().getTime());

        Date result = today;
        List<Task> tasks = taskDao.findAllAfterDate(today);
        for (int i = 0; i < tasks.size(); i++){
            if(findByDate(tasks.get(i).getDate()) != null){
                if(tasks.get(0).getDate().before(result)) {
                    return tasks.get(i).getDate();
                }
            }
        }
        log.debug("[Microservice/TaskController/task/lastAvailable] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/sizeDate")
    public int getSizeDate(){
        log.debug("[Microservice/TaskController/sizeDate]");
        int result = taskService.getNbDay();
        log.debug("[Microservice/TaskController/sizeDate] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/task/getString/{date}")
    public List<String> getString(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/getString/{date}] with date = "+date);
        List<String> result= taskService.getStringDate(date);
        log.debug("[Microservice/TaskController/task/getString/{date}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/task/getDates/{date}")
    public List<Date> getDates(@PathVariable Date date){
        log.debug("[Microservice/TaskController/task/getDates/{date}] with date = "+date);
        List<Date> result= taskService.getDates(date);
        log.debug("[Microservice/TaskController/task/getDates/{date}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/taskPassed/{idUser}")
    public List<Task> getTaskPassed(@PathVariable Long idUser){
        Date today = new Date(Calendar.getInstance().getTime().getTime());
        log.debug("[Microservice/TaskController/taskPassed/{idUser}] with date = "+today+" and idUser = "+idUser);
        List<Task> result= taskDao.findTaskPassedByUser(today,idUser);
        log.debug("[Microservice/TaskController/task/taskPassed/{idUser}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/taskNotPassed/{idUser}")
    public List<Task> getTaskNotPassed(@PathVariable Long idUser){
        Date today = new Date(Calendar.getInstance().getTime().getTime());
        log.debug("[Microservice/TaskController/taskNotPassed/{idUser}] with date = "+today+" and idUser = "+idUser);
        List<Task> result= taskDao.findTaskNotPassedByUser(today,idUser);
        log.debug("[Microservice/TaskController/task/taskNotPassed/{idUser}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping(value = "/taskByUser/{idUser}")
    public List<Task> getTaskByUser(@PathVariable Long idUser){
        log.debug("[Microservice/TaskController/taskByUser/{idUser}] with idUser = "+idUser);
        List<Task> result= taskDao.findTaskByUser(idUser);
        log.debug("[Microservice/TaskController/taskByUser/{idUser}] Return -> \n"+result +" \n");
        return result;
    }


    @GetMapping(value = "/taskNotAccepted/{idUser}")
    public List<Task> getTaskNotAccepted(@PathVariable Long idUser){
        Date today = new Date(Calendar.getInstance().getTime().getTime());
        log.debug("[Microservice/TaskController/taskNotAccepted/{idUser}] with date = "+today+" and idUser = "+idUser);
        List<Task> result= taskDao.findTaskNotAcceptedByUser(today,idUser);
        log.debug("[Microservice/TaskController/taskNotAccepted/{idUser}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/taskByDate/{date}")
    public List<Task> findTaskByDate(@PathVariable Date date){
        log.debug("[Microservice/TaskController/taskByDate/{date}] with date = "+date);
        List<Task> result= taskDao.findByDate(date);
        log.debug("[Microservice/TaskController/taskByDate/{date}] Return -> \n"+result +" \n");
        return result;

    }

    /**
     * find task in task
     * i.e. search if a spot overlaps the spot I want added
     * @param date date of day
     * @param hour hour wanted
     * @param duration duration wanted
     * @return List of Task
     */
    @GetMapping("/taskInTask/{date}/{hour}/{duration}")
    public List<Task> findTaskInTask(@PathVariable Date date,@PathVariable int hour,@PathVariable int duration){
        log.debug("[Microservice/TaskController/taskInTask/{date}/{hour}/{duration}] with date = "+date+" and hour = "+hour+" and duration = "+duration);
        List<Task> tasks = taskDao.findByDate(date);
        List<Task> result = new ArrayList<>();
        for (Task task:tasks) {
            if((task.getHour()+task.getDuration()>hour && task.getHour()+task.getDuration() < hour+duration ) || (task.getHour()>hour && task.getHour() < hour+duration ) || (task.getHour()<hour && task.getHour()+task.getDuration() > hour+duration )){
                result.add(task);
            }
        }
        log.debug("[Microservice/TaskController/taskInTask/{date}/{hour}/{duration}] Return -> \n"+result +" \n");
        return result;
    }

    @GetMapping("/taskAccepted/{id}")
    public void taskAccepted(@PathVariable Long id){
        Optional<Task> task = taskDao.findById(id);
        task.get().setAccepted(true);
        log.debug("[Microservice/TaskController/taskAccepted/{id}] with id = "+id+" and task = "+task.get());
        taskDao.save(task.get());
    }
}

package com.oc.microservice.integration.dao;

import com.oc.microservice.dao.AddressDao;

import com.oc.microservice.dao.UserDao;
import com.oc.microservice.model.Address;
import com.oc.microservice.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class AddressDaoTestIT {

    @Autowired
    AddressDao addressDao;
    @Autowired
    UserDao userDao;

    @Test
    public void saveAddressTest_Address_noReturn(){
        Address address = new Address();
        address.setCity("cityTest");
        User user = userDao.getOne(5L);
        address.setUser(user);
        address.setMail("mailTest");
        address.setName("nameTest");
        address.setNum(0);
        address.setPhone("0605185012");
        address.setZipCode("Test1");

        addressDao.save(address);
        Address addressSearch = addressDao.findAddressByUser(5L);

        boolean testCheck = address.getCity().equals(addressSearch.getCity())
                && address.getUser().getId().equals(addressSearch.getUser().getId())
                && address.getMail().equals(addressSearch.getMail())
                && address.getName().equals(addressSearch.getName())
                && address.getNum() == 0
                && address.getPhone().equals(addressSearch.getPhone())
                && address.getZipCode().equals(addressSearch.getZipCode());
        Assert.assertTrue(testCheck);
        addressDao.delete(addressSearch);
    }

    @Test
    public void findAddressByUser_idUser_Address(){
        Address address = addressDao.findAddressByUser(2l);
        boolean testCheck =
                        address.getCity().equals("MARSEILLE")
                        && address.getUser().getId().equals(2l)
                        && address.getMail().equals("duboisgeoffrey03@gmail.com")
                        && address.getName().equals("RUE SAINT PIERRE")
                        && address.getNum() == 592
                        && address.getPhone().equals("0605187091")
                        && address.getZipCode().equals("13010");
        Assert.assertTrue(testCheck);
    }

    @Test
    public void findAddressByMail_mailString_Address(){
        Address address = addressDao.findAddressByMail("duboisgeoffrey03@gmail.com");
        boolean testCheck =
                address.getCity().equals("MARSEILLE")
                        && address.getUser().getId().equals(2l)
                        && address.getMail().equals("duboisgeoffrey03@gmail.com")
                        && address.getName().equals("RUE SAINT PIERRE")
                        && address.getNum() == 592
                        && address.getPhone().equals("0605187091")
                        && address.getZipCode().equals("13010");
        Assert.assertTrue(testCheck);
    }
    @Test
    public void findAllAddressByClient_no_ListAddress(){
        List<Address> addressList = addressDao.findAllAddressByClient();
        boolean testCheck =
                addressList.get(0).getCity().equals("MARSEILLE")
                        && addressList.get(0).getUser().getId().equals(2l)
                        && addressList.get(0).getMail().equals("duboisgeoffrey03@gmail.com")
                        && addressList.get(0).getName().equals("RUE SAINT PIERRE")
                        && addressList.get(0).getNum() == 592
                        && addressList.get(0).getPhone().equals("0605187091")
                        && addressList.get(0).getZipCode().equals("13010");
        Assert.assertTrue(testCheck);
        Assert.assertEquals(3,addressList.size());
    }
}

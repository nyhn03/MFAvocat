-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: db_mfavocat_test
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,53,'rue Chanzy','13300','Salon de Provence','0623524897','mylenefernandez.avocat@gmail.com',1),(2,592,'RUE SAINT PIERRE','13010','MARSEILLE','0605187091','duboisgeoffrey03@gmail.com',2),(3,7,'Boulevard Polo','13013','Marseille','0667801953','camille.ponte.cp@gmail.com',3),(4,23,'Allée des fauvettes','62121','Pas de calais','0607080910','Frypon@gmail.com',4);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (15,0,10.00,'2021-05-13',2,1,'Service test','Description Test',0),(16,0,20.00,'2021-05-13',2,1,'Service test','Description Test',1),(17,0,30.00,'2021-05-13',3,1,'Service test','Description Test',0),(18,0,40.00,'2021-04-13',4,1,'Service test','Description Test',1),(19,0,50.00,'2021-04-13',2,1,'Service test','Description Test',0),(20,1,15.00,'2021-05-13',2,1,'Service test','Description Test',1),(21,1,25.00,'2021-05-13',2,1,'Service test','Description Test',0),(22,1,35.00,'2021-05-13',3,1,'Service test','Description Test',1),(23,1,45.00,'2021-04-13',4,1,'Service test','Description Test',0),(24,1,55.00,'2021-04-13',2,1,'Service test','Description Test',1);
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (16,'salutation',0,1,2),(17,'bonjour',0,1,3),(18,'salut',0,1,4),(19,'bienvenue',0,1,2),(20,'hola',0,1,3),(21,'hi',1,1,4),(22,'hello',1,1,2),(23,'hey',1,1,3),(24,'coucou',1,1,4),(26,'Pas de lawyer',1,2,1);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (6,'Dubois','mail1@gmail.com','sujet1','message1',1),(7,'Ponté','mail2@gmail.com','sujet2','message2',1),(8,'Laifa','mail3@gmail.com','sujet3','message3',1),(9,'Bezandry','mail4@gmail.com','sujet4','message4',1),(10,'Cartier','mail5@gmail.com','sujet5','message5',1),(11,'Domanech','mail6@gmail.com','sujet6','message6',0),(12,'Long','mail7@gmail.com','sujet7','message7',0),(13,'Turki','mail8@gmail.com','sujet8','message8',0);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (17,'2021-03-12','message1',1,2,0,1),(18,'2021-03-11','message2',1,3,1,0),(19,'2021-03-14','message3',1,4,0,1),(20,'2021-03-15','message4',1,2,1,1),(21,'2021-03-16','message5',1,3,0,0),(22,'2021-02-12','message6',1,4,1,1),(23,'2021-03-21','message7',1,2,0,0),(24,'2021-04-12','message8',4,3,1,1),(25,'2021-03-23','message9',4,4,0,1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (77,'HOLIDAY','2021-04-17',0,24,'OTHER','description2',1,1,1,1,'lien2'),(78,'MEETING','2021-03-12',8,1,'DIVORCE','description1',1,1,2,1,'lien1'),(79,'AUDIENCE','2021-05-13',13,4,'FAMILY','description3',1,1,2,0,NULL),(80,'OTHER','2021-05-13',9,2,'CHILD_CARE','description4',1,1,3,0,NULL),(81,'MEETING','2021-05-14',8,1,'DIVORCE','description5',1,1,2,1,'lien6'),(82,'MEETING','2021-05-14',9,1,'ADVICE','description6',1,1,2,1,'lien7'),(83,'MEETING','2021-05-14',11,1,'SUCCESSION','description7',1,1,3,1,'lien8'),(84,'MEETING','2021-05-14',12,1,'CRIMINAL','description8',1,1,4,0,NULL),(85,'MEETING','2021-05-14',13,1,'FAMILY','description9',1,1,2,0,NULL),(86,'MEETING','2021-05-14',14,1,'CHILD_CARE','description10',0,1,3,0,NULL),(87,'MEETING','2021-05-14',15,1,'ALIMONY','description11',0,1,4,1,'lien11'),(88,'MEETING','2021-05-14',16,1,'CONSTRUCTION','description12',0,1,2,1,'lien12'),(89,'MEETING','2021-05-14',17,1,'INTELLECTUAL_PROPERTY','description13',0,1,3,0,NULL);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Fernandez','Mylène','mymy','3e8738ad025865addb12e21c79c1f1e4','LAWYER'),(2,'Dubois','Geoffrey','geo','8ac48b780a569a2fabfe7ce1018e8b50','CLIENT'),(3,'Ponte','Camille','cam','e0cdee00b05d237f53140af5ab43d7d5','CLIENT'),(4,'Murphy','Fripouille','frypon','e0ff5d96f8fb65a953c5a5b2158e0e23','CLIENT'),(5,'AddAddressTest','AddAddressTest','AddAddressTest','AddAddressTest','DEVELOPPER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-01  9:52:35

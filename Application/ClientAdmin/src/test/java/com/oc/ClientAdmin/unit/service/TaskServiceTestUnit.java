package com.oc.ClientAdmin.unit.service;

import com.oc.ClientAdmin.beans.TaskBean;
import com.oc.ClientAdmin.service.TaskService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTestUnit {
    @InjectMocks
    TaskService taskService;

    @Test
    public void pourcentTest_ListTask_int(){
        List<TaskBean> tasks = new ArrayList<>();
        TaskBean task1 = new TaskBean();
        task1.setDuration(1);
        task1.setHour(4);
        TaskBean task2 = new TaskBean();
        task2.setDuration(1);
        task2.setHour(23);
        tasks.add(task1);
        tasks.add(task2);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 1 ){
            hour += 12;
        }
        if(hour>5 && hour<23){
            Assert.assertEquals(50,taskService.pourcent(tasks));
        }
        if(hour<4){
            Assert.assertEquals(0,taskService.pourcent(tasks));
        }
        if(hour>23){
            Assert.assertEquals(100,taskService.pourcent(tasks));
        }
    }

    @Test
    public void currentTaskTest_ListTask_task(){
        List<TaskBean> tasks = new ArrayList<>();
        TaskBean task1 = new TaskBean();
        task1.setDuration(8);
        task1.setHour(4);
        TaskBean task2 = new TaskBean();
        task2.setDuration(12);
        task2.setHour(12);
        tasks.add(task1);
        tasks.add(task2);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 1 ){
            hour += 12;
        }
        if(hour<12){
            Assert.assertEquals(task1,taskService.currentTask(tasks));
        }
        else{
            Assert.assertEquals(task2,taskService.currentTask(tasks));
        }
    }

    @Test
    public void nextTaskTest_ListTask_task(){
        List<TaskBean> tasks = new ArrayList<>();
        TaskBean task1 = new TaskBean();
        task1.setDuration(8);
        task1.setHour(4);
        TaskBean task2 = new TaskBean();
        task2.setDuration(10);
        task2.setHour(12);
        TaskBean task3 = new TaskBean();
        task3.setDuration(2);
        task3.setHour(22);
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 1 ){
            hour += 12;
        }
        if(hour<4){
            Assert.assertEquals(task1,taskService.nextTask(tasks));
        }
        else if(hour<12){
            Assert.assertEquals(task2,taskService.nextTask(tasks));
        }
        else{
            Assert.assertEquals(task3,taskService.nextTask(tasks));
        }
    }
}

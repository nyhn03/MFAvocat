package com.oc.ClientAdmin.enums;

/**
 * TaskType matches to type of tasks
 */
public enum TaskType {
    NO_CHOICE,
    MEETING,
    HOLIDAY,
    AUDIENCE,
    OTHER
}

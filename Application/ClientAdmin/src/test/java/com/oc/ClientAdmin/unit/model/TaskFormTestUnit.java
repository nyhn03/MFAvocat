package com.oc.ClientAdmin.unit.model;

import com.oc.ClientAdmin.model.TaskForm;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;

public class TaskFormTestUnit {
    @Test
    public void validateSettersAndGetters() {
        final PojoClass userPojo = PojoClassFactory.getPojoClass(TaskForm.class);

        final Validator validator = ValidatorBuilder.create()
                .with(new SetterTester(), new GetterTester())
                .build();
        validator.validate(userPojo);
    }
}

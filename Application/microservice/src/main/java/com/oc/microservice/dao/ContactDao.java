package com.oc.microservice.dao;

import com.oc.microservice.model.Contact;
import com.oc.microservice.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ContactDao extends JpaRepository<Contact,Long> {
    /**
     * give all contact not seen
     * @return list of contact
     */
    @Query("SELECT contact FROM Contact contact WHERE contact.seen = false")
    List<Contact> findContactByNotSeen();

    /**
     * give all contact seen
     * @return list of contact
     */
    @Query("SELECT contact FROM Contact contact WHERE contact.seen = true")
    List<Contact> findContactBySeen();
}

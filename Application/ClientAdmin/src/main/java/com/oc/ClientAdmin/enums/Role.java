package com.oc.ClientAdmin.enums;

/**
 * Role matches to role of User
 */
public enum Role {
    DEVELOPPER,
    LAWYER,
    CLIENT,
    NOT_CONNECTED
}
